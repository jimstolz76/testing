using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;
using BiampTesiraLib;
using BiampTesiraLib.Parser;
using BiampTesiraLib.Components;
using BiampTesiraLib.Comm;
using BiampTesiraLib.Model;
using CCI_Library;

namespace UserModule_BIAMP_TESIRA_ROUTER_CONTROL_V2_1
{
    public class UserModuleClass_BIAMP_TESIRA_ROUTER_CONTROL_V2_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput POLL_ROUTER;
        Crestron.Logos.SplusObjects.DigitalInput ROUTE_INPUT;
        Crestron.Logos.SplusObjects.DigitalInput DEROUTE_INPUT;
        Crestron.Logos.SplusObjects.DigitalInput TOGGLE_INPUT;
        Crestron.Logos.SplusObjects.AnalogInput NEW_INPUT;
        Crestron.Logos.SplusObjects.DigitalOutput IS_INITIALIZED;
        Crestron.Logos.SplusObjects.DigitalOutput IS_ROUTED;
        Crestron.Logos.SplusObjects.AnalogOutput OUTPUT_ROUTED;
        StringParameter INSTANCE_TAG;
        StringParameter ROUTER_TYPE;
        UShortParameter OUTPUT;
        UShortParameter COMMAND_PROCESSOR_ID;
        BiampTesiraLib.RouterComponent COMPONENT;
        public void ISINIT ( ushort STATE ) 
            { 
            try
            {
                SplusExecutionContext __context__ = SplusSimplSharpDelegateThreadStartCode();
                
                __context__.SourceCodeLine = 123;
                IS_INITIALIZED  .Value = (ushort) ( STATE ) ; 
                
                
            }
            finally { ObjectFinallyHandler(); }
            }
            
        public void ONOUTPUTROUTED ( ushort INPUT ) 
            { 
            try
            {
                SplusExecutionContext __context__ = SplusSimplSharpDelegateThreadStartCode();
                
                __context__.SourceCodeLine = 128;
                OUTPUT_ROUTED  .Value = (ushort) ( INPUT ) ; 
                
                
            }
            finally { ObjectFinallyHandler(); }
            }
            
        object POLL_ROUTER_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 137;
                COMPONENT . PollRouter ( ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object ROUTE_INPUT_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 142;
            COMPONENT . RouteInput ( (ushort)( NEW_INPUT  .UshortValue )) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object DEROUTE_INPUT_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 147;
        COMPONENT . DerouteInput ( (ushort)( NEW_INPUT  .UshortValue )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TOGGLE_INPUT_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 152;
        COMPONENT . ToggleInput ( (ushort)( NEW_INPUT  .UshortValue )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object NEW_INPUT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 157;
        IS_ROUTED  .Value = (ushort) ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (OUTPUT_ROUTED  .Value == NEW_INPUT  .UshortValue) ) && Functions.TestForTrue ( Functions.BoolToInt ( NEW_INPUT  .UshortValue > 0 ) )) ) ) ; 
        __context__.SourceCodeLine = 159;
        if ( Functions.TestForTrue  ( ( ROUTE_INPUT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 161;
            COMPONENT . RouteInput ( (ushort)( NEW_INPUT  .UshortValue )) ; 
            __context__.SourceCodeLine = 162;
            Functions.Delay (  (int) ( 30 ) ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 174;
        WaitForInitializationComplete ( ) ; 
        __context__.SourceCodeLine = 175;
        // RegisterDelegate( COMPONENT , ONINITIALIZECHANGE , ISINIT ) 
        COMPONENT .OnInitializeChange  = ISINIT; ; 
        __context__.SourceCodeLine = 176;
        // RegisterDelegate( COMPONENT , ONOUTPUTROUTEDCHANGE , ONOUTPUTROUTED ) 
        COMPONENT .OnOutputRoutedChange  = ONOUTPUTROUTED; ; 
        __context__.SourceCodeLine = 178;
        COMPONENT . Configure ( (ushort)( COMMAND_PROCESSOR_ID  .Value ), INSTANCE_TAG  .ToString(), ROUTER_TYPE  .ToString(), (ushort)( OUTPUT  .Value )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    
    POLL_ROUTER = new Crestron.Logos.SplusObjects.DigitalInput( POLL_ROUTER__DigitalInput__, this );
    m_DigitalInputList.Add( POLL_ROUTER__DigitalInput__, POLL_ROUTER );
    
    ROUTE_INPUT = new Crestron.Logos.SplusObjects.DigitalInput( ROUTE_INPUT__DigitalInput__, this );
    m_DigitalInputList.Add( ROUTE_INPUT__DigitalInput__, ROUTE_INPUT );
    
    DEROUTE_INPUT = new Crestron.Logos.SplusObjects.DigitalInput( DEROUTE_INPUT__DigitalInput__, this );
    m_DigitalInputList.Add( DEROUTE_INPUT__DigitalInput__, DEROUTE_INPUT );
    
    TOGGLE_INPUT = new Crestron.Logos.SplusObjects.DigitalInput( TOGGLE_INPUT__DigitalInput__, this );
    m_DigitalInputList.Add( TOGGLE_INPUT__DigitalInput__, TOGGLE_INPUT );
    
    IS_INITIALIZED = new Crestron.Logos.SplusObjects.DigitalOutput( IS_INITIALIZED__DigitalOutput__, this );
    m_DigitalOutputList.Add( IS_INITIALIZED__DigitalOutput__, IS_INITIALIZED );
    
    IS_ROUTED = new Crestron.Logos.SplusObjects.DigitalOutput( IS_ROUTED__DigitalOutput__, this );
    m_DigitalOutputList.Add( IS_ROUTED__DigitalOutput__, IS_ROUTED );
    
    NEW_INPUT = new Crestron.Logos.SplusObjects.AnalogInput( NEW_INPUT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( NEW_INPUT__AnalogSerialInput__, NEW_INPUT );
    
    OUTPUT_ROUTED = new Crestron.Logos.SplusObjects.AnalogOutput( OUTPUT_ROUTED__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( OUTPUT_ROUTED__AnalogSerialOutput__, OUTPUT_ROUTED );
    
    OUTPUT = new UShortParameter( OUTPUT__Parameter__, this );
    m_ParameterList.Add( OUTPUT__Parameter__, OUTPUT );
    
    COMMAND_PROCESSOR_ID = new UShortParameter( COMMAND_PROCESSOR_ID__Parameter__, this );
    m_ParameterList.Add( COMMAND_PROCESSOR_ID__Parameter__, COMMAND_PROCESSOR_ID );
    
    INSTANCE_TAG = new StringParameter( INSTANCE_TAG__Parameter__, this );
    m_ParameterList.Add( INSTANCE_TAG__Parameter__, INSTANCE_TAG );
    
    ROUTER_TYPE = new StringParameter( ROUTER_TYPE__Parameter__, this );
    m_ParameterList.Add( ROUTER_TYPE__Parameter__, ROUTER_TYPE );
    
    
    POLL_ROUTER.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_ROUTER_OnPush_0, true ) );
    ROUTE_INPUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( ROUTE_INPUT_OnPush_1, true ) );
    DEROUTE_INPUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DEROUTE_INPUT_OnPush_2, true ) );
    TOGGLE_INPUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( TOGGLE_INPUT_OnPush_3, true ) );
    NEW_INPUT.OnAnalogChange.Add( new InputChangeHandlerWrapper( NEW_INPUT_OnChange_4, true ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    COMPONENT  = new BiampTesiraLib.RouterComponent();
    
    
}

public UserModuleClass_BIAMP_TESIRA_ROUTER_CONTROL_V2_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint POLL_ROUTER__DigitalInput__ = 0;
const uint ROUTE_INPUT__DigitalInput__ = 1;
const uint DEROUTE_INPUT__DigitalInput__ = 2;
const uint TOGGLE_INPUT__DigitalInput__ = 3;
const uint NEW_INPUT__AnalogSerialInput__ = 0;
const uint IS_INITIALIZED__DigitalOutput__ = 0;
const uint IS_ROUTED__DigitalOutput__ = 1;
const uint OUTPUT_ROUTED__AnalogSerialOutput__ = 0;
const uint INSTANCE_TAG__Parameter__ = 10;
const uint ROUTER_TYPE__Parameter__ = 11;
const uint OUTPUT__Parameter__ = 12;
const uint COMMAND_PROCESSOR_ID__Parameter__ = 13;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}

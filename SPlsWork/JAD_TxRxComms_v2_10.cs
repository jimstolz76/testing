using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_JAD_TXRXCOMMS_V2_10
{
    public class UserModuleClass_JAD_TXRXCOMMS_V2_10 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        Crestron.Logos.SplusObjects.DigitalInput SOIP_ENABLE;
        Crestron.Logos.SplusObjects.DigitalInput START_MODULE;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.StringInput TX;
        Crestron.Logos.SplusObjects.StringInput CRESTRON_ID;
        Crestron.Logos.SplusObjects.StringInput CONSOLE_CMD;
        Crestron.Logos.SplusObjects.AnalogInput AUDIO_DELAY;
        Crestron.Logos.SplusObjects.AnalogInput TEAR_ADJUST;
        Crestron.Logos.SplusObjects.StringOutput RX;
        Crestron.Logos.SplusObjects.StringOutput VERSION__DOLLAR__;
        SplusTcpClient JAP_DEV_TELNET;
        SplusTcpClient JAP_DEV_CONTROL;
        StringParameter IP_ADDRESS;
        StringParameter SOIP_MODE;
        StringParameter BAUD_RATE;
        StringParameter DATA_BIT;
        StringParameter PARITY;
        StringParameter STOP_BIT;
        StringParameter LICENCE_KEY;
        _9 _15;
        _9 [] _16;
        _9 _17;
        ushort _18 = 0;
        CrestronString _19;
        CrestronString _20;
        CrestronString _21;
        CrestronString _22;
        CrestronString _23;
        CrestronString _24;
        CrestronString _25;
        ushort _26 = 0;
        ushort _27 = 0;
        ushort _28 = 0;
        ushort _29 = 0;
        ushort _30 = 0;
        CrestronString _31;
        ushort _32 = 0;
        ushort _33 = 0;
        ushort _34 = 0;
        ushort _35 = 0;
        ushort _36 = 0;
        ushort _37 = 0;
        private void _38 (  SplusExecutionContext __context__, CrestronString _40 , CrestronString _41 ) 
            { 
            CrestronString _15;
            CrestronString _42;
            _15  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5000, this );
            _42  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 250, this );
            
            ushort _43 = 0;
            
            
            __context__.SourceCodeLine = 15;
            _43 = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 15;
            _15  .UpdateValue ( _41  ) ; 
            __context__.SourceCodeLine = 15;
            if ( Functions.TestForTrue  ( ( _32)  ) ) 
                { 
                __context__.SourceCodeLine = 15;
                while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( _15 ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 15;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( _15 ) > 200 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 15;
                        _42  .UpdateValue ( Functions.Remove ( 200, _15 )  ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 16;
                        _42  .UpdateValue ( _15  ) ; 
                        __context__.SourceCodeLine = 16;
                        _15  .UpdateValue ( ""  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 16;
                    if ( Functions.TestForTrue  ( ( _43)  ) ) 
                        { 
                        __context__.SourceCodeLine = 16;
                        Print( "{0} ({1}) {2} - {3}\r\n", Functions.Time ( ) , IP_ADDRESS , _40 , _42 ) ; 
                        __context__.SourceCodeLine = 16;
                        _43 = (ushort) ( 0 ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 16;
                        Print( "         {0}\r\n", _42 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 15;
                    } 
                
                } 
            
            
            }
            
        private void _39 (  SplusExecutionContext __context__, CrestronString _41 ) 
            { 
            
            __context__.SourceCodeLine = 16;
            _38 (  __context__ , "setConnectStateControl", _41) ; 
            __context__.SourceCodeLine = 16;
            _24  .UpdateValue ( _41  ) ; 
            
            }
            
        private void _40 (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 16;
            _34 = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 16;
            _33 = (ushort) ( 0 ) ; 
            
            }
            
        private void _41 (  SplusExecutionContext __context__, _9 _43 ) 
            { 
            CrestronString _44;
            _44  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            
            __context__.SourceCodeLine = 16;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( ((_33 + 1) - _34) < 1000 ))  ) ) 
                { 
                __context__.SourceCodeLine = 16;
                _33 = (ushort) ( (_33 + 1) ) ; 
                __context__.SourceCodeLine = 16;
                _16 [ Mod( _33 , 1000 )] . _10  .UpdateValue ( _43 . _10  ) ; 
                __context__.SourceCodeLine = 16;
                _16 [ Mod( _33 , 1000 )] . _11  .UpdateValue ( _43 . _11  ) ; 
                __context__.SourceCodeLine = 16;
                _16 [ Mod( _33 , 1000 )] . _12 = (ushort) ( _43._12 ) ; 
                __context__.SourceCodeLine = 16;
                _16 [ Mod( _33 , 1000 )] . _13  .UpdateValue ( _43 . _13  ) ; 
                __context__.SourceCodeLine = 16;
                _16 [ Mod( _33 , 1000 )] . _14 = (ushort) ( _43._14 ) ; 
                __context__.SourceCodeLine = 16;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _34 > 1000 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _33 > 1000 ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 16;
                    _34 = (ushort) ( (_34 - 1000) ) ; 
                    __context__.SourceCodeLine = 16;
                    _33 = (ushort) ( (_33 - 1000) ) ; 
                    } 
                
                __context__.SourceCodeLine = 17;
                _44  .UpdateValue ( "Queuing: " + _43 . _10 + "   (" + Functions.ItoA (  (int) ( _34 ) ) + " / " + Functions.ItoA (  (int) ( _33 ) ) + ")"  ) ; 
                __context__.SourceCodeLine = 17;
                _38 (  __context__ , "Queue", _44) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 17;
                _38 (  __context__ , "Queue", "\r\nQueue:Error queue full") ; 
                } 
            
            
            }
            
        private void _42 (  SplusExecutionContext __context__, ref _9 _44 ) 
            { 
            
            __context__.SourceCodeLine = 17;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _34 <= _33 ))  ) ) 
                { 
                __context__.SourceCodeLine = 17;
                _44 . _10  .UpdateValue ( _16 [ Mod( _34 , 1000 )] . _10  ) ; 
                __context__.SourceCodeLine = 17;
                _44 . _11  .UpdateValue ( _16 [ Mod( _34 , 1000 )] . _11  ) ; 
                __context__.SourceCodeLine = 17;
                _44 . _12 = (ushort) ( _16[ Mod( _34 , 1000 ) ]._12 ) ; 
                __context__.SourceCodeLine = 17;
                _44 . _13  .UpdateValue ( _16 [ Mod( _34 , 1000 )] . _13  ) ; 
                __context__.SourceCodeLine = 17;
                _44 . _14 = (ushort) ( _16[ Mod( _34 , 1000 ) ]._14 ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 17;
                _38 (  __context__ , "Queue", "ERROR _ NOTHING IN LIST") ; 
                __context__.SourceCodeLine = 17;
                _40 (  __context__  ) ; 
                } 
            
            
            }
            
        private void _43 (  SplusExecutionContext __context__, ref _9 _45 ) 
            { 
            
            __context__.SourceCodeLine = 17;
            _42 (  __context__ ,   ref  _45 ) ; 
            __context__.SourceCodeLine = 17;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( ((_33 + 1) - _34) < 1000 ))  ) ) 
                { 
                __context__.SourceCodeLine = 17;
                _16 [ Mod( _34 , 1000 )] . _10  .UpdateValue ( "NULL STRING"  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 17;
                _38 (  __context__ , "Queue", "Queue:Error") ; 
                } 
            
            
            }
            
        private void _44 (  SplusExecutionContext __context__, ref _9 _46 ) 
            { 
            
            __context__.SourceCodeLine = 18;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _34 <= _33 ))  ) ) 
                { 
                __context__.SourceCodeLine = 18;
                _46 . _10  .UpdateValue ( _16 [ Mod( _33 , 1000 )] . _10  ) ; 
                __context__.SourceCodeLine = 18;
                _46 . _11  .UpdateValue ( _16 [ Mod( _33 , 1000 )] . _11  ) ; 
                __context__.SourceCodeLine = 18;
                _46 . _12 = (ushort) ( _16[ Mod( _33 , 1000 ) ]._12 ) ; 
                __context__.SourceCodeLine = 18;
                _46 . _13  .UpdateValue ( _16 [ Mod( _33 , 1000 )] . _13  ) ; 
                __context__.SourceCodeLine = 18;
                _46 . _14 = (ushort) ( _16[ Mod( _33 , 1000 ) ]._14 ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 18;
                _38 (  __context__ , "Queue", "Queue:Error") ; 
                } 
            
            
            }
            
        private ushort _45 (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 18;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _34 > _33 ))  ) ) 
                { 
                __context__.SourceCodeLine = 18;
                _40 (  __context__  ) ; 
                __context__.SourceCodeLine = 18;
                return (ushort)( 65535) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 18;
                return (ushort)( 0) ; 
                } 
            
            
            return 0; // default return value (none specified in module)
            }
            
        private ushort _46 (  SplusExecutionContext __context__, CrestronString _48 ) 
            { 
            ushort _49 = 0;
            ushort _50 = 0;
            ushort _51 = 0;
            
            
            __context__.SourceCodeLine = 18;
            _51 = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 18;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( _48 ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( _49  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_49  >= __FN_FORSTART_VAL__1) && (_49  <= __FN_FOREND_VAL__1) ) : ( (_49  <= __FN_FORSTART_VAL__1) && (_49  >= __FN_FOREND_VAL__1) ) ; _49  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 18;
                _50 = (ushort) ( Byte( _48 , (int)( _49 ) ) ) ; 
                __context__.SourceCodeLine = 18;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _50 >= 48 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _50 <= 57 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (_50 == 46) )) ))  ) ) 
                    { 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 18;
                    _51 = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 18;
                    break ; 
                    } 
                
                __context__.SourceCodeLine = 18;
                } 
            
            __context__.SourceCodeLine = 18;
            return (ushort)( _51) ; 
            
            }
            
        private ushort _47 (  SplusExecutionContext __context__, CrestronString _49 ) 
            { 
            ushort [] _50;
            _50  = new ushort[ 4 ];
            
            ushort [] _51;
            _51  = new ushort[ 5 ];
            
            CrestronString _52;
            _52  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64, this );
            
            
            __context__.SourceCodeLine = 19;
            _50 [ 1] = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 19;
            _50 [ 2] = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 19;
            _50 [ 3] = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 19;
            _51 [ 1] = (ushort) ( 256 ) ; 
            __context__.SourceCodeLine = 19;
            _51 [ 2] = (ushort) ( 256 ) ; 
            __context__.SourceCodeLine = 19;
            _51 [ 3] = (ushort) ( 256 ) ; 
            __context__.SourceCodeLine = 19;
            _51 [ 4] = (ushort) ( 256 ) ; 
            __context__.SourceCodeLine = 19;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( _49 ) <= 15 ) ) && Functions.TestForTrue ( _46( __context__ , _49 ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 19;
                _50 [ 1] = (ushort) ( Functions.Find( "." , _49 , 1 ) ) ; 
                __context__.SourceCodeLine = 19;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _50[ 1 ] > 1 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 19;
                    _51 [ 1] = (ushort) ( Functions.Atoi( Functions.Left( _49 , (int)( (_50[ 1 ] - 1) ) ) ) ) ; 
                    __context__.SourceCodeLine = 19;
                    _50 [ 2] = (ushort) ( Functions.Find( "." , _49 , (_50[ 1 ] + 1) ) ) ; 
                    __context__.SourceCodeLine = 19;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (_50[ 2 ] + 1) > _50[ 1 ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 19;
                        _51 [ 2] = (ushort) ( Functions.Atoi( Functions.Mid( _49 , (int)( (_50[ 1 ] + 1) ) , (int)( ((_50[ 2 ] - _50[ 1 ]) - 1) ) ) ) ) ; 
                        __context__.SourceCodeLine = 19;
                        _50 [ 3] = (ushort) ( Functions.Find( "." , _49 , (_50[ 2 ] + 1) ) ) ; 
                        __context__.SourceCodeLine = 19;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (_50[ 3 ] + 1) > _50[ 2 ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 19;
                            _51 [ 3] = (ushort) ( Functions.Atoi( Functions.Mid( _49 , (int)( (_50[ 2 ] + 1) ) , (int)( ((_50[ 3 ] - _50[ 2 ]) - 1) ) ) ) ) ; 
                            __context__.SourceCodeLine = 19;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( _49 ) > _50[ 3 ] ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( _49 ) < (_50[ 3 ] + 4) ) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 19;
                                _51 [ 4] = (ushort) ( Functions.Atoi( Functions.Right( _49 , (int)( (Functions.Length( _49 ) - _50[ 3 ]) ) ) ) ) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    } 
                
                } 
            
            __context__.SourceCodeLine = 20;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _51[ 1 ] < 256 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _51[ 2 ] < 256 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _51[ 3 ] < 256 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _51[ 4 ] < 256 ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 20;
                return (ushort)( 65535) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 20;
                return (ushort)( 0) ; 
                } 
            
            
            return 0; // default return value (none specified in module)
            }
            
        private void _48 (  SplusExecutionContext __context__ ) 
            { 
            short _50 = 0;
            
            CrestronString _51;
            _51  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64, this );
            
            CrestronString _52;
            _52  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64, this );
            
            
            __context__.SourceCodeLine = 20;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_24 != "CONNECTED_6752") ) && Functions.TestForTrue ( Functions.BoolToInt (_24 != "WAITING SOCKET") )) ))  ) ) 
                {
                __context__.SourceCodeLine = 20;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IP_ADDRESS  != ""))  ) ) 
                    { 
                    __context__.SourceCodeLine = 20;
                    _38 (  __context__ , "Comms_Open_6752", "Connecting") ; 
                    __context__.SourceCodeLine = 20;
                    if ( Functions.TestForTrue  ( ( _47( __context__ , IP_ADDRESS  ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 20;
                        _50 = (short) ( Functions.SocketConnectClient( JAP_DEV_CONTROL , IP_ADDRESS  , (ushort)( 6752 ) , (ushort)( 1 ) ) ) ; 
                        __context__.SourceCodeLine = 20;
                        _39 (  __context__ , "WAITING SOCKET") ; 
                        __context__.SourceCodeLine = 20;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_50 != 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 20;
                            MakeString ( _52 , "socketconnectclient error ({0:d})", (short)_50) ; 
                            __context__.SourceCodeLine = 20;
                            _38 (  __context__ , "Comms_Open_6752", _52) ; 
                            __context__.SourceCodeLine = 20;
                            return ; 
                            } 
                        
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 20;
                        _52  .UpdateValue ( "Invalid IP address: " + IP_ADDRESS  ) ; 
                        __context__.SourceCodeLine = 21;
                        _38 (  __context__ , "Comms_Open_6752", _52) ; 
                        } 
                    
                    } 
                
                }
            
            
            }
            
        private void _49 (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 21;
            _38 (  __context__ , "COMMS", "Closing TCP port 6752") ; 
            __context__.SourceCodeLine = 21;
            Functions.SocketDisconnectClient ( JAP_DEV_CONTROL ) ; 
            
            }
            
        private void _50 (  SplusExecutionContext __context__ ) 
            { 
            ushort _52 = 0;
            
            
            __context__.SourceCodeLine = 21;
            _38 (  __context__ , "Comms_Close_Telnet", "Closing TCP port") ; 
            __context__.SourceCodeLine = 21;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_26 == 2))  ) ) 
                { 
                __context__.SourceCodeLine = 21;
                Functions.SocketDisconnectClient ( JAP_DEV_TELNET ) ; 
                } 
            
            
            }
            
        private void _51 (  SplusExecutionContext __context__ ) 
            { 
            short _53 = 0;
            
            CrestronString _54;
            _54  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64, this );
            
            CrestronString _55;
            _55  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64, this );
            
            
            __context__.SourceCodeLine = 21;
            MakeString ( _55 , "socketStatus23 ({0:d})", (short)_26) ; 
            __context__.SourceCodeLine = 21;
            _38 (  __context__ , "Comms_Open_Telnet", _55) ; 
            __context__.SourceCodeLine = 21;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_26 == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (_26 == 2) )) ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 21;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IP_ADDRESS  != ""))  ) ) 
                    { 
                    __context__.SourceCodeLine = 21;
                    if ( Functions.TestForTrue  ( ( _47( __context__ , IP_ADDRESS  ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 21;
                        _38 (  __context__ , "Comms_Open_Telnet", "Connecting") ; 
                        __context__.SourceCodeLine = 21;
                        _37 = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 22;
                        _53 = (short) ( Functions.SocketConnectClient( JAP_DEV_TELNET , IP_ADDRESS  , (ushort)( 23 ) , (ushort)( 1 ) ) ) ; 
                        __context__.SourceCodeLine = 22;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_53 != 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 22;
                            MakeString ( _55 , "socketconnectclient error ({0:d})", (short)_53) ; 
                            __context__.SourceCodeLine = 22;
                            _38 (  __context__ , "Comms_Open_Telnet", _55) ; 
                            __context__.SourceCodeLine = 22;
                            return ; 
                            } 
                        
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 22;
                        _55  .UpdateValue ( "Invalid IP address: " + IP_ADDRESS  ) ; 
                        __context__.SourceCodeLine = 22;
                        _38 (  __context__ , "Comms_Open_Telnet", _55) ; 
                        } 
                    
                    } 
                
                } 
            
            
            }
            
        private void _52 (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 22;
            _38 (  __context__ , "QUEUE_TIMEOUT1", "_queueTimeOut") ; 
            __context__.SourceCodeLine = 22;
            _50 (  __context__  ) ; 
            __context__.SourceCodeLine = 22;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_21__" , 400 , __SPLS_TMPVAR__WAITLABEL_21___Callback ) ;
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_21___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            {
            __context__.SourceCodeLine = 22;
            _51 (  __context__  ) ; 
            }
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void _53 (  SplusExecutionContext __context__ ) 
        { 
        
        __context__.SourceCodeLine = 22;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_22__" , 3000 , __SPLS_TMPVAR__WAITLABEL_22___Callback ) ;
        
        }
        
    public void __SPLS_TMPVAR__WAITLABEL_22___CallbackFn( object stateInfo )
    {
    
        try
        {
            Wait __LocalWait__ = (Wait)stateInfo;
            SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
            __LocalWait__.RemoveFromList();
            
            
            __context__.SourceCodeLine = 22;
            _52 (  __context__  ) ; 
            
        
        
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        
    }
    
private void _54 (  SplusExecutionContext __context__, CrestronString _56 ) 
    { 
    
    __context__.SourceCodeLine = 22;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_24 == "CONNECTED_6752") ) && Functions.TestForTrue ( Functions.BoolToInt (_27 == 1) )) ))  ) ) 
        { 
        __context__.SourceCodeLine = 22;
        Functions.SocketSend ( JAP_DEV_CONTROL , _56 ) ; 
        } 
    
    else 
        {
        __context__.SourceCodeLine = 22;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_24 != "CONNECTED_6752"))  ) ) 
            { 
            __context__.SourceCodeLine = 22;
            _38 (  __context__ , "sendToSOIP", "SOCKET NOT CONNECTED") ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 23;
            _38 (  __context__ , "sendToSOIP", "NOT INITIALISED") ; 
            } 
        
        }
    
    
    }
    
private void _55 (  SplusExecutionContext __context__, CrestronString _57 ) 
    { 
    CrestronString _58;
    _58  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
    
    CrestronString _59;
    _59  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
    
    
    __context__.SourceCodeLine = 23;
    MakeString ( _59 , "> {0}", _57 ) ; 
    __context__.SourceCodeLine = 23;
    _38 (  __context__ , "sendToJAPComms", _59) ; 
    __context__.SourceCodeLine = 23;
    _58  .UpdateValue ( _57 + "\r\n"  ) ; 
    __context__.SourceCodeLine = 23;
    Functions.SocketSend ( JAP_DEV_TELNET , _58 ) ; 
    
    }
    
private void _56 (  SplusExecutionContext __context__, CrestronString _10 , CrestronString _58 , ushort _12 , CrestronString _59 , ushort _14 ) 
    { 
    CrestronString _60;
    _60  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
    
    
    __context__.SourceCodeLine = 23;
    _60  .UpdateValue ( _10 + "   (okResponse: " + _58 + ")"  ) ; 
    __context__.SourceCodeLine = 23;
    _38 (  __context__ , "sendNow", _60) ; 
    __context__.SourceCodeLine = 23;
    _15 . _10  .UpdateValue ( _10  ) ; 
    __context__.SourceCodeLine = 23;
    _15 . _11  .UpdateValue ( _58  ) ; 
    __context__.SourceCodeLine = 23;
    _15 . _12 = (ushort) ( _12 ) ; 
    __context__.SourceCodeLine = 23;
    _15 . _13  .UpdateValue ( _59  ) ; 
    __context__.SourceCodeLine = 23;
    _15 . _14 = (ushort) ( _14 ) ; 
    __context__.SourceCodeLine = 23;
    _29 = (ushort) ( 65535 ) ; 
    __context__.SourceCodeLine = 23;
    _53 (  __context__  ) ; 
    __context__.SourceCodeLine = 23;
    _55 (  __context__ , _10) ; 
    
    }
    
private void _57 (  SplusExecutionContext __context__ ) 
    { 
    CrestronString _59;
    _59  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    _9 _60;
    _60  = new _9( this, true );
    _60 .PopulateCustomAttributeList( false );
    
    
    __context__.SourceCodeLine = 23;
    if ( Functions.TestForTrue  ( ( _45( __context__ ))  ) ) 
        { 
        __context__.SourceCodeLine = 23;
        _15 . _10  .UpdateValue ( "NULL STRING"  ) ; 
        __context__.SourceCodeLine = 24;
        _38 (  __context__ , "Queue", "Empty") ; 
        __context__.SourceCodeLine = 24;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_23__" , 400 , __SPLS_TMPVAR__WAITLABEL_23___Callback ) ;
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 24;
        CancelAllWait ( ) ; 
        __context__.SourceCodeLine = 24;
        _43 (  __context__ ,   ref  _60 ) ; 
        __context__.SourceCodeLine = 24;
        _34 = (ushort) ( (_34 + 1) ) ; 
        __context__.SourceCodeLine = 24;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_60._10 == "NULL STRING") ) || Functions.TestForTrue ( Functions.BoolToInt (_60._10 == "") )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 24;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_24__" , 10 , __SPLS_TMPVAR__WAITLABEL_24___Callback ) ;
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 24;
            _56 (  __context__ , _60._10, _60._11, (ushort)( _60._12 ), _60._13, (ushort)( _60._14 )) ; 
            } 
        
        } 
    
    
    }
    
public void __SPLS_TMPVAR__WAITLABEL_23___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 24;
            _50 (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_24___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 24;
            _38 (  __context__ , "Queue", "Invalid Entry") ; 
            __context__.SourceCodeLine = 24;
            _57 (  __context__  ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

private void _58 (  SplusExecutionContext __context__ ) 
    { 
    CrestronString _60;
    _60  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    
    __context__.SourceCodeLine = 24;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_26 == 2))  ) ) 
        { 
        __context__.SourceCodeLine = 24;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_15._10 == "NULL STRING"))  ) ) 
            { 
            __context__.SourceCodeLine = 24;
            _57 (  __context__  ) ; 
            } 
        
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 24;
        _51 (  __context__  ) ; 
        } 
    
    
    }
    
private void _59 (  SplusExecutionContext __context__, CrestronString _10 , CrestronString _61 , ushort _62 ) 
    { 
    ushort _63 = 0;
    
    CrestronString _64;
    _64  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    _9 _65;
    _65  = new _9( this, true );
    _65 .PopulateCustomAttributeList( false );
    
    
    __context__.SourceCodeLine = 24;
    _65 . _10  .UpdateValue ( _10  ) ; 
    __context__.SourceCodeLine = 24;
    _65 . _11  .UpdateValue ( _61  ) ; 
    __context__.SourceCodeLine = 24;
    _65 . _12 = (ushort) ( _62 ) ; 
    __context__.SourceCodeLine = 24;
    _65 . _13  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 24;
    _65 . _14 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 24;
    _41 (  __context__ , _65) ; 
    __context__.SourceCodeLine = 24;
    _58 (  __context__  ) ; 
    
    }
    
private CrestronString _60 (  SplusExecutionContext __context__ ) 
    { 
    CrestronString _62;
    _62  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    
    __context__.SourceCodeLine = 25;
    MakeString ( _62 , "{0}-{1}{2}{3}", BAUD_RATE , DATA_BIT , PARITY , STOP_BIT ) ; 
    __context__.SourceCodeLine = 25;
    return ( _62 ) ; 
    
    }
    
private CrestronString _61 (  SplusExecutionContext __context__ ) 
    { 
    CrestronString _63;
    CrestronString _64;
    CrestronString _65;
    _63  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
    _64  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    _65  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    
    
    __context__.SourceCodeLine = 25;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PARITY  == "n"))  ) ) 
        {
        __context__.SourceCodeLine = 25;
        _64  .UpdateValue ( "-parenb"  ) ; 
        }
    
    else 
        {
        __context__.SourceCodeLine = 25;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PARITY  == "o"))  ) ) 
            {
            __context__.SourceCodeLine = 25;
            _64  .UpdateValue ( "parenb parodd"  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 25;
            _64  .UpdateValue ( "parenb -parodd"  ) ; 
            }
        
        }
    
    __context__.SourceCodeLine = 25;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (STOP_BIT  == "1"))  ) ) 
        {
        __context__.SourceCodeLine = 25;
        _65  .UpdateValue ( "-cstopb"  ) ; 
        }
    
    else 
        {
        __context__.SourceCodeLine = 25;
        _65  .UpdateValue ( "cstopb"  ) ; 
        }
    
    __context__.SourceCodeLine = 25;
    MakeString ( _63 , "stty -F /dev/ttyS0 {0} cs{1} {2} {3}", BAUD_RATE , DATA_BIT , _64 , _65 ) ; 
    __context__.SourceCodeLine = 25;
    return ( _63 ) ; 
    
    }
    
private CrestronString _62 (  SplusExecutionContext __context__ ) 
    { 
    CrestronString _64;
    _64  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 3, this );
    
    
    __context__.SourceCodeLine = 25;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SOIP_MODE  == "2"))  ) ) 
        {
        __context__.SourceCodeLine = 25;
        _64  .UpdateValue ( "nyn"  ) ; 
        }
    
    else 
        {
        __context__.SourceCodeLine = 25;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SOIP_MODE  == "1"))  ) ) 
            {
            __context__.SourceCodeLine = 25;
            _64  .UpdateValue ( "nyy"  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 25;
            _64  .UpdateValue ( "nny"  ) ; 
            }
        
        }
    
    __context__.SourceCodeLine = 25;
    return ( _64 ) ; 
    
    }
    
private CrestronString _63 (  SplusExecutionContext __context__, CrestronString _15 ) 
    { 
    ushort _65 = 0;
    
    ushort _66 = 0;
    
    CrestronString _67;
    CrestronString _68;
    _67  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5000, this );
    _68  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    
    __context__.SourceCodeLine = 26;
    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
    ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( _15 ); 
    int __FN_FORSTEP_VAL__1 = (int)1; 
    for ( _65  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_65  >= __FN_FORSTART_VAL__1) && (_65  <= __FN_FOREND_VAL__1) ) : ( (_65  <= __FN_FORSTART_VAL__1) && (_65  >= __FN_FOREND_VAL__1) ) ; _65  += (ushort)__FN_FORSTEP_VAL__1) 
        { 
        __context__.SourceCodeLine = 26;
        _66 = (ushort) ( Byte( _15 , (int)( _65 ) ) ) ; 
        __context__.SourceCodeLine = 26;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _66 >= 65 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _66 <= 122 ) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 26;
            _67  .UpdateValue ( _67 + Functions.Mid ( _15 ,  (int) ( _65 ) ,  (int) ( 1 ) )  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 26;
            MakeString ( _67 , "{0}\\x{1}", _67 , Functions.ItoHex (  (int) ( _66 ) ) ) ; 
            }
        
        __context__.SourceCodeLine = 26;
        } 
    
    __context__.SourceCodeLine = 26;
    return ( _67 ) ; 
    
    }
    
private void _64 (  SplusExecutionContext __context__ ) 
    { 
    CrestronString _66;
    CrestronString _67;
    CrestronString _10;
    _66  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    _67  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 3, this );
    _10  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 265, this );
    
    
    __context__.SourceCodeLine = 26;
    _67  .UpdateValue ( _62 (  __context__  )  ) ; 
    __context__.SourceCodeLine = 26;
    _59 (  __context__ , "cd /usr/local/bin", "#", (ushort)( 0 )) ; 
    __context__.SourceCodeLine = 26;
    MakeString ( _10 , "./astparam s no_soip {0}", Functions.Left ( _67 ,  (int) ( 1 ) ) ) ; 
    __context__.SourceCodeLine = 26;
    _59 (  __context__ , _10, "#", (ushort)( 0 )) ; 
    __context__.SourceCodeLine = 26;
    MakeString ( _10 , "./astparam s soip_type2 {0}", Functions.Mid ( _67 ,  (int) ( 2 ) ,  (int) ( 1 ) ) ) ; 
    __context__.SourceCodeLine = 26;
    _59 (  __context__ , _10, "#", (ushort)( 0 )) ; 
    __context__.SourceCodeLine = 27;
    MakeString ( _10 , "./astparam s soip_guest_on {0}", Functions.Right ( _67 ,  (int) ( 1 ) ) ) ; 
    __context__.SourceCodeLine = 27;
    _59 (  __context__ , _10, "#", (ushort)( 0 )) ; 
    __context__.SourceCodeLine = 27;
    _66  .UpdateValue ( _60 (  __context__  )  ) ; 
    __context__.SourceCodeLine = 27;
    MakeString ( _66 , "./astparam s s0_baudrate {0}", _66 ) ; 
    __context__.SourceCodeLine = 27;
    _59 (  __context__ , _66, "#", (ushort)( 0 )) ; 
    __context__.SourceCodeLine = 27;
    _59 (  __context__ , "./astparam save", "#", (ushort)( 0 )) ; 
    __context__.SourceCodeLine = 27;
    _59 (  __context__ , "sleep 2", "#", (ushort)( 0 )) ; 
    __context__.SourceCodeLine = 27;
    _59 (  __context__ , "reboot", "reboot", (ushort)( 2 )) ; 
    
    }
    
private void _65 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 27;
    _59 (  __context__ , "./astparam dump", "#", (ushort)( 1 )) ; 
    
    }
    
private void _66 (  SplusExecutionContext __context__, CrestronString _68 ) 
    { 
    CrestronString _69;
    CrestronString _70;
    CrestronString _71;
    CrestronString _72;
    CrestronString _73;
    _69  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    _70  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    _71  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    _72  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    _73  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 3, this );
    
    ushort _74 = 0;
    
    
    __context__.SourceCodeLine = 27;
    _74 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 27;
    _73  .UpdateValue ( _62 (  __context__  )  ) ; 
    __context__.SourceCodeLine = 27;
    while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( _68 ) > 0 ))  ) ) 
        { 
        __context__.SourceCodeLine = 27;
        _70  .UpdateValue ( Functions.Remove ( "\r\n" , _68 )  ) ; 
        __context__.SourceCodeLine = 27;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( _70 ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 27;
            _70  .UpdateValue ( Functions.Left ( _70 ,  (int) ( (Functions.Length( _70 ) - 2) ) )  ) ; 
            __context__.SourceCodeLine = 27;
            _72  .UpdateValue ( _60 (  __context__  )  ) ; 
            __context__.SourceCodeLine = 27;
            _71  .UpdateValue ( Functions.Remove ( "=" , _70 )  ) ; 
            __context__.SourceCodeLine = 27;
            _71  .UpdateValue ( Functions.Left ( _71 ,  (int) ( (Functions.Length( _71 ) - 1) ) )  ) ; 
            __context__.SourceCodeLine = 28;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_71 == "no_soip") ) && Functions.TestForTrue ( Functions.BoolToInt (_70 == Functions.Left( _73 , (int)( 1 ) )) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_71 == "soip_type2") ) && Functions.TestForTrue ( Functions.BoolToInt (_70 == Functions.Mid( _73 , (int)( 2 ) , (int)( 1 ) )) )) ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_71 == "soip_guest_on") ) && Functions.TestForTrue ( Functions.BoolToInt (_70 == Functions.Right( _73 , (int)( 1 ) )) )) ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_71 == "s0_baudrate") ) && Functions.TestForTrue ( Functions.BoolToInt (_70 == _72) )) ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 28;
                _74 = (ushort) ( (_74 + 1) ) ; 
                } 
            
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 28;
            _68  .UpdateValue ( ""  ) ; 
            } 
        
        __context__.SourceCodeLine = 27;
        } 
    
    __context__.SourceCodeLine = 28;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_74 != 4))  ) ) 
        { 
        __context__.SourceCodeLine = 28;
        _27 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 28;
        _38 (  __context__ , "process_astparam_dump  ", "SERIALPORT NOT CONFIGURED") ; 
        __context__.SourceCodeLine = 28;
        _64 (  __context__  ) ; 
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 28;
        _27 = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 28;
        _38 (  __context__ , "process_astparam_dump  ", "SERIALPORT CONFIGURED") ; 
        __context__.SourceCodeLine = 28;
        _57 (  __context__  ) ; 
        } 
    
    
    }
    
private CrestronString _67 (  SplusExecutionContext __context__, CrestronString _69 ) 
    { 
    CrestronString _70;
    CrestronString _71;
    _70  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
    _71  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
    
    ushort _72 = 0;
    ushort _73 = 0;
    
    
    __context__.SourceCodeLine = 28;
    _71  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 28;
    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
    ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( _69 ); 
    int __FN_FORSTEP_VAL__1 = (int)1; 
    for ( _72  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_72  >= __FN_FORSTART_VAL__1) && (_72  <= __FN_FOREND_VAL__1) ) : ( (_72  <= __FN_FORSTART_VAL__1) && (_72  >= __FN_FOREND_VAL__1) ) ; _72  += (ushort)__FN_FORSTEP_VAL__1) 
        { 
        __context__.SourceCodeLine = 28;
        _72 = (ushort) ( Functions.Find( ":" , _69 ) ) ; 
        __context__.SourceCodeLine = 28;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _72 > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 28;
            _70  .UpdateValue ( Functions.Remove ( ":" , _69 )  ) ; 
            __context__.SourceCodeLine = 28;
            _70  .UpdateValue ( Functions.Left ( _70 ,  (int) ( (Functions.Length( _70 ) - 1) ) )  ) ; 
            __context__.SourceCodeLine = 29;
            _71  .UpdateValue ( _71 + _70  ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 29;
            _71  .UpdateValue ( _71 + _69  ) ; 
            __context__.SourceCodeLine = 29;
            break ; 
            } 
        
        __context__.SourceCodeLine = 28;
        } 
    
    __context__.SourceCodeLine = 29;
    return ( Functions.Lower ( _71 ) ) ; 
    
    }
    
private void _68 (  SplusExecutionContext __context__, ushort _70 , CrestronString _71 ) 
    { 
    
    __context__.SourceCodeLine = 29;
    _38 (  __context__ , "doAction", Functions.ItoA( (int)( _70 ) )) ; 
    __context__.SourceCodeLine = 29;
    
        {
        int __SPLS_TMPVAR__SWTCH_1__ = ((int)_70);
        
            { 
            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                { 
                __context__.SourceCodeLine = 29;
                _50 (  __context__  ) ; 
                __context__.SourceCodeLine = 29;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_25__" , 1500 , __SPLS_TMPVAR__WAITLABEL_25___Callback ) ;
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                { 
                __context__.SourceCodeLine = 29;
                _66 (  __context__ , _71) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 29;
                _38 (  __context__ , "doAction", "No action to do") ; 
                } 
            
            } 
            
        }
        
    
    
    }
    
public void __SPLS_TMPVAR__WAITLABEL_25___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 29;
            _51 (  __context__  ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

private ushort _69 (  SplusExecutionContext __context__ ) 
    { 
    ushort _71 = 0;
    ushort _72 = 0;
    ushort _73 = 0;
    ushort _74 = 0;
    
    CrestronString _75;
    CrestronString _76;
    _75  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    _76  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    ushort _77 = 0;
    
    CrestronString _78;
    CrestronString _79;
    _78  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10000, this );
    _79  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10000, this );
    
    ushort _80 = 0;
    
    ushort _81 = 0;
    
    CrestronString _82;
    _82  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    
    __context__.SourceCodeLine = 29;
    _80 = (ushort) ( _18 ) ; 
    __context__.SourceCodeLine = 29;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_37 == 1))  ) ) 
        { 
        __context__.SourceCodeLine = 29;
        _71 = (ushort) ( Functions.Find( "#" , JAP_DEV_TELNET.SocketRxBuf , 1 ) ) ; 
        __context__.SourceCodeLine = 29;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _71 > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 29;
            _37 = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 29;
            _79  .UpdateValue ( Functions.Remove ( "#" , JAP_DEV_TELNET .  SocketRxBuf , 1)  ) ; 
            __context__.SourceCodeLine = 30;
            _15 . _10  .UpdateValue ( "NULL STRING"  ) ; 
            __context__.SourceCodeLine = 30;
            _38 (  __context__ , "onCommRx", "Received initial prompt") ; 
            __context__.SourceCodeLine = 30;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SOIP_MODE  != "0") ) && Functions.TestForTrue ( Functions.BoolToInt (_27 == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (BAUD_RATE  != "0") )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 30;
                _40 (  __context__  ) ; 
                __context__.SourceCodeLine = 30;
                _65 (  __context__  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 30;
                _57 (  __context__  ) ; 
                } 
            
            __context__.SourceCodeLine = 30;
            return (ushort)( 0) ; 
            } 
        
        } 
    
    __context__.SourceCodeLine = 30;
    _75  .UpdateValue ( _15 . _11  ) ; 
    __context__.SourceCodeLine = 30;
    _71 = (ushort) ( Functions.Find( _75 , JAP_DEV_TELNET.SocketRxBuf , 1 ) ) ; 
    __context__.SourceCodeLine = 30;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _71 > 0 ))  ) ) 
        { 
        __context__.SourceCodeLine = 30;
        _77 = (ushort) ( _15._12 ) ; 
        __context__.SourceCodeLine = 30;
        _76  .UpdateValue ( "action: " + Functions.ItoA (  (int) ( _77 ) )  ) ; 
        __context__.SourceCodeLine = 30;
        _38 (  __context__ , "RX 23 ", _76) ; 
        __context__.SourceCodeLine = 30;
        CancelAllWait ( ) ; 
        } 
    
    else 
        {
        __context__.SourceCodeLine = 30;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( _15._13 ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 30;
            _75  .UpdateValue ( _15 . _13  ) ; 
            __context__.SourceCodeLine = 30;
            _71 = (ushort) ( Functions.Find( _75 , JAP_DEV_TELNET.SocketRxBuf ) ) ; 
            __context__.SourceCodeLine = 30;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _71 > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 30;
                _77 = (ushort) ( _15._14 ) ; 
                __context__.SourceCodeLine = 30;
                CancelAllWait ( ) ; 
                } 
            
            } 
        
        }
    
    __context__.SourceCodeLine = 30;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _71 > 0 ))  ) ) 
        { 
        __context__.SourceCodeLine = 30;
        _73 = (ushort) ( Functions.Find( _75 , JAP_DEV_TELNET.SocketRxBuf ) ) ; 
        __context__.SourceCodeLine = 30;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _73 > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 30;
            _78  .UpdateValue ( Functions.Remove ( _75 , JAP_DEV_TELNET .  SocketRxBuf , 1)  ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 31;
            Print( "\r\nNOTFound Msg.pattern\r\n") ; 
            } 
        
        __context__.SourceCodeLine = 31;
        _38 (  __context__ , "RX 23 Action", Functions.ItoA( (int)( _77 ) )) ; 
        __context__.SourceCodeLine = 31;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _77 > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 31;
            _68 (  __context__ , (ushort)( _77 ), _78) ; 
            } 
        
        __context__.SourceCodeLine = 31;
        _57 (  __context__  ) ; 
        } 
    
    __context__.SourceCodeLine = 31;
    return (ushort)( _71) ; 
    
    }
    
object START_MODULE_OnPush_0 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort VALUEFIND = 0;
        ushort FINDMACADDRESSCOUNT = 0;
        
        CrestronString LICENCEVALUECLEAN;
        CrestronString CRESTRONCLEAN;
        LICENCEVALUECLEAN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
        CRESTRONCLEAN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
        
        CrestronString VALUE;
        VALUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        
        ushort RESULT = 0;
        
        ushort OUTARRAYSIZE = 0;
        
        ushort [] KEYS;
        KEYS  = new ushort[ 17 ];
        
        CrestronString [] VALUES;
        VALUES  = new CrestronString[ 17 ];
        for( uint i = 0; i < 17; i++ )
            VALUES [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
        
        ushort NUMBERTEST = 0;
        
        CrestronString RXDATAMAC;
        RXDATAMAC  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
        
        CrestronString TMPTRASH;
        TMPTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
        
        
        __context__.SourceCodeLine = 31;
        _38 (  __context__ , "START_MODULE", "Starting...") ; 
        __context__.SourceCodeLine = 31;
        _24  .UpdateValue ( "OFFLINE"  ) ; 
        __context__.SourceCodeLine = 31;
        _27 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 31;
        _36 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 31;
        _15 . _10  .UpdateValue ( "NULL STRING"  ) ; 
        __context__.SourceCodeLine = 31;
        _15 . _11  .UpdateValue ( "NULL STRING"  ) ; 
        __context__.SourceCodeLine = 31;
        _15 . _12 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 31;
        _15 . _13  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 32;
        _15 . _14 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 33;
        
        __context__.SourceCodeLine = 37;
        __context__.SourceCodeLine = 38;
        Print( "WARNING! Pretending that licence is valid!\r\n") ; 
        __context__.SourceCodeLine = 38;
        _28 = (ushort) ( 65535 ) ; 
        
        __context__.SourceCodeLine = 40;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_28 == 65535))  ) ) 
            { 
            __context__.SourceCodeLine = 40;
            _38 (  __context__ , "START_MODULE", "Licence valid. Starting telnet connection.") ; 
            __context__.SourceCodeLine = 40;
            _51 (  __context__  ) ; 
            __context__.SourceCodeLine = 40;
            VERSION__DOLLAR__  .UpdateValue ( "v2_10"  ) ; 
            __context__.SourceCodeLine = 40;
            Print( "Valid Licence") ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 40;
            _38 (  __context__ , "START_MODULE", "Licence invalid.") ; 
            __context__.SourceCodeLine = 40;
            VERSION__DOLLAR__  .UpdateValue ( "Invalid Licence"  ) ; 
            __context__.SourceCodeLine = 40;
            Print( "Invalid Licence") ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SOIP_ENABLE_OnChange_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 40;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SOIP_ENABLE  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (_27 == 1) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SOIP_MODE  == "1") )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 40;
            _48 (  __context__  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 40;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_27 == 0))  ) ) 
                { 
                __context__.SourceCodeLine = 40;
                _38 (  __context__ , "SOIP_ENABLE", "NOT INITIALISED") ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 40;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SOIP_MODE  != "1"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 40;
                    _38 (  __context__ , "SOIP_ENABLE", "WRONG SOIP MODE") ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 40;
                    _49 (  __context__  ) ; 
                    } 
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DEBUG_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 40;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DEBUG  .Value == 1))  ) ) 
            { 
            __context__.SourceCodeLine = 40;
            _32 = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 40;
            _32 = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CRESTRON_ID_OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 40;
        _25  .UpdateValue ( _25 + CRESTRON_ID  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TX_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString _10;
        _10  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
        
        
        __context__.SourceCodeLine = 41;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_28 == 65535))  ) ) 
            { 
            __context__.SourceCodeLine = 41;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_27 == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 41;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SOIP_MODE  == "1"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 41;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SOIP_ENABLE  .Value == 1))  ) ) 
                        {
                        __context__.SourceCodeLine = 41;
                        _54 (  __context__ , TX) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 41;
                        _38 (  __context__ , "TX", "SOIP Disabled") ; 
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 41;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SOIP_MODE  == "3"))  ) ) 
                        { 
                        __context__.SourceCodeLine = 41;
                        MakeString ( _10 , "{0};  printf  '{1}'  > /dev/ttyS0", _61 (  __context__  ) , _63 (  __context__ , TX) ) ; 
                        __context__.SourceCodeLine = 41;
                        _59 (  __context__ , _10, "#", (ushort)( 0 )) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 41;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SOIP_MODE  == "4"))  ) ) 
                            { 
                            __context__.SourceCodeLine = 41;
                            MakeString ( _10 , "commandallrx.sh \"{0}; printf '{1}' > /dev/ttyS0\"", _61 (  __context__  ) , _63 (  __context__ , TX) ) ; 
                            __context__.SourceCodeLine = 41;
                            _59 (  __context__ , _10, "#", (ushort)( 0 )) ; 
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 41;
                            _38 (  __context__ , "TX", "SOIP MODE DOES UNSUPPORT THIS MODE") ; 
                            } 
                        
                        }
                    
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 41;
                _38 (  __context__ , "TX", "SOIP Not INITIALISED") ; 
                } 
            
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 41;
            _38 (  __context__ , "CHANGE TX", "Unlicenced or Module not Started") ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object JAP_DEV_TELNET_OnSocketConnect_5 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 42;
        _38 (  __context__ , "JAP_DEV_TELNET", "*** IP ONLINE ***") ; 
        __context__.SourceCodeLine = 42;
        CancelAllWait ( ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object JAP_DEV_CONTROL_OnSocketConnect_6 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 42;
        _38 (  __context__ , "JAP_DEV_CONTROL", "*** IP ONLINE ***") ; 
        __context__.SourceCodeLine = 42;
        _39 (  __context__ , "CONNECTED_6752") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object JAP_DEV_TELNET_OnSocketDisconnect_7 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 42;
        _38 (  __context__ , "JAP_DEV_TELNET", "*** IP OFFLINE ***") ; 
        __context__.SourceCodeLine = 42;
        _38 (  __context__ , "JAP_DEV_TELNET Configured =", Functions.ItoA( (int)( _27 ) )) ; 
        __context__.SourceCodeLine = 42;
        _37 = (ushort) ( 1 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object JAP_DEV_CONTROL_OnSocketDisconnect_8 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 42;
        _39 (  __context__ , "OFFLINE") ; 
        __context__.SourceCodeLine = 42;
        _38 (  __context__ , "JAP_DEV_CONTROL", "*** IP OFFLINE ***") ; 
        __context__.SourceCodeLine = 42;
        _38 (  __context__ , "JAP_DEV_CONTROL Configured =", Functions.ItoA( (int)( _27 ) )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

private void _70 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 42;
    if ( Functions.TestForTrue  ( ( _36)  ) ) 
        { 
        __context__.SourceCodeLine = 42;
        _70 (  __context__  ) ; 
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 42;
        _36 = (ushort) ( 65535 ) ; 
        __context__.SourceCodeLine = 42;
        while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _69( __context__ ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 42;
            } 
        
        __context__.SourceCodeLine = 43;
        _36 = (ushort) ( 0 ) ; 
        } 
    
    
    }
    
object JAP_DEV_TELNET_OnSocketReceive_9 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 43;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_36 == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 43;
            _36 = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 43;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _69( __context__ ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 43;
                } 
            
            __context__.SourceCodeLine = 43;
            _36 = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object JAP_DEV_CONTROL_OnSocketReceive_10 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 43;
        RX  .UpdateValue ( JAP_DEV_CONTROL .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 43;
        JAP_DEV_CONTROL .  SocketRxBuf  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 43;
        ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

private CrestronString _71 (  SplusExecutionContext __context__, ushort _73 ) 
    { 
    
    __context__.SourceCodeLine = 43;
    
        {
        int __SPLS_TMPVAR__SWTCH_2__ = ((int)_73);
        
            { 
            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 0) ) ) ) 
                { 
                __context__.SourceCodeLine = 43;
                return ( "Not connected" ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 1) ) ) ) 
                { 
                __context__.SourceCodeLine = 43;
                return ( "Waiting" ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 2) ) ) ) 
                { 
                __context__.SourceCodeLine = 43;
                return ( "Connected" ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 3) ) ) ) 
                { 
                __context__.SourceCodeLine = 43;
                return ( "Connection failed" ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 4) ) ) ) 
                { 
                __context__.SourceCodeLine = 43;
                return ( "Connection broken remotely" ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 5) ) ) ) 
                { 
                __context__.SourceCodeLine = 43;
                return ( "Connection broken locally" ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 6) ) ) ) 
                { 
                __context__.SourceCodeLine = 43;
                return ( "Performing DNS lookup" ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 7) ) ) ) 
                { 
                __context__.SourceCodeLine = 44;
                return ( "DNS lookup failed" ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 8) ) ) ) 
                { 
                __context__.SourceCodeLine = 44;
                return ( "DNS name resolved" ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 44;
                return ( "Unknown!" ) ; 
                } 
            
            } 
            
        }
        
    
    
    return ""; // default return value (none specified in module)
    }
    
object JAP_DEV_TELNET_OnSocketStatus_11 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 44;
        _26 = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 44;
        _38 (  __context__ , "JAP_DEV_TELNET", _71( __context__ , (ushort)( _26 ) )) ; 
        __context__.SourceCodeLine = 44;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_26 != 2))  ) ) 
            { 
            __context__.SourceCodeLine = 44;
            _37 = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object JAP_DEV_CONTROL_OnSocketStatus_12 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        short STATUS = 0;
        
        
        __context__.SourceCodeLine = 44;
        STATUS = (short) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 44;
        _38 (  __context__ , "JAP_DEV_CONTROL", _71( __context__ , (ushort)( STATUS ) )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object CONSOLE_CMD_OnChange_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 44;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_28 == 65535))  ) ) 
            { 
            __context__.SourceCodeLine = 44;
            _38 (  __context__ , "CHANGE CONSOLE_CMD", CONSOLE_CMD) ; 
            __context__.SourceCodeLine = 44;
            _59 (  __context__ , CONSOLE_CMD, "#", (ushort)( 0 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 44;
            _38 (  __context__ , "CHANGE CONSOLE_CMD", "Unlicenced or Module not Started") ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object AUDIO_DELAY_OnChange_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString _10;
        _10  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        CrestronString TMP;
        TMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        ushort AD = 0;
        
        
        __context__.SourceCodeLine = 45;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_28 == 65535))  ) ) 
            { 
            __context__.SourceCodeLine = 45;
            AD = (ushort) ( AUDIO_DELAY  .UshortValue ) ; 
            __context__.SourceCodeLine = 45;
            _38 (  __context__ , "CHANGE AUDIO_DELAY", Functions.ItoA( (int)( AD ) )) ; 
            __context__.SourceCodeLine = 45;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( AD > 25 ))  ) ) 
                { 
                __context__.SourceCodeLine = 45;
                AD = (ushort) ( 25 ) ; 
                __context__.SourceCodeLine = 45;
                MakeString ( TMP , "AUDIO_DELAY too large. Setting to {0}", Functions.ItoA (  (int) ( AD ) ) ) ; 
                __context__.SourceCodeLine = 45;
                _38 (  __context__ , "CHANGE AUDIO_DELAY", TMP) ; 
                } 
            
            __context__.SourceCodeLine = 45;
            MakeString ( _10 , "AUD_DLY_SET_VALUE {0}", Functions.ItoA (  (int) ( AD ) ) ) ; 
            __context__.SourceCodeLine = 45;
            _59 (  __context__ , _10, "#", (ushort)( 0 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 45;
            _38 (  __context__ , "CHANGE AUDIO_DELAY", "Unlicenced or Module not Started") ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TEAR_ADJUST_OnChange_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString _10;
        _10  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        CrestronString TMP;
        TMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        ushort TA = 0;
        
        
        __context__.SourceCodeLine = 45;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_28 == 65535))  ) ) 
            { 
            __context__.SourceCodeLine = 45;
            TA = (ushort) ( TEAR_ADJUST  .UshortValue ) ; 
            __context__.SourceCodeLine = 45;
            _38 (  __context__ , "CHANGE TEAR_ADJUST", Functions.ItoA( (int)( TA ) )) ; 
            __context__.SourceCodeLine = 46;
            MakeString ( _10 , "e e_vw_delay_kick_{0}", Functions.ItoA (  (int) ( TA ) ) ) ; 
            __context__.SourceCodeLine = 46;
            _59 (  __context__ , _10, "#", (ushort)( 0 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 46;
            _38 (  __context__ , "CHANGE TEAR_ADJUST", "Unlicenced or Module not Started") ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 46;
        _32 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 46;
        _27 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 46;
        _25  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 46;
        _24  .UpdateValue ( "OFFLINE"  ) ; 
        __context__.SourceCodeLine = 46;
        _28 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 46;
        _36 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 46;
        _37 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 46;
        _26 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 46;
        _15 . _10  .UpdateValue ( "NULL STRING"  ) ; 
        __context__.SourceCodeLine = 46;
        _15 . _11  .UpdateValue ( "NULL STRING"  ) ; 
        __context__.SourceCodeLine = 46;
        _15 . _12 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 46;
        _15 . _13  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 46;
        _15 . _14 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 46;
        _38 (  __context__ , "Main", "Starting") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    _19  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 6, this );
    _20  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5, this );
    _21  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5, this );
    _22  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5, this );
    _23  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5, this );
    _24  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, this );
    _25  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
    _31  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    JAP_DEV_TELNET  = new SplusTcpClient ( 65533, this );
    JAP_DEV_CONTROL  = new SplusTcpClient ( 65533, this );
    _15  = new _9( this, true );
    _15 .PopulateCustomAttributeList( false );
    _17  = new _9( this, true );
    _17 .PopulateCustomAttributeList( false );
    _16  = new _9[ 1001 ];
    for( uint i = 0; i < 1001; i++ )
    {
        _16 [i] = new _9( this, true );
        _16 [i].PopulateCustomAttributeList( false );
        
    }
    
    SOIP_ENABLE = new Crestron.Logos.SplusObjects.DigitalInput( SOIP_ENABLE__DigitalInput__, this );
    m_DigitalInputList.Add( SOIP_ENABLE__DigitalInput__, SOIP_ENABLE );
    
    START_MODULE = new Crestron.Logos.SplusObjects.DigitalInput( START_MODULE__DigitalInput__, this );
    m_DigitalInputList.Add( START_MODULE__DigitalInput__, START_MODULE );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    AUDIO_DELAY = new Crestron.Logos.SplusObjects.AnalogInput( AUDIO_DELAY__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AUDIO_DELAY__AnalogSerialInput__, AUDIO_DELAY );
    
    TEAR_ADJUST = new Crestron.Logos.SplusObjects.AnalogInput( TEAR_ADJUST__AnalogSerialInput__, this );
    m_AnalogInputList.Add( TEAR_ADJUST__AnalogSerialInput__, TEAR_ADJUST );
    
    TX = new Crestron.Logos.SplusObjects.StringInput( TX__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( TX__AnalogSerialInput__, TX );
    
    CRESTRON_ID = new Crestron.Logos.SplusObjects.StringInput( CRESTRON_ID__AnalogSerialInput__, 100, this );
    m_StringInputList.Add( CRESTRON_ID__AnalogSerialInput__, CRESTRON_ID );
    
    CONSOLE_CMD = new Crestron.Logos.SplusObjects.StringInput( CONSOLE_CMD__AnalogSerialInput__, 250, this );
    m_StringInputList.Add( CONSOLE_CMD__AnalogSerialInput__, CONSOLE_CMD );
    
    RX = new Crestron.Logos.SplusObjects.StringOutput( RX__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RX__AnalogSerialOutput__, RX );
    
    VERSION__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( VERSION__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( VERSION__DOLLAR____AnalogSerialOutput__, VERSION__DOLLAR__ );
    
    IP_ADDRESS = new StringParameter( IP_ADDRESS__Parameter__, this );
    m_ParameterList.Add( IP_ADDRESS__Parameter__, IP_ADDRESS );
    
    SOIP_MODE = new StringParameter( SOIP_MODE__Parameter__, this );
    m_ParameterList.Add( SOIP_MODE__Parameter__, SOIP_MODE );
    
    BAUD_RATE = new StringParameter( BAUD_RATE__Parameter__, this );
    m_ParameterList.Add( BAUD_RATE__Parameter__, BAUD_RATE );
    
    DATA_BIT = new StringParameter( DATA_BIT__Parameter__, this );
    m_ParameterList.Add( DATA_BIT__Parameter__, DATA_BIT );
    
    PARITY = new StringParameter( PARITY__Parameter__, this );
    m_ParameterList.Add( PARITY__Parameter__, PARITY );
    
    STOP_BIT = new StringParameter( STOP_BIT__Parameter__, this );
    m_ParameterList.Add( STOP_BIT__Parameter__, STOP_BIT );
    
    LICENCE_KEY = new StringParameter( LICENCE_KEY__Parameter__, this );
    m_ParameterList.Add( LICENCE_KEY__Parameter__, LICENCE_KEY );
    
    __SPLS_TMPVAR__WAITLABEL_21___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_21___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_22___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_22___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_23___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_23___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_24___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_24___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_25___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_25___CallbackFn );
    
    START_MODULE.OnDigitalPush.Add( new InputChangeHandlerWrapper( START_MODULE_OnPush_0, false ) );
    SOIP_ENABLE.OnDigitalChange.Add( new InputChangeHandlerWrapper( SOIP_ENABLE_OnChange_1, false ) );
    DEBUG.OnDigitalChange.Add( new InputChangeHandlerWrapper( DEBUG_OnChange_2, false ) );
    CRESTRON_ID.OnSerialChange.Add( new InputChangeHandlerWrapper( CRESTRON_ID_OnChange_3, false ) );
    TX.OnSerialChange.Add( new InputChangeHandlerWrapper( TX_OnChange_4, false ) );
    JAP_DEV_TELNET.OnSocketConnect.Add( new SocketHandlerWrapper( JAP_DEV_TELNET_OnSocketConnect_5, false ) );
    JAP_DEV_CONTROL.OnSocketConnect.Add( new SocketHandlerWrapper( JAP_DEV_CONTROL_OnSocketConnect_6, false ) );
    JAP_DEV_TELNET.OnSocketDisconnect.Add( new SocketHandlerWrapper( JAP_DEV_TELNET_OnSocketDisconnect_7, false ) );
    JAP_DEV_CONTROL.OnSocketDisconnect.Add( new SocketHandlerWrapper( JAP_DEV_CONTROL_OnSocketDisconnect_8, false ) );
    JAP_DEV_TELNET.OnSocketReceive.Add( new SocketHandlerWrapper( JAP_DEV_TELNET_OnSocketReceive_9, false ) );
    JAP_DEV_CONTROL.OnSocketReceive.Add( new SocketHandlerWrapper( JAP_DEV_CONTROL_OnSocketReceive_10, false ) );
    JAP_DEV_TELNET.OnSocketStatus.Add( new SocketHandlerWrapper( JAP_DEV_TELNET_OnSocketStatus_11, false ) );
    JAP_DEV_CONTROL.OnSocketStatus.Add( new SocketHandlerWrapper( JAP_DEV_CONTROL_OnSocketStatus_12, false ) );
    CONSOLE_CMD.OnSerialChange.Add( new InputChangeHandlerWrapper( CONSOLE_CMD_OnChange_13, false ) );
    AUDIO_DELAY.OnAnalogChange.Add( new InputChangeHandlerWrapper( AUDIO_DELAY_OnChange_14, false ) );
    TEAR_ADJUST.OnAnalogChange.Add( new InputChangeHandlerWrapper( TEAR_ADJUST_OnChange_15, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_JAD_TXRXCOMMS_V2_10 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_21___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_22___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_23___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_24___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_25___Callback;


const uint SOIP_ENABLE__DigitalInput__ = 0;
const uint START_MODULE__DigitalInput__ = 1;
const uint DEBUG__DigitalInput__ = 2;
const uint TX__AnalogSerialInput__ = 0;
const uint CRESTRON_ID__AnalogSerialInput__ = 1;
const uint CONSOLE_CMD__AnalogSerialInput__ = 2;
const uint AUDIO_DELAY__AnalogSerialInput__ = 3;
const uint TEAR_ADJUST__AnalogSerialInput__ = 4;
const uint RX__AnalogSerialOutput__ = 0;
const uint VERSION__DOLLAR____AnalogSerialOutput__ = 1;
const uint IP_ADDRESS__Parameter__ = 10;
const uint SOIP_MODE__Parameter__ = 11;
const uint BAUD_RATE__Parameter__ = 12;
const uint DATA_BIT__Parameter__ = 13;
const uint PARITY__Parameter__ = 14;
const uint STOP_BIT__Parameter__ = 15;
const uint LICENCE_KEY__Parameter__ = 16;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class _9 : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public CrestronString  _10;
    
    [SplusStructAttribute(1, false, false)]
    public CrestronString  _11;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  _12 = 0;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  _13;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  _14 = 0;
    
    
    public _9( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        _10  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, Owner );
        _11  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, Owner );
        _13  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, Owner );
        
        
    }
    
}

}

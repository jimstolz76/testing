using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_JAP_AVSWITCH_CISCO_V2_03
{
    public class UserModuleClass_JAP_AVSWITCH_CISCO_V2_03 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        private ushort _12 (  SplusExecutionContext __context__, ushort _14 ) 
            { 
            
            __context__.SourceCodeLine = 3;
            return (ushort)( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _14 >= 64 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _14 <= 90 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _14 >= 96 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _14 <= 122 ) )) ) )) )) ; 
            
            }
            
        private ushort _13 (  SplusExecutionContext __context__, ushort _15 ) 
            { 
            
            __context__.SourceCodeLine = 3;
            return (ushort)( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _15 < 32 ) ) || Functions.TestForTrue ( Functions.BoolToInt (_15 == 127) )) )) ; 
            
            }
            
        private ushort _14 (  SplusExecutionContext __context__, ushort _16 ) 
            { 
            
            __context__.SourceCodeLine = 3;
            return (ushort)( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _16 >= 48 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _16 <= 57 ) )) )) ; 
            
            }
            
        private ushort _15 (  SplusExecutionContext __context__, ushort _17 ) 
            { 
            
            __context__.SourceCodeLine = 3;
            return (ushort)( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _17 >= 96 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _17 <= 122 ) )) )) ; 
            
            }
            
        private ushort _16 (  SplusExecutionContext __context__, ushort _18 ) 
            { 
            
            __context__.SourceCodeLine = 4;
            return (ushort)( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _18 >= 33 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _18 <= 47 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _18 >= 58 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _18 <= 64 ) )) ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _18 >= 91 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _18 <= 96 ) )) ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _18 >= 123 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _18 <= 126 ) )) ) )) )) ; 
            
            }
            
        private ushort _17 (  SplusExecutionContext __context__, ushort _19 ) 
            { 
            
            __context__.SourceCodeLine = 4;
            return (ushort)( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_19 == 9) ) || Functions.TestForTrue ( Functions.BoolToInt (_19 == 10) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (_19 == 13) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (_19 == 32) )) )) ; 
            
            }
            
        private ushort _18 (  SplusExecutionContext __context__, ushort _20 ) 
            { 
            
            __context__.SourceCodeLine = 4;
            return (ushort)( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _20 >= 64 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _20 <= 90 ) )) )) ; 
            
            }
            
        private ushort _19 (  SplusExecutionContext __context__, ushort _21 ) 
            { 
            
            __context__.SourceCodeLine = 4;
            return (ushort)( Functions.BoolToInt ( (Functions.TestForTrue ( _14( __context__ , (ushort)( _21 ) ) ) || Functions.TestForTrue ( _12( __context__ , (ushort)( _21 ) ) )) )) ; 
            
            }
            
        private ushort _20 (  SplusExecutionContext __context__, ushort _22 ) 
            { 
            
            __context__.SourceCodeLine = 4;
            return (ushort)( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( _14( __context__ , (ushort)( _22 ) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _22 >= 65 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _22 <= 70 ) )) ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _22 >= 97 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _22 <= 102 ) )) ) )) )) ; 
            
            }
            
        private ushort _21 (  SplusExecutionContext __context__, ref _0 _23 , ushort _24 , CrestronString _5 , ushort _25 ) 
            { 
            ushort _26 = 0;
            
            ushort _27 = 0;
            
            
            __context__.SourceCodeLine = 5;
            _23 . _5  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 5;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( _5 ) > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt (Byte( _5 , (int)( 1 ) ) == 94) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 5;
                _26 = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 5;
                _25 = (ushort) ( (_25 - 1) ) ; 
                __context__.SourceCodeLine = 5;
                _23 . _5  .UpdateValue ( Functions.Right ( _5 ,  (int) ( _25 ) )  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 5;
                _26 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 5;
                _23 . _5  .UpdateValue ( _5  ) ; 
                } 
            
            __context__.SourceCodeLine = 5;
            _23 . _1  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 5;
            _23 . _2 = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 5;
            _23 . _6 = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 5;
            _23 . _7 = (ushort) ( (_24 + 1) ) ; 
            __context__.SourceCodeLine = 5;
            _23 . _8 = (ushort) ( (_25 + 1) ) ; 
            __context__.SourceCodeLine = 5;
            _23 . _9 = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 5;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)(10 - 1); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( _27  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_27  >= __FN_FORSTART_VAL__1) && (_27  <= __FN_FOREND_VAL__1) ) : ( (_27  <= __FN_FORSTART_VAL__1) && (_27  >= __FN_FOREND_VAL__1) ) ; _27  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 5;
                _23 . _3 [ _27] = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 5;
                _23 . _4 [ _27] = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 5;
                _23 . _10 [ _27] = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 5;
                _23 . _11 [ _27] = (short) ( 0 ) ; 
                __context__.SourceCodeLine = 5;
                } 
            
            __context__.SourceCodeLine = 5;
            return (ushort)( _26) ; 
            
            }
            
        private ushort _22 (  SplusExecutionContext __context__, ref _0 _24 , ushort _25 , CrestronString _26 ) 
            { 
            
            __context__.SourceCodeLine = 5;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (_24._2 + _25) > 10 ))  ) ) 
                { 
                __context__.SourceCodeLine = 5;
                _25 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 5;
                _24 . _1  .UpdateValue ( _26  ) ; 
                } 
            
            __context__.SourceCodeLine = 5;
            return (ushort)( _25) ; 
            
            }
            
        private ushort _23 (  SplusExecutionContext __context__, ref _0 _25 , ushort _26 , ushort _27 ) 
            { 
            ushort _28 = 0;
            
            ushort _3 = 0;
            
            
            __context__.SourceCodeLine = 5;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _25._2 >= 10 ))  ) ) 
                { 
                __context__.SourceCodeLine = 5;
                _25 . _1  .UpdateValue ( "stack overflow"  ) ; 
                __context__.SourceCodeLine = 5;
                _28 = (ushort) ( 0 ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 6;
                _3 = (ushort) ( ((_26 - _25._6) + 1) ) ; 
                __context__.SourceCodeLine = 6;
                _25 . _3 [ _25._2] = (ushort) ( _3 ) ; 
                __context__.SourceCodeLine = 6;
                _25 . _4 [ _25._2] = (ushort) ( (_3 + (_27 - _26)) ) ; 
                __context__.SourceCodeLine = 6;
                _25 . _2 = (ushort) ( (_25._2 + 1) ) ; 
                __context__.SourceCodeLine = 6;
                _28 = (ushort) ( 1 ) ; 
                } 
            
            __context__.SourceCodeLine = 6;
            return (ushort)( _28) ; 
            
            }
            
        private ushort _24 (  SplusExecutionContext __context__, ref _0 _26 , ushort _27 ) 
            { 
            
            __context__.SourceCodeLine = 6;
            _27 = (ushort) ( (_27 - 49) ) ; 
            __context__.SourceCodeLine = 6;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _27 < 0 ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _27 >= _26._9 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (_26._11[ _27 ] == Functions.ToSignedLongInteger( -( 1 ) )) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 6;
                _26 . _1  .UpdateValue ( "invalid capture index"  ) ; 
                __context__.SourceCodeLine = 6;
                _27 = (ushort) ( 0 ) ; 
                } 
            
            __context__.SourceCodeLine = 6;
            return (ushort)( _27) ; 
            
            }
            
        private short _25 (  SplusExecutionContext __context__, ref _0 _27 ) 
            { 
            ushort _9 = 0;
            
            
            __context__.SourceCodeLine = 6;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( _27._9 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)0; 
            int __FN_FORSTEP_VAL__1 = (int)Functions.ToLongInteger( -( 1 ) ); 
            for ( _9  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_9  >= __FN_FORSTART_VAL__1) && (_9  <= __FN_FOREND_VAL__1) ) : ( (_9  <= __FN_FORSTART_VAL__1) && (_9  >= __FN_FOREND_VAL__1) ) ; _9  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 6;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_27._11[ _9 ] == Functions.ToSignedLongInteger( -( 1 ) )))  ) ) 
                    {
                    __context__.SourceCodeLine = 6;
                    return (short)( _9) ; 
                    }
                
                __context__.SourceCodeLine = 6;
                } 
            
            __context__.SourceCodeLine = 6;
            _27 . _1  .UpdateValue ( "invalid pattern capture"  ) ; 
            __context__.SourceCodeLine = 6;
            return (short)( Functions.ToSignedInteger( -( 1 ) )) ; 
            
            }
            
        private ushort _26 (  SplusExecutionContext __context__, ref _0 _28 , ushort _29 ) 
            { 
            
            __context__.SourceCodeLine = 6;
            _29 = (ushort) ( (_29 + 1) ) ; 
            __context__.SourceCodeLine = 6;
            
                {
                int __SPLS_TMPVAR__SWTCH_1__ = ((int)Byte( _28._5 , (int)( (_29 - 1) ) ));
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 37) ) ) ) 
                        { 
                        __context__.SourceCodeLine = 7;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_29 == _28._8))  ) ) 
                            { 
                            __context__.SourceCodeLine = 7;
                            _28 . _1  .UpdateValue ( "malformed pattern (ends with %)"  ) ; 
                            __context__.SourceCodeLine = 7;
                            return (ushort)( _29) ; 
                            } 
                        
                        __context__.SourceCodeLine = 7;
                        return (ushort)( (_29 + 1)) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 91) ) ) ) 
                        { 
                        __context__.SourceCodeLine = 7;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _29 < _28._8 ) ) && Functions.TestForTrue ( Functions.BoolToInt (Byte( _28._5 , (int)( _29 ) ) == 94) )) ))  ) ) 
                            {
                            __context__.SourceCodeLine = 7;
                            _29 = (ushort) ( (_29 + 1) ) ; 
                            }
                        
                        __context__.SourceCodeLine = 7;
                        do 
                            { 
                            __context__.SourceCodeLine = 7;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_29 == _28._8))  ) ) 
                                { 
                                __context__.SourceCodeLine = 7;
                                _28 . _1  .UpdateValue ( "malformed pattern (missing ])"  ) ; 
                                __context__.SourceCodeLine = 7;
                                return (ushort)( _29) ; 
                                } 
                            
                            __context__.SourceCodeLine = 7;
                            _29 = (ushort) ( (_29 + 1) ) ; 
                            __context__.SourceCodeLine = 7;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Byte( _28._5 , (int)( (_29 - 1) ) ) == 37) ) && Functions.TestForTrue ( Functions.BoolToInt ( _29 < _28._8 ) )) ))  ) ) 
                                {
                                __context__.SourceCodeLine = 7;
                                _29 = (ushort) ( (_29 + 1) ) ; 
                                }
                            
                            } 
                        while (false == ( Functions.TestForTrue  ( Functions.BoolToInt (Byte( _28._5 , (int)( _29 ) ) == 93)) )); 
                        __context__.SourceCodeLine = 7;
                        return (ushort)( (_29 + 1)) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 7;
                        return (ushort)( _29) ; 
                        } 
                    
                    } 
                    
                }
                
            
            
            return 0; // default return value (none specified in module)
            }
            
        private ushort _27 (  SplusExecutionContext __context__, ushort _29 , ushort _30 ) 
            { 
            ushort _31 = 0;
            
            
            __context__.SourceCodeLine = 7;
            
                {
                int __SPLS_TMPVAR__SWTCH_2__ = ((int)Functions.LowerChar( (uint)( _30 ) ));
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == (  (uint) ( 97 ) ) ) ) ) 
                        {
                        __context__.SourceCodeLine = 7;
                        _31 = (ushort) ( _12( __context__ , (ushort)( _29 ) ) ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 99) ) ) ) 
                        {
                        __context__.SourceCodeLine = 7;
                        _31 = (ushort) ( _13( __context__ , (ushort)( _29 ) ) ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 100) ) ) ) 
                        {
                        __context__.SourceCodeLine = 7;
                        _31 = (ushort) ( _14( __context__ , (ushort)( _29 ) ) ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 108) ) ) ) 
                        {
                        __context__.SourceCodeLine = 8;
                        _31 = (ushort) ( _15( __context__ , (ushort)( _29 ) ) ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 112) ) ) ) 
                        {
                        __context__.SourceCodeLine = 8;
                        _31 = (ushort) ( _16( __context__ , (ushort)( _29 ) ) ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 115) ) ) ) 
                        {
                        __context__.SourceCodeLine = 8;
                        _31 = (ushort) ( _17( __context__ , (ushort)( _29 ) ) ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 117) ) ) ) 
                        {
                        __context__.SourceCodeLine = 8;
                        _31 = (ushort) ( _18( __context__ , (ushort)( _29 ) ) ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 119) ) ) ) 
                        {
                        __context__.SourceCodeLine = 8;
                        _31 = (ushort) ( _19( __context__ , (ushort)( _29 ) ) ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 120) ) ) ) 
                        {
                        __context__.SourceCodeLine = 8;
                        _31 = (ushort) ( _20( __context__ , (ushort)( _29 ) ) ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 122) ) ) ) 
                        {
                        __context__.SourceCodeLine = 8;
                        return (ushort)( Functions.BoolToInt (_29 == 0)) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 8;
                        return (ushort)( Functions.BoolToInt (_30 == _29)) ; 
                        }
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 8;
            if ( Functions.TestForTrue  ( ( _18( __context__ , (ushort)( _30 ) ))  ) ) 
                {
                __context__.SourceCodeLine = 8;
                _31 = (ushort) ( Functions.Not( _31 ) ) ; 
                }
            
            __context__.SourceCodeLine = 8;
            return (ushort)( _31) ; 
            
            }
            
        private ushort _28 (  SplusExecutionContext __context__, ref _0 _30 , ushort _31 , ushort _32 , ushort _33 ) 
            { 
            ushort _34 = 0;
            
            
            __context__.SourceCodeLine = 8;
            _34 = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 8;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (_32 + 1) < _30._8 ) ) && Functions.TestForTrue ( Functions.BoolToInt (Byte( _30._5 , (int)( (_32 + 1) ) ) == 94) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 8;
                _34 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 8;
                _32 = (ushort) ( (_32 + 1) ) ; 
                } 
            
            __context__.SourceCodeLine = 8;
            _32 = (ushort) ( (_32 + 1) ) ; 
            __context__.SourceCodeLine = 8;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _32 < _33 ))  ) ) 
                { 
                __context__.SourceCodeLine = 8;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( _30._5 , (int)( _32 ) ) == 37))  ) ) 
                    { 
                    __context__.SourceCodeLine = 8;
                    _32 = (ushort) ( (_32 + 1) ) ; 
                    __context__.SourceCodeLine = 8;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _32 < _30._8 ) ) && Functions.TestForTrue ( _27( __context__ , (ushort)( _31 ) , (ushort)( Byte( _30._5 , (int)( _32 ) ) ) ) )) ))  ) ) 
                        {
                        __context__.SourceCodeLine = 8;
                        return (ushort)( _34) ; 
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 9;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (_32 + 1) < _30._8 ) ) && Functions.TestForTrue ( Functions.BoolToInt (Byte( _30._5 , (int)( (_32 + 1) ) ) == 45) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( (_32 + 2) < _33 ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 9;
                        _32 = (ushort) ( (_32 + 2) ) ; 
                        __context__.SourceCodeLine = 9;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Byte( _30._5 , (int)( (_32 - 2) ) ) <= _31 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _31 <= Byte( _30._5 , (int)( _32 ) ) ) )) ))  ) ) 
                            {
                            __context__.SourceCodeLine = 9;
                            return (ushort)( _34) ; 
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 9;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( _30._5 , (int)( _32 ) ) == _31))  ) ) 
                            {
                            __context__.SourceCodeLine = 9;
                            return (ushort)( _34) ; 
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 9;
                _32 = (ushort) ( (_32 + 1) ) ; 
                __context__.SourceCodeLine = 8;
                } 
            
            __context__.SourceCodeLine = 9;
            return (ushort)( Functions.Not( _34 )) ; 
            
            }
            
        private ushort _29 (  SplusExecutionContext __context__, ref _0 _31 , ushort _32 , ushort _33 , ushort _34 ) 
            { 
            
            __context__.SourceCodeLine = 9;
            
                {
                int __SPLS_TMPVAR__SWTCH_3__ = ((int)Byte( _31._5 , (int)( _33 ) ));
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 46) ) ) ) 
                        {
                        __context__.SourceCodeLine = 9;
                        return (ushort)( 1) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 37) ) ) ) 
                        {
                        __context__.SourceCodeLine = 9;
                        return (ushort)( _27( __context__ , (ushort)( _32 ) , (ushort)( Byte( _31._5 , (int)( (_33 + 1) ) ) ) )) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 91) ) ) ) 
                        {
                        __context__.SourceCodeLine = 9;
                        return (ushort)( _28( __context__ , ref _31 , (ushort)( _32 ) , (ushort)( _33 ) , (ushort)( (_34 - 1) ) )) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 9;
                        return (ushort)( Functions.BoolToInt (Byte( _31._5 , (int)( _33 ) ) == _32)) ; 
                        }
                    
                    } 
                    
                }
                
            
            
            return 0; // default return value (none specified in module)
            }
            
        private ushort _30 (  SplusExecutionContext __context__, ref _0 _32 , CrestronString _33 , ushort _34 , ushort _35 ) 
            { 
            ushort _36 = 0;
            
            ushort _37 = 0;
            
            ushort _38 = 0;
            
            
            __context__.SourceCodeLine = 10;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _35 >= (_32._8 - 1) ))  ) ) 
                { 
                __context__.SourceCodeLine = 10;
                _32 . _1  .UpdateValue ( "malformed pattern (missing arguments to %b)"  ) ; 
                __context__.SourceCodeLine = 10;
                return (ushort)( 0) ; 
                } 
            
            __context__.SourceCodeLine = 10;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( _33 , (int)( _34 ) ) != Byte( _32._5 , (int)( _35 ) )))  ) ) 
                {
                __context__.SourceCodeLine = 10;
                return (ushort)( 0) ; 
                }
            
            else 
                { 
                __context__.SourceCodeLine = 10;
                _36 = (ushort) ( Byte( _32._5 , (int)( _35 ) ) ) ; 
                __context__.SourceCodeLine = 10;
                _37 = (ushort) ( Byte( _32._5 , (int)( (_35 + 1) ) ) ) ; 
                __context__.SourceCodeLine = 10;
                _38 = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 10;
                _34 = (ushort) ( (_34 + 1) ) ; 
                __context__.SourceCodeLine = 10;
                while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _34 < _32._7 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 10;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( _33 , (int)( _34 ) ) == _37))  ) ) 
                        { 
                        __context__.SourceCodeLine = 10;
                        _38 = (ushort) ( (_38 - 1) ) ; 
                        __context__.SourceCodeLine = 10;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_38 == 0))  ) ) 
                            {
                            __context__.SourceCodeLine = 10;
                            return (ushort)( (_34 + 1)) ; 
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 10;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( _33 , (int)( _34 ) ) == _36))  ) ) 
                            {
                            __context__.SourceCodeLine = 10;
                            _38 = (ushort) ( (_38 + 1) ) ; 
                            }
                        
                        }
                    
                    __context__.SourceCodeLine = 10;
                    _34 = (ushort) ( (_34 + 1) ) ; 
                    __context__.SourceCodeLine = 10;
                    } 
                
                } 
            
            __context__.SourceCodeLine = 10;
            return (ushort)( 0) ; 
            
            }
            
        private ushort _31 (  SplusExecutionContext __context__, ref _0 _33 , CrestronString _34 , ushort _35 , ushort _36 ) 
            { 
            ushort _11 = 0;
            
            
            __context__.SourceCodeLine = 11;
            _36 = (ushort) ( _24( __context__ , ref _33 , (ushort)( _36 ) ) ) ; 
            __context__.SourceCodeLine = 11;
            _11 = (ushort) ( _33._11[ _36 ] ) ; 
            __context__.SourceCodeLine = 11;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (_33._7 - _35) >= _11 ) ) && Functions.TestForTrue ( Functions.BoolToInt (Functions.Mid( _34 , (int)( _33._10[ _36 ] ) , (int)( _11 ) ) == Functions.Mid( _34 , (int)( _35 ) , (int)( _11 ) )) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 11;
                return (ushort)( (_35 + _11)) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 11;
                return (ushort)( 0) ; 
                }
            
            
            return 0; // default return value (none specified in module)
            }
            
        private ushort _32 (  SplusExecutionContext __context__, ref _0 _34 , CrestronString _35 , ushort _36 , ushort _37 ) 
            { 
            ushort _38 = 0;
            
            ushort _39 = 0;
            
            short _40 = 0;
            
            ushort _41 = 0;
            
            ushort _42 = 0;
            
            short _43 = 0;
            
            ushort _44 = 0;
            
            
            __context__.SourceCodeLine = 11;
            while ( Functions.TestForTrue  ( ( 1)  ) ) 
                { 
                __context__.SourceCodeLine = 11;
                _41 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 11;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_37 == _34._8))  ) ) 
                    { 
                    __context__.SourceCodeLine = 11;
                    return (ushort)( _36) ; 
                    } 
                
                __context__.SourceCodeLine = 11;
                
                    {
                    int __SPLS_TMPVAR__SWTCH_4__ = ((int)Byte( _34._5 , (int)( _37 ) ));
                    
                        { 
                        if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 40) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 11;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( _34._5 , (int)( (_37 + 1) ) ) == 41))  ) ) 
                                { 
                                __context__.SourceCodeLine = 11;
                                _40 = (short) ( Functions.ToSignedInteger( -( 2 ) ) ) ; 
                                __context__.SourceCodeLine = 11;
                                _37 = (ushort) ( (_37 + 2) ) ; 
                                } 
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 11;
                                _40 = (short) ( Functions.ToSignedInteger( -( 1 ) ) ) ; 
                                __context__.SourceCodeLine = 11;
                                _37 = (ushort) ( (_37 + 1) ) ; 
                                } 
                            
                            __context__.SourceCodeLine = 11;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _34._9 >= 10 ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 11;
                                _34 . _1  .UpdateValue ( "too many captures"  ) ; 
                                __context__.SourceCodeLine = 11;
                                return (ushort)( 0) ; 
                                } 
                            
                            __context__.SourceCodeLine = 12;
                            _34 . _10 [ _34._9] = (ushort) ( _36 ) ; 
                            __context__.SourceCodeLine = 12;
                            _34 . _11 [ _34._9] = (short) ( _40 ) ; 
                            __context__.SourceCodeLine = 12;
                            _34 . _9 = (ushort) ( (_34._9 + 1) ) ; 
                            __context__.SourceCodeLine = 12;
                            _44 = (ushort) ( _32( __context__ , ref _34 , _35 , (ushort)( _36 ) , (ushort)( _37 ) ) ) ; 
                            __context__.SourceCodeLine = 12;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_44 == 0))  ) ) 
                                {
                                __context__.SourceCodeLine = 12;
                                _34 . _9 = (ushort) ( (_34._9 - 1) ) ; 
                                }
                            
                            __context__.SourceCodeLine = 12;
                            return (ushort)( _44) ; 
                            } 
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 41) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 12;
                            _43 = (short) ( _25( __context__ , ref _34 ) ) ; 
                            __context__.SourceCodeLine = 12;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _43 < 0 ))  ) ) 
                                {
                                __context__.SourceCodeLine = 12;
                                return (ushort)( 0) ; 
                                }
                            
                            __context__.SourceCodeLine = 12;
                            _34 . _11 [ _43] = (short) ( (_36 - _34._10[ _43 ]) ) ; 
                            __context__.SourceCodeLine = 12;
                            _44 = (ushort) ( _32( __context__ , ref _34 , _35 , (ushort)( _36 ) , (ushort)( (_37 + 1) ) ) ) ; 
                            __context__.SourceCodeLine = 12;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_44 == 0))  ) ) 
                                {
                                __context__.SourceCodeLine = 12;
                                _34 . _11 [ _43] = (short) ( Functions.ToInteger( -( 1 ) ) ) ; 
                                }
                            
                            __context__.SourceCodeLine = 12;
                            return (ushort)( _44) ; 
                            } 
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 36) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 12;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((_37 + 1) == _34._8))  ) ) 
                                { 
                                __context__.SourceCodeLine = 12;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_36 == _34._7))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 12;
                                    return (ushort)( _36) ; 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 12;
                                    return (ushort)( 0) ; 
                                    } 
                                
                                } 
                            
                            } 
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 37) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 12;
                            
                                {
                                int __SPLS_TMPVAR__SWTCH_5__ = ((int)Byte( _34._5 , (int)( (_37 + 1) ) ));
                                
                                    { 
                                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_5__ == ( 98) ) ) ) 
                                        { 
                                        __context__.SourceCodeLine = 12;
                                        _36 = (ushort) ( _30( __context__ , ref _34 , _35 , (ushort)( _36 ) , (ushort)( (_37 + 2) ) ) ) ; 
                                        __context__.SourceCodeLine = 12;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_36 == 0))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 12;
                                            return (ushort)( 0) ; 
                                            } 
                                        
                                        __context__.SourceCodeLine = 12;
                                        _37 = (ushort) ( (_37 + 4) ) ; 
                                        __context__.SourceCodeLine = 12;
                                        _41 = (ushort) ( 1 ) ; 
                                        } 
                                    
                                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_5__ == ( 102) ) ) ) 
                                        { 
                                        __context__.SourceCodeLine = 12;
                                        _37 = (ushort) ( (_37 + 2) ) ; 
                                        __context__.SourceCodeLine = 12;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( _34._5 , (int)( _37 ) ) != 91))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 13;
                                            _34 . _1  .UpdateValue ( "missing [ after %f in pattern"  ) ; 
                                            __context__.SourceCodeLine = 13;
                                            return (ushort)( 0) ; 
                                            } 
                                        
                                        __context__.SourceCodeLine = 13;
                                        _38 = (ushort) ( _26( __context__ , ref _34 , (ushort)( _37 ) ) ) ; 
                                        __context__.SourceCodeLine = 13;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_36 == _34._6))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 13;
                                            _39 = (ushort) ( 0 ) ; 
                                            }
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 13;
                                            _39 = (ushort) ( Byte( _35 , (int)( (_36 - 1) ) ) ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 13;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( _28( __context__ , ref _34 , (ushort)( _39 ) , (ushort)( _37 ) , (ushort)( (_38 - 1) ) ) ) || Functions.TestForTrue ( Functions.Not( _28( __context__ , ref _34 , (ushort)( Byte( _35 , (int)( _36 ) ) ) , (ushort)( _37 ) , (ushort)( (_38 - 1) ) ) ) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 13;
                                            return (ushort)( 0) ; 
                                            } 
                                        
                                        __context__.SourceCodeLine = 13;
                                        _37 = (ushort) ( _38 ) ; 
                                        __context__.SourceCodeLine = 13;
                                        _41 = (ushort) ( 1 ) ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 13;
                                        if ( Functions.TestForTrue  ( ( _14( __context__ , (ushort)( Byte( _34._5 , (int)( (_37 + 1) ) ) ) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 13;
                                            _36 = (ushort) ( _31( __context__ , ref _34 , _35 , (ushort)( _36 ) , (ushort)( Byte( _34._5 , (int)( (_37 + 1) ) ) ) ) ) ; 
                                            __context__.SourceCodeLine = 13;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_36 == 0))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 13;
                                                return (ushort)( 0) ; 
                                                } 
                                            
                                            __context__.SourceCodeLine = 13;
                                            _37 = (ushort) ( (_37 + 2) ) ; 
                                            __context__.SourceCodeLine = 13;
                                            _41 = (ushort) ( 1 ) ; 
                                            } 
                                        
                                        } 
                                    
                                    } 
                                    
                                }
                                
                            
                            } 
                        
                        else 
                            { 
                            } 
                        
                        } 
                        
                    }
                    
                
                __context__.SourceCodeLine = 13;
                if ( Functions.TestForTrue  ( ( Functions.Not( _41 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 13;
                    _38 = (ushort) ( _26( __context__ , ref _34 , (ushort)( _37 ) ) ) ; 
                    __context__.SourceCodeLine = 13;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _36 < _34._7 ) ) && Functions.TestForTrue ( _29( __context__ , ref _34 , (ushort)( Byte( _35 , (int)( _36 ) ) ) , (ushort)( _37 ) , (ushort)( _38 ) ) )) ))  ) ) 
                        {
                        __context__.SourceCodeLine = 13;
                        _40 = (short) ( 1 ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 13;
                        _40 = (short) ( 0 ) ; 
                        }
                    
                    __context__.SourceCodeLine = 13;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _38 >= _34._8 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 13;
                        _42 = (ushort) ( 0 ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 13;
                        _42 = (ushort) ( Byte( _34._5 , (int)( _38 ) ) ) ; 
                        }
                    
                    __context__.SourceCodeLine = 13;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_42 == 63))  ) ) 
                        { 
                        __context__.SourceCodeLine = 13;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_40 != 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 14;
                            _40 = (short) ( _32( __context__ , ref _34 , _35 , (ushort)( (_36 + 1) ) , (ushort)( (_38 + 1) ) ) ) ; 
                            __context__.SourceCodeLine = 14;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_40 != 0))  ) ) 
                                { 
                                __context__.SourceCodeLine = 14;
                                return (ushort)( _40) ; 
                                } 
                            
                            } 
                        
                        __context__.SourceCodeLine = 14;
                        _37 = (ushort) ( (_38 + 1) ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 14;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_42 == 42) ) || Functions.TestForTrue ( Functions.BoolToInt (_42 == 43) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 14;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_42 == 43))  ) ) 
                                { 
                                __context__.SourceCodeLine = 14;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_40 == 0))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 14;
                                    return (ushort)( 0) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 14;
                                    _36 = (ushort) ( (_36 + 1) ) ; 
                                    }
                                
                                } 
                            
                            __context__.SourceCodeLine = 14;
                            _43 = (short) ( 0 ) ; 
                            __context__.SourceCodeLine = 14;
                            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (_36 + _43) < _34._7 ) ) && Functions.TestForTrue ( _29( __context__ , ref _34 , (ushort)( Byte( _35 , (int)( (_36 + _43) ) ) ) , (ushort)( _37 ) , (ushort)( _38 ) ) )) ))  ) ) 
                                {
                                __context__.SourceCodeLine = 14;
                                _43 = (short) ( (_43 + 1) ) ; 
                                __context__.SourceCodeLine = 14;
                                }
                            
                            __context__.SourceCodeLine = 14;
                            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _43 >= 0 ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 14;
                                _44 = (ushort) ( _32( __context__ , ref _34 , _35 , (ushort)( (_36 + _43) ) , (ushort)( (_38 + 1) ) ) ) ; 
                                __context__.SourceCodeLine = 14;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _44 > 0 ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 14;
                                    return (ushort)( _44) ; 
                                    } 
                                
                                __context__.SourceCodeLine = 14;
                                _43 = (short) ( (_43 - 1) ) ; 
                                __context__.SourceCodeLine = 14;
                                } 
                            
                            __context__.SourceCodeLine = 14;
                            return (ushort)( 0) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 14;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_42 == 45))  ) ) 
                                { 
                                __context__.SourceCodeLine = 14;
                                while ( Functions.TestForTrue  ( ( 1)  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 14;
                                    _44 = (ushort) ( _32( __context__ , ref _34 , _35 , (ushort)( _36 ) , (ushort)( (_38 + 1) ) ) ) ; 
                                    __context__.SourceCodeLine = 14;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_44 != 0))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 14;
                                        return (ushort)( _44) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 14;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _36 < _34._7 ) ) && Functions.TestForTrue ( _29( __context__ , ref _34 , (ushort)( Byte( _35 , (int)( _36 ) ) ) , (ushort)( _37 ) , (ushort)( _38 ) ) )) ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 14;
                                            _36 = (ushort) ( (_36 + 1) ) ; 
                                            }
                                        
                                        else 
                                            { 
                                            __context__.SourceCodeLine = 14;
                                            return (ushort)( 0) ; 
                                            } 
                                        
                                        }
                                    
                                    __context__.SourceCodeLine = 14;
                                    } 
                                
                                } 
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 14;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_40 == 0))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 14;
                                    return (ushort)( 0) ; 
                                    } 
                                
                                __context__.SourceCodeLine = 14;
                                _36 = (ushort) ( (_36 + 1) ) ; 
                                __context__.SourceCodeLine = 15;
                                _37 = (ushort) ( _38 ) ; 
                                } 
                            
                            }
                        
                        }
                    
                    } 
                
                __context__.SourceCodeLine = 11;
                } 
            
            
            return 0; // default return value (none specified in module)
            }
            
        private ushort _33 (  SplusExecutionContext __context__, ref _0 _35 , ushort _36 , ushort _37 , ushort _38 ) 
            { 
            ushort _39 = 0;
            
            short _40 = 0;
            
            
            __context__.SourceCodeLine = 15;
            _39 = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 15;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _36 >= _35._9 ))  ) ) 
                { 
                __context__.SourceCodeLine = 15;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_36 == 0))  ) ) 
                    {
                    __context__.SourceCodeLine = 15;
                    _39 = (ushort) ( _23( __context__ , ref _35 , (ushort)( _37 ) , (ushort)( _38 ) ) ) ; 
                    }
                
                else 
                    { 
                    __context__.SourceCodeLine = 15;
                    _35 . _1  .UpdateValue ( "invalid capture index"  ) ; 
                    __context__.SourceCodeLine = 15;
                    _39 = (ushort) ( 0 ) ; 
                    } 
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 15;
                _40 = (short) ( _35._11[ _36 ] ) ; 
                __context__.SourceCodeLine = 15;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_40 == Functions.ToSignedLongInteger( -( 1 ) )))  ) ) 
                    { 
                    __context__.SourceCodeLine = 15;
                    _35 . _1  .UpdateValue ( "unfinished capture"  ) ; 
                    __context__.SourceCodeLine = 15;
                    _39 = (ushort) ( 0 ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 15;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_40 == Functions.ToSignedLongInteger( -( 2 ) )))  ) ) 
                        {
                        __context__.SourceCodeLine = 15;
                        _40 = (short) ( 0 ) ; 
                        }
                    
                    __context__.SourceCodeLine = 15;
                    _39 = (ushort) ( _23( __context__ , ref _35 , (ushort)( _35._10[ _36 ] ) , (ushort)( (_35._10[ _36 ] + _40) ) ) ) ; 
                    } 
                
                } 
            
            __context__.SourceCodeLine = 15;
            return (ushort)( _39) ; 
            
            }
            
        private ushort _34 (  SplusExecutionContext __context__, ref _0 _36 , ushort _37 , ushort _38 ) 
            { 
            ushort _39 = 0;
            
            ushort _40 = 0;
            
            
            __context__.SourceCodeLine = 15;
            _39 = (ushort) ( _22( __context__ , ref _36 , (ushort)( (_36._9 + 1) ) , "too many captures" ) ) ; 
            __context__.SourceCodeLine = 15;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _39 > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 16;
                _23 (  __context__ ,   ref  _36 , (ushort)( _37 ), (ushort)( _38 )) ; 
                __context__.SourceCodeLine = 16;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)(_39 - 2); 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( _40  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_40  >= __FN_FORSTART_VAL__1) && (_40  <= __FN_FOREND_VAL__1) ) : ( (_40  <= __FN_FORSTART_VAL__1) && (_40  >= __FN_FOREND_VAL__1) ) ; _40  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 16;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_33( __context__ , ref _36 , (ushort)( _40 ) , (ushort)( _37 ) , (ushort)( _38 ) ) == 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 16;
                        _39 = (ushort) ( _40 ) ; 
                        __context__.SourceCodeLine = 16;
                        break ; 
                        } 
                    
                    __context__.SourceCodeLine = 16;
                    } 
                
                } 
            
            __context__.SourceCodeLine = 16;
            return (ushort)( _39) ; 
            
            }
            
        private short _35 (  SplusExecutionContext __context__, CrestronString _37 , ushort _38 , ushort _39 , CrestronString _40 , ushort _41 , ushort _42 ) 
            { 
            
            __context__.SourceCodeLine = 16;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (_39 + _42) <= _38 ))  ) ) 
                { 
                __context__.SourceCodeLine = 16;
                _37  .UpdateValue ( Functions.Left ( _37 ,  (int) ( _39 ) ) + Functions.Mid ( _40 ,  (int) ( _41 ) ,  (int) ( _42 ) )  ) ; 
                __context__.SourceCodeLine = 16;
                return (short)( (_39 + _42)) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 16;
                _37  .UpdateValue ( "buffer overflow"  ) ; 
                __context__.SourceCodeLine = 16;
                return (short)( Functions.ToSignedInteger( -( 15 ) )) ; 
                } 
            
            
            return 0; // default return value (none specified in module)
            }
            
        private short _36 (  SplusExecutionContext __context__, ref _0 _38 , CrestronString _39 , ushort _40 , ushort _41 , CrestronString _42 , ushort _43 , ushort _44 , CrestronString _45 , ushort _46 ) 
            { 
            short _47 = 0;
            
            ushort _48 = 0;
            
            ushort _49 = 0;
            
            
            __context__.SourceCodeLine = 16;
            _47 = (short) ( _41 ) ; 
            __context__.SourceCodeLine = 16;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)_46; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( _48  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_48  >= __FN_FORSTART_VAL__1) && (_48  <= __FN_FOREND_VAL__1) ) : ( (_48  <= __FN_FORSTART_VAL__1) && (_48  >= __FN_FOREND_VAL__1) ) ; _48  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 17;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( _45 , (int)( _48 ) ) != 37))  ) ) 
                    {
                    __context__.SourceCodeLine = 17;
                    _47 = (short) ( _35( __context__ , _39 , (ushort)( _40 ) , (ushort)( _41 ) , _45 , (ushort)( _48 ) , (ushort)( 1 ) ) ) ; 
                    }
                
                else 
                    { 
                    __context__.SourceCodeLine = 17;
                    _48 = (ushort) ( (_48 + 1) ) ; 
                    __context__.SourceCodeLine = 17;
                    _49 = (ushort) ( Byte( _45 , (int)( _48 ) ) ) ; 
                    __context__.SourceCodeLine = 17;
                    if ( Functions.TestForTrue  ( ( Functions.Not( _14( __context__ , (ushort)( _49 ) ) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 17;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_49 == 37))  ) ) 
                            {
                            __context__.SourceCodeLine = 17;
                            _47 = (short) ( _35( __context__ , _39 , (ushort)( _40 ) , (ushort)( _41 ) , _45 , (ushort)( _48 ) , (ushort)( 1 ) ) ) ; 
                            }
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 17;
                            _39  .UpdateValue ( "invalid use of %c in replacement string"  ) ; 
                            __context__.SourceCodeLine = 17;
                            _47 = (short) ( Functions.ToSignedInteger( -( Functions.Length( _39 ) ) ) ) ; 
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 17;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_49 == 48))  ) ) 
                            {
                            __context__.SourceCodeLine = 17;
                            _47 = (short) ( _35( __context__ , _39 , (ushort)( _40 ) , (ushort)( _41 ) , _42 , (ushort)( _43 ) , (ushort)( (_44 - _43) ) ) ) ; 
                            }
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 17;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_33( __context__ , ref _38 , (ushort)( (_49 - 49) ) , (ushort)( _43 ) , (ushort)( _44 ) ) == 0))  ) ) 
                                { 
                                __context__.SourceCodeLine = 17;
                                _39  .UpdateValue ( _38 . _1  ) ; 
                                __context__.SourceCodeLine = 17;
                                _47 = (short) ( Functions.ToSignedInteger( -( Functions.Length( _39 ) ) ) ) ; 
                                } 
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 17;
                                _47 = (short) ( _35( __context__ , _39 , (ushort)( _40 ) , (ushort)( _41 ) , _42 , (ushort)( ((_38._6 + _38._3[ (_38._2 - 1) ]) - 1) ) , (ushort)( (_38._4[ (_38._2 - 1) ] - _38._3[ (_38._2 - 1) ]) ) ) ) ; 
                                __context__.SourceCodeLine = 17;
                                _38 . _2 = (ushort) ( (_38._2 - 1) ) ; 
                                } 
                            
                            } 
                        
                        }
                    
                    } 
                
                __context__.SourceCodeLine = 17;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _47 < 0 ))  ) ) 
                    {
                    __context__.SourceCodeLine = 17;
                    break ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 17;
                    _41 = (ushort) ( _47 ) ; 
                    }
                
                __context__.SourceCodeLine = 16;
                } 
            
            __context__.SourceCodeLine = 17;
            return (short)( _47) ; 
            
            }
            
        private ushort _37 (  SplusExecutionContext __context__, CrestronString _39 , ushort _40 , short _41 , CrestronString _5 , ushort _42 , ref _0 _43 ) 
            { 
            ushort _44 = 0;
            
            ushort _45 = 0;
            
            ushort _46 = 0;
            
            
            __context__.SourceCodeLine = 18;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _41 < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 18;
                _41 = (short) ( ((_40 + _41) + 1) ) ; 
                }
            
            __context__.SourceCodeLine = 18;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _41 < 1 ))  ) ) 
                {
                __context__.SourceCodeLine = 18;
                _41 = (short) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 18;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _41 > (_40 + 1) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 18;
                    _41 = (short) ( (_40 + 1) ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 18;
            _45 = (ushort) ( _41 ) ; 
            __context__.SourceCodeLine = 18;
            _44 = (ushort) ( _21( __context__ , ref _43 , (ushort)( _40 ) , _5 , (ushort)( _42 ) ) ) ; 
            __context__.SourceCodeLine = 18;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _45 < _43._7 ))  ) ) 
                { 
                __context__.SourceCodeLine = 18;
                do 
                    { 
                    __context__.SourceCodeLine = 18;
                    _43 . _9 = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 18;
                    _46 = (ushort) ( _32( __context__ , ref _43 , _39 , (ushort)( _45 ) , (ushort)( 1 ) ) ) ; 
                    __context__.SourceCodeLine = 18;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_46 != 0))  ) ) 
                        {
                        __context__.SourceCodeLine = 18;
                        return (ushort)( _34( __context__ , ref _43 , (ushort)( _45 ) , (ushort)( _46 ) )) ; 
                        }
                    
                    __context__.SourceCodeLine = 18;
                    _45 = (ushort) ( (_45 + 1) ) ; 
                    } 
                while (false == ( Functions.TestForTrue  ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _45 >= _43._7 ) ) || Functions.TestForTrue ( _44 )) )) )); 
                } 
            
            __context__.SourceCodeLine = 18;
            return (ushort)( 0) ; 
            
            }
            
        private ushort _38 (  SplusExecutionContext __context__, CrestronString _40 , short _41 , CrestronString _5 , ref _0 _42 ) 
            { 
            
            __context__.SourceCodeLine = 18;
            return (ushort)( _37( __context__ , _40 , (ushort)( Functions.Length( _40 ) ) , (short)( _41 ) , _5 , (ushort)( Functions.Length( _5 ) ) , ref _42 )) ; 
            
            }
            
        private short _39 (  SplusExecutionContext __context__, CrestronString _41 , ushort _42 , short _43 , CrestronString _5 , ushort _44 , CrestronString _45 , ushort REPLEN , short REPCOUNT , CrestronString BUFFER , ref ushort BUFFERLEN ) 
            { 
            ushort _46 = 0;
            
            short _47 = 0;
            
            short _48 = 0;
            
            ushort _49 = 0;
            
            ushort _50 = 0;
            
            _0 _51;
            _51  = new _0( this, true );
            _51 .PopulateCustomAttributeList( false );
            
            
            __context__.SourceCodeLine = 19;
            _47 = (short) ( 0 ) ; 
            __context__.SourceCodeLine = 19;
            _48 = (short) ( 0 ) ; 
            __context__.SourceCodeLine = 19;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( REPCOUNT <= 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 19;
                REPCOUNT = (short) ( (_42 + 1) ) ; 
                }
            
            __context__.SourceCodeLine = 19;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _43 < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 19;
                _43 = (short) ( ((_42 + _43) + 1) ) ; 
                }
            
            __context__.SourceCodeLine = 19;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _43 < 1 ))  ) ) 
                {
                __context__.SourceCodeLine = 19;
                _43 = (short) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 19;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _43 > (_42 + 1) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 19;
                    _43 = (short) ( (_42 + 1) ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 19;
            BUFFER  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 19;
            _49 = (ushort) ( _43 ) ; 
            __context__.SourceCodeLine = 19;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _49 > 1 ))  ) ) 
                {
                __context__.SourceCodeLine = 19;
                _48 = (short) ( _35( __context__ , BUFFER , (ushort)( BUFFERLEN ) , (ushort)( _48 ) , _41 , (ushort)( 1 ) , (ushort)( (_49 - 1) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 19;
            _46 = (ushort) ( _21( __context__ , ref _51 , (ushort)( _42 ) , _5 , (ushort)( _44 ) ) ) ; 
            __context__.SourceCodeLine = 19;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _47 < REPCOUNT ))  ) ) 
                { 
                __context__.SourceCodeLine = 19;
                _51 . _9 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 19;
                _51 . _2 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 20;
                _50 = (ushort) ( _32( __context__ , ref _51 , _41 , (ushort)( _49 ) , (ushort)( 1 ) ) ) ; 
                __context__.SourceCodeLine = 20;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Length( _51._1 ) != 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 20;
                    _48 = (short) ( Functions.Length( _51._1 ) ) ; 
                    __context__.SourceCodeLine = 20;
                    BUFFER  .UpdateValue ( _51 . _1  ) ; 
                    __context__.SourceCodeLine = 20;
                    _47 = (short) ( Functions.ToSignedInteger( -( 1 ) ) ) ; 
                    __context__.SourceCodeLine = 20;
                    break ; 
                    } 
                
                __context__.SourceCodeLine = 20;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_50 != 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 20;
                    _47 = (short) ( (_47 + 1) ) ; 
                    __context__.SourceCodeLine = 20;
                    _48 = (short) ( _36( __context__ , ref _51 , BUFFER , (ushort)( BUFFERLEN ) , (ushort)( _48 ) , _41 , (ushort)( _49 ) , (ushort)( _50 ) , _45 , (ushort)( REPLEN ) ) ) ; 
                    __context__.SourceCodeLine = 20;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _48 < 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 20;
                        _47 = (short) ( Functions.ToSignedInteger( -( 1 ) ) ) ; 
                        __context__.SourceCodeLine = 20;
                        _48 = (short) ( Functions.ToSignedInteger( -( _48 ) ) ) ; 
                        __context__.SourceCodeLine = 20;
                        break ; 
                        } 
                    
                    } 
                
                __context__.SourceCodeLine = 20;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _50 > _49 ))  ) ) 
                    {
                    __context__.SourceCodeLine = 20;
                    _49 = (ushort) ( _50 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 20;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _49 < _51._7 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 20;
                        _48 = (short) ( _35( __context__ , BUFFER , (ushort)( BUFFERLEN ) , (ushort)( _48 ) , _41 , (ushort)( _49 ) , (ushort)( 1 ) ) ) ; 
                        __context__.SourceCodeLine = 20;
                        _49 = (ushort) ( (_49 + 1) ) ; 
                        __context__.SourceCodeLine = 20;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _48 < 0 ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 20;
                            _47 = (short) ( Functions.ToSignedInteger( -( 1 ) ) ) ; 
                            __context__.SourceCodeLine = 20;
                            _48 = (short) ( Functions.ToSignedInteger( -( _48 ) ) ) ; 
                            __context__.SourceCodeLine = 20;
                            break ; 
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 20;
                        break ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 20;
                if ( Functions.TestForTrue  ( ( _46)  ) ) 
                    {
                    __context__.SourceCodeLine = 20;
                    break ; 
                    }
                
                __context__.SourceCodeLine = 19;
                } 
            
            __context__.SourceCodeLine = 20;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _47 >= 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 20;
                _48 = (short) ( _35( __context__ , BUFFER , (ushort)( BUFFERLEN ) , (ushort)( _48 ) , _41 , (ushort)( _49 ) , (ushort)( (_51._7 - _49) ) ) ) ; 
                __context__.SourceCodeLine = 20;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _48 < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 20;
                    _47 = (short) ( Functions.ToSignedInteger( -( 1 ) ) ) ; 
                    __context__.SourceCodeLine = 20;
                    _48 = (short) ( Functions.ToSignedInteger( -( _48 ) ) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 20;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _48 < BUFFERLEN ))  ) ) 
                        { 
                        } 
                    
                    }
                
                } 
            
            __context__.SourceCodeLine = 20;
            BUFFERLEN = (ushort) ( _48 ) ; 
            __context__.SourceCodeLine = 20;
            return (short)( _47) ; 
            
            }
            
        private short _40 (  SplusExecutionContext __context__, CrestronString _42 , short _43 , CrestronString _5 , CrestronString _44 , short _45 , CrestronString BUFFER , ref ushort BUFFERLEN ) 
            { 
            
            __context__.SourceCodeLine = 21;
            return (short)( _39( __context__ , _42 , (ushort)( Functions.Length( _42 ) ) , (short)( _43 ) , _5 , (ushort)( Functions.Length( _5 ) ) , _44 , (ushort)( Functions.Length( _44 ) ) , (short)( _45 ) , BUFFER , ref BUFFERLEN )) ; 
            
            }
            
        private ushort _45 (  SplusExecutionContext __context__, ref _41 _41 ) 
            { 
            ushort _48 = 0;
            
            
            __context__.SourceCodeLine = 21;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( ((_41._43 + 1) - _41._42) < _41._44 ))  ) ) 
                { 
                __context__.SourceCodeLine = 21;
                _41 . _43 = (ushort) ( (_41._43 + 1) ) ; 
                __context__.SourceCodeLine = 21;
                _48 = (ushort) ( Mod( _41._43 , _41._44 ) ) ; 
                __context__.SourceCodeLine = 21;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _41._42 > _41._44 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _41._43 > _41._44 ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 21;
                    _41 . _42 = (ushort) ( (_41._42 - _41._44) ) ; 
                    __context__.SourceCodeLine = 21;
                    _41 . _43 = (ushort) ( (_41._43 - _41._44) ) ; 
                    } 
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 21;
                _48 = (ushort) ( _41._44 ) ; 
                } 
            
            __context__.SourceCodeLine = 21;
            return (ushort)( _48) ; 
            
            }
            
        private ushort _46 (  SplusExecutionContext __context__, _41 _41 ) 
            { 
            ushort _49 = 0;
            
            
            __context__.SourceCodeLine = 22;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( ((_41._43 + 1) - _41._42) < _41._44 ))  ) ) 
                { 
                __context__.SourceCodeLine = 22;
                _49 = (ushort) ( Mod( _41._42 , _41._44 ) ) ; 
                __context__.SourceCodeLine = 22;
                _41 . _42 = (ushort) ( (_41._42 + 1) ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 22;
                Print( "Queue - Queue:Error") ; 
                __context__.SourceCodeLine = 22;
                _49 = (ushort) ( _41._44 ) ; 
                } 
            
            __context__.SourceCodeLine = 22;
            return (ushort)( _49) ; 
            
            }
            
        private void _47 (  SplusExecutionContext __context__, ref _41 _41 , ushort _50 ) 
            { 
            
            __context__.SourceCodeLine = 22;
            _41 . _42 = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 22;
            _41 . _43 = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 22;
            _41 . _44 = (ushort) ( _50 ) ; 
            
            }
            
        private void _48 (  SplusExecutionContext __context__, ref _41 _41 ) 
            { 
            
            __context__.SourceCodeLine = 22;
            _41 . _42 = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 22;
            _41 . _43 = (ushort) ( 0 ) ; 
            
            }
            
        private ushort _49 (  SplusExecutionContext __context__, ref _41 _41 ) 
            { 
            
            __context__.SourceCodeLine = 22;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _41._42 > _41._43 ))  ) ) 
                { 
                __context__.SourceCodeLine = 22;
                _48 (  __context__ ,   ref  _41 ) ; 
                __context__.SourceCodeLine = 22;
                return (ushort)( 1) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 22;
                return (ushort)( 0) ; 
                } 
            
            
            return 0; // default return value (none specified in module)
            }
            
        private void _50 (  SplusExecutionContext __context__, ushort [] _52 , ushort [] _53 , ushort [] _54 ) 
            { 
            
            __context__.SourceCodeLine = 22;
            _52 [ 0] = (ushort) ( 254 ) ; 
            __context__.SourceCodeLine = 22;
            _52 [ 1] = (ushort) ( 126 ) ; 
            __context__.SourceCodeLine = 22;
            _52 [ 2] = (ushort) ( 62 ) ; 
            __context__.SourceCodeLine = 22;
            _52 [ 3] = (ushort) ( 30 ) ; 
            __context__.SourceCodeLine = 22;
            _52 [ 4] = (ushort) ( 14 ) ; 
            __context__.SourceCodeLine = 23;
            _52 [ 5] = (ushort) ( 6 ) ; 
            __context__.SourceCodeLine = 23;
            _52 [ 6] = (ushort) ( 2 ) ; 
            __context__.SourceCodeLine = 23;
            _53 [ 0] = (ushort) ( 128 ) ; 
            __context__.SourceCodeLine = 23;
            _53 [ 1] = (ushort) ( 192 ) ; 
            __context__.SourceCodeLine = 23;
            _53 [ 2] = (ushort) ( 224 ) ; 
            __context__.SourceCodeLine = 23;
            _53 [ 3] = (ushort) ( 240 ) ; 
            __context__.SourceCodeLine = 23;
            _53 [ 4] = (ushort) ( 248 ) ; 
            __context__.SourceCodeLine = 23;
            _53 [ 5] = (ushort) ( 252 ) ; 
            __context__.SourceCodeLine = 23;
            _53 [ 6] = (ushort) ( 254 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 0] = (ushort) ( 9 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 1] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 2] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 3] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 4] = (ushort) ( 44 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 5] = (ushort) ( 3 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 6] = (ushort) ( 55 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 7] = (ushort) ( 18 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 8] = (ushort) ( 58 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 9] = (ushort) ( 33 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 10] = (ushort) ( 43 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 11] = (ushort) ( 24 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 12] = (ushort) ( 60 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 13] = (ushort) ( 51 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 14] = (ushort) ( 7 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 15] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 16] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 17] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 18] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 19] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 20] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 21] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 22] = (ushort) ( 61 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 23] = (ushort) ( 48 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 24] = (ushort) ( 56 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 25] = (ushort) ( 46 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 26] = (ushort) ( 52 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 27] = (ushort) ( 38 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 28] = (ushort) ( 39 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 29] = (ushort) ( 36 ) ; 
            __context__.SourceCodeLine = 23;
            _54 [ 30] = (ushort) ( 31 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 31] = (ushort) ( 59 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 32] = (ushort) ( 30 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 33] = (ushort) ( 20 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 34] = (ushort) ( 45 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 35] = (ushort) ( 37 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 36] = (ushort) ( 62 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 37] = (ushort) ( 14 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 38] = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 39] = (ushort) ( 15 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 40] = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 41] = (ushort) ( 23 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 42] = (ushort) ( 5 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 43] = (ushort) ( 50 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 44] = (ushort) ( 13 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 45] = (ushort) ( 19 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 46] = (ushort) ( 10 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 47] = (ushort) ( 27 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 48] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 49] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 50] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 51] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 52] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 53] = (ushort) ( 255 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 54] = (ushort) ( 28 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 55] = (ushort) ( 49 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 56] = (ushort) ( 63 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 57] = (ushort) ( 57 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 58] = (ushort) ( 22 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 59] = (ushort) ( 40 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 60] = (ushort) ( 41 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 61] = (ushort) ( 12 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 62] = (ushort) ( 47 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 63] = (ushort) ( 34 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 64] = (ushort) ( 2 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 65] = (ushort) ( 16 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 66] = (ushort) ( 6 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 67] = (ushort) ( 11 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 68] = (ushort) ( 32 ) ; 
            __context__.SourceCodeLine = 24;
            _54 [ 69] = (ushort) ( 21 ) ; 
            __context__.SourceCodeLine = 25;
            _54 [ 70] = (ushort) ( 8 ) ; 
            __context__.SourceCodeLine = 25;
            _54 [ 71] = (ushort) ( 4 ) ; 
            __context__.SourceCodeLine = 25;
            _54 [ 72] = (ushort) ( 26 ) ; 
            __context__.SourceCodeLine = 25;
            _54 [ 73] = (ushort) ( 29 ) ; 
            __context__.SourceCodeLine = 25;
            _54 [ 74] = (ushort) ( 17 ) ; 
            __context__.SourceCodeLine = 25;
            _54 [ 75] = (ushort) ( 42 ) ; 
            __context__.SourceCodeLine = 25;
            _54 [ 76] = (ushort) ( 53 ) ; 
            __context__.SourceCodeLine = 25;
            _54 [ 77] = (ushort) ( 54 ) ; 
            __context__.SourceCodeLine = 25;
            _54 [ 78] = (ushort) ( 35 ) ; 
            __context__.SourceCodeLine = 25;
            _54 [ 79] = (ushort) ( 25 ) ; 
            
            }
            
        private void _51 (  SplusExecutionContext __context__, CrestronString _53 , ushort [] _54 , ushort _55 ) 
            { 
            CrestronString _56;
            _56  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 512, this );
            
            ushort _57 = 0;
            ushort _58 = 0;
            
            
            __context__.SourceCodeLine = 25;
            _58 = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 25;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)_55; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( _57  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_57  >= __FN_FORSTART_VAL__1) && (_57  <= __FN_FOREND_VAL__1) ) : ( (_57  <= __FN_FORSTART_VAL__1) && (_57  >= __FN_FOREND_VAL__1) ) ; _57  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 25;
                _56  .UpdateValue ( _56 + Functions.ItoHex (  (int) ( _54[ (_57 - 1) ] ) ) + " "  ) ; 
                __context__.SourceCodeLine = 25;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Mod( _57 , 16 ) == 0) ) || Functions.TestForTrue ( Functions.BoolToInt (_57 == _55) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 25;
                    _56  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 25;
                    _58 = (ushort) ( (_58 + 1) ) ; 
                    } 
                
                __context__.SourceCodeLine = 25;
                } 
            
            
            }
            
        private uint _52 (  SplusExecutionContext __context__, uint [] _54 , ref ushort _55 ) 
            { 
            uint _56 = 0;
            uint _57 = 0;
            uint _58 = 0;
            uint _59 = 0;
            
            
            __context__.SourceCodeLine = 25;
            _56 = (uint) ( _54[ _55 ] ) ; 
            __context__.SourceCodeLine = 25;
            _57 = (uint) ( _54[ ((_55 + 13) & 15) ] ) ; 
            __context__.SourceCodeLine = 25;
            _58 = (uint) ( (((_56 ^ _57) ^ (_56 << 16)) ^ (_57 << 15)) ) ; 
            __context__.SourceCodeLine = 25;
            _57 = (uint) ( _54[ ((_55 + 9) & 15) ] ) ; 
            __context__.SourceCodeLine = 26;
            _57 = (uint) ( (_57 ^ (_57 >> 11)) ) ; 
            __context__.SourceCodeLine = 26;
            _54 [ _55] = (uint) ( (_58 ^ _57) ) ; 
            __context__.SourceCodeLine = 26;
            _56 = (uint) ( _54[ _55 ] ) ; 
            __context__.SourceCodeLine = 26;
            _59 = (uint) ( (_56 ^ ((_56 << 5) & 3661901088)) ) ; 
            __context__.SourceCodeLine = 26;
            _55 = (ushort) ( ((_55 + 15) & 15) ) ; 
            __context__.SourceCodeLine = 26;
            _56 = (uint) ( _54[ _55 ] ) ; 
            __context__.SourceCodeLine = 26;
            _54 [ _55] = (uint) ( (((((_56 ^ _58) ^ _59) ^ (_56 << 2)) ^ (_58 << 18)) ^ (_57 << 28)) ) ; 
            __context__.SourceCodeLine = 26;
            return (uint)( _54[ _55 ]) ; 
            
            }
            
        private void _53 (  SplusExecutionContext __context__, uint _55 , ref uint [] _56 , ref ushort _57 ) 
            { 
            ushort _58 = 0;
            
            uint _59 = 0;
            
            
            __context__.SourceCodeLine = 26;
            _59 = (uint) ( 0 ) ; 
            __context__.SourceCodeLine = 26;
            _57 = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 26;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)15; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( _58  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_58  >= __FN_FORSTART_VAL__1) && (_58  <= __FN_FOREND_VAL__1) ) : ( (_58  <= __FN_FORSTART_VAL__1) && (_58  >= __FN_FOREND_VAL__1) ) ; _58  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 26;
                _56 [ _58] = (uint) ( ((_55 + _59) ^ (1431655765 >> _58)) ) ; 
                __context__.SourceCodeLine = 26;
                _59 = (uint) ( (_59 + 7623293) ) ; 
                __context__.SourceCodeLine = 26;
                } 
            
            
            }
            
        private ushort _54 (  SplusExecutionContext __context__, CrestronString _56 , ushort _57 , ref ushort [] _58 , ref CrestronString [] _59 ) 
            { 
            ushort _60 = 0;
            
            ushort _61 = 0;
            
            ushort [] _62;
            _62  = new ushort[ 257 ];
            
            uint [] _63;
            _63  = new uint[ 257 ];
            
            uint _64 = 0;
            
            ushort _65 = 0;
            
            ushort [] _66;
            _66  = new ushort[ 4 ];
            
            short _67 = 0;
            short _68 = 0;
            
            ushort _69 = 0;
            ushort _70 = 0;
            
            ushort _71 = 0;
            
            uint _72 = 0;
            
            ushort _73 = 0;
            
            ushort _74 = 0;
            
            ushort [] _75;
            _75  = new ushort[ 7 ];
            
            ushort [] _76;
            _76  = new ushort[ 7 ];
            
            ushort [] _77;
            _77  = new ushort[ 80 ];
            
            uint [] _78;
            _78  = new uint[ 17 ];
            
            ushort _79 = 0;
            
            uint _80 = 0;
            
            
            __context__.SourceCodeLine = 27;
            _50 (  __context__ , (ushort[])( _75 ), (ushort[])( _76 ), (ushort[])( _77 )) ; 
            __context__.SourceCodeLine = 27;
            _61 = (ushort) ( Functions.Length( _56 ) ) ; 
            __context__.SourceCodeLine = 27;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _61 < 44 ) ) || Functions.TestForTrue ( Functions.BoolToInt (Mod( _61 , 4 ) != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( ((_61 * 3) / 4) > 256 ) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 27;
                _60 = (ushort) ( 0 ) ; 
                }
            
            else 
                { 
                __context__.SourceCodeLine = 27;
                _64 = (uint) ( 0 ) ; 
                __context__.SourceCodeLine = 27;
                _65 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 27;
                short __FN_FORSTART_VAL__1 = (short) ( 0 ) ;
                short __FN_FOREND_VAL__1 = (short)(_61 - 1); 
                int __FN_FORSTEP_VAL__1 = (int)4; 
                for ( _67  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_67  >= __FN_FORSTART_VAL__1) && (_67  <= __FN_FOREND_VAL__1) ) : ( (_67  <= __FN_FORSTART_VAL__1) && (_67  >= __FN_FOREND_VAL__1) ) ; _67  += (short)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 27;
                    short __FN_FORSTART_VAL__2 = (short) ( 0 ) ;
                    short __FN_FOREND_VAL__2 = (short)3; 
                    int __FN_FORSTEP_VAL__2 = (int)1; 
                    for ( _68  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (_68  >= __FN_FORSTART_VAL__2) && (_68  <= __FN_FOREND_VAL__2) ) : ( (_68  <= __FN_FORSTART_VAL__2) && (_68  >= __FN_FOREND_VAL__2) ) ; _68  += (short)__FN_FORSTEP_VAL__2) 
                        { 
                        __context__.SourceCodeLine = 27;
                        _71 = (ushort) ( Byte( _56 , (int)( ((_67 + _68) + 1) ) ) ) ; 
                        __context__.SourceCodeLine = 27;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _71 >= 43 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _71 <= 122 ) )) ))  ) ) 
                            {
                            __context__.SourceCodeLine = 27;
                            _66 [ _68] = (ushort) ( _77[ (_71 - 43) ] ) ; 
                            }
                        
                        else 
                            {
                            __context__.SourceCodeLine = 27;
                            _66 [ _68] = (ushort) ( 255 ) ; 
                            }
                        
                        __context__.SourceCodeLine = 27;
                        } 
                    
                    __context__.SourceCodeLine = 28;
                    _62 [ _65] = (ushort) ( (((_66[ 0 ] << 2) | (_66[ 1 ] >> 4)) & 255) ) ; 
                    __context__.SourceCodeLine = 28;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_66[ 2 ] != 255))  ) ) 
                        { 
                        __context__.SourceCodeLine = 28;
                        _65 = (ushort) ( (_65 + 1) ) ; 
                        __context__.SourceCodeLine = 28;
                        _62 [ _65] = (ushort) ( (((_66[ 1 ] << 4) | (_66[ 2 ] >> 2)) & 255) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 28;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_66[ 3 ] != 255))  ) ) 
                        { 
                        __context__.SourceCodeLine = 28;
                        _65 = (ushort) ( (_65 + 1) ) ; 
                        __context__.SourceCodeLine = 28;
                        _62 [ _65] = (ushort) ( ((((_66[ 2 ] << 6) & 192) | _66[ 3 ]) & 255) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 28;
                    _65 = (ushort) ( (_65 + 1) ) ; 
                    __context__.SourceCodeLine = 27;
                    } 
                
                __context__.SourceCodeLine = 28;
                short __FN_FORSTART_VAL__3 = (short) ( 0 ) ;
                short __FN_FOREND_VAL__3 = (short)27; 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( _67  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (_67  >= __FN_FORSTART_VAL__3) && (_67  <= __FN_FOREND_VAL__3) ) : ( (_67  <= __FN_FORSTART_VAL__3) && (_67  >= __FN_FOREND_VAL__3) ) ; _67  += (short)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 28;
                    _69 = (ushort) ( Mod( _67 , 7 ) ) ; 
                    __context__.SourceCodeLine = 28;
                    _70 = (ushort) ( (_67 / 7) ) ; 
                    __context__.SourceCodeLine = 28;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((_62[ _67 ] & 1) != 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 28;
                        _64 = (uint) ( (_64 | (1 << _67)) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 28;
                    _62 [ _67] = (ushort) ( ((((_62[ (_67 + _70) ] & _75[ _69 ]) << _69) | ((_62[ ((_67 + _70) + 1) ] & _76[ _69 ]) >> (7 - _69))) & 255) ) ; 
                    __context__.SourceCodeLine = 28;
                    } 
                
                __context__.SourceCodeLine = 28;
                short __FN_FORSTART_VAL__4 = (short) ( 28 ) ;
                short __FN_FOREND_VAL__4 = (short)31; 
                int __FN_FORSTEP_VAL__4 = (int)1; 
                for ( _67  = __FN_FORSTART_VAL__4; (__FN_FORSTEP_VAL__4 > 0)  ? ( (_67  >= __FN_FORSTART_VAL__4) && (_67  <= __FN_FOREND_VAL__4) ) : ( (_67  <= __FN_FORSTART_VAL__4) && (_67  >= __FN_FOREND_VAL__4) ) ; _67  += (short)__FN_FORSTEP_VAL__4) 
                    { 
                    __context__.SourceCodeLine = 28;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((_62[ _67 ] & 1) != 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 28;
                        _64 = (uint) ( (_64 ^ (1 << _67)) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 28;
                    } 
                
                __context__.SourceCodeLine = 28;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _65 > 32 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 28;
                    short __FN_FORSTART_VAL__5 = (short) ( 32 ) ;
                    short __FN_FOREND_VAL__5 = (short)(_65 - 1); 
                    int __FN_FORSTEP_VAL__5 = (int)1; 
                    for ( _67  = __FN_FORSTART_VAL__5; (__FN_FORSTEP_VAL__5 > 0)  ? ( (_67  >= __FN_FORSTART_VAL__5) && (_67  <= __FN_FOREND_VAL__5) ) : ( (_67  <= __FN_FORSTART_VAL__5) && (_67  >= __FN_FOREND_VAL__5) ) ; _67  += (short)__FN_FORSTEP_VAL__5) 
                        { 
                        __context__.SourceCodeLine = 28;
                        _62 [ (_67 - 4)] = (ushort) ( _62[ _67 ] ) ; 
                        __context__.SourceCodeLine = 28;
                        } 
                    
                    } 
                
                __context__.SourceCodeLine = 28;
                _65 = (ushort) ( (_65 - 4) ) ; 
                __context__.SourceCodeLine = 28;
                _53 (  __context__ , (uint)( _64 ),   ref  _78 ,   ref  _79 ) ; 
                __context__.SourceCodeLine = 29;
                short __FN_FORSTART_VAL__6 = (short) ( 0 ) ;
                short __FN_FOREND_VAL__6 = (short)(_65 - 1); 
                int __FN_FORSTEP_VAL__6 = (int)1; 
                for ( _67  = __FN_FORSTART_VAL__6; (__FN_FORSTEP_VAL__6 > 0)  ? ( (_67  >= __FN_FORSTART_VAL__6) && (_67  <= __FN_FOREND_VAL__6) ) : ( (_67  <= __FN_FORSTART_VAL__6) && (_67  >= __FN_FOREND_VAL__6) ) ; _67  += (short)__FN_FORSTEP_VAL__6) 
                    { 
                    __context__.SourceCodeLine = 29;
                    _80 = (uint) ( (_65 - _67) ) ; 
                    __context__.SourceCodeLine = 29;
                    _63 [ _67] = (uint) ( (_52( __context__ , (uint[])( _78 ) , ref _79 ) >> 1) ) ; 
                    __context__.SourceCodeLine = 29;
                    _63 [ _67] = (uint) ( (Mod( _63[ _67 ] , _80 ) + _67) ) ; 
                    __context__.SourceCodeLine = 29;
                    _80 = (uint) ( _52( __context__ , (uint[])( _78 ) , ref _79 ) ) ; 
                    __context__.SourceCodeLine = 29;
                    _62 [ _67] = (ushort) ( (_62[ _67 ] ^ (_80 & 255)) ) ; 
                    __context__.SourceCodeLine = 29;
                    } 
                
                __context__.SourceCodeLine = 29;
                short __FN_FORSTART_VAL__7 = (short) ( (_65 - 1) ) ;
                short __FN_FOREND_VAL__7 = (short)0; 
                int __FN_FORSTEP_VAL__7 = (int)Functions.ToLongInteger( -( 1 ) ); 
                for ( _67  = __FN_FORSTART_VAL__7; (__FN_FORSTEP_VAL__7 > 0)  ? ( (_67  >= __FN_FORSTART_VAL__7) && (_67  <= __FN_FOREND_VAL__7) ) : ( (_67  <= __FN_FORSTART_VAL__7) && (_67  >= __FN_FOREND_VAL__7) ) ; _67  += (short)__FN_FORSTEP_VAL__7) 
                    { 
                    __context__.SourceCodeLine = 29;
                    _72 = (uint) ( _63[ _67 ] ) ; 
                    __context__.SourceCodeLine = 29;
                    _73 = (ushort) ( _62[ _72 ] ) ; 
                    __context__.SourceCodeLine = 29;
                    _62 [ _72] = (ushort) ( _62[ _67 ] ) ; 
                    __context__.SourceCodeLine = 29;
                    _62 [ _67] = (ushort) ( _73 ) ; 
                    __context__.SourceCodeLine = 29;
                    } 
                
                __context__.SourceCodeLine = 29;
                _67 = (short) ( 0 ) ; 
                __context__.SourceCodeLine = 29;
                _60 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 29;
                while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _67 < _65 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _60 < _57 ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 29;
                    _74 = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 29;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (_67 + 3) <= _65 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 29;
                        _58 [ _60] = (ushort) ( ((_62[ _67 ] << 8) | _62[ (_67 + 1) ]) ) ; 
                        __context__.SourceCodeLine = 29;
                        _74 = (ushort) ( _62[ (_67 + 2) ] ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 29;
                    _67 = (short) ( (_67 + 3) ) ; 
                    __context__.SourceCodeLine = 29;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (_67 + _74) > _65 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 29;
                        _60 = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 29;
                        break ; 
                        } 
                    
                    __context__.SourceCodeLine = 29;
                    _59 [ _60]  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 29;
                    short __FN_FORSTART_VAL__8 = (short) ( 0 ) ;
                    short __FN_FOREND_VAL__8 = (short)(_74 - 1); 
                    int __FN_FORSTEP_VAL__8 = (int)1; 
                    for ( _68  = __FN_FORSTART_VAL__8; (__FN_FORSTEP_VAL__8 > 0)  ? ( (_68  >= __FN_FORSTART_VAL__8) && (_68  <= __FN_FOREND_VAL__8) ) : ( (_68  <= __FN_FORSTART_VAL__8) && (_68  >= __FN_FOREND_VAL__8) ) ; _68  += (short)__FN_FORSTEP_VAL__8) 
                        { 
                        __context__.SourceCodeLine = 29;
                        _59 [ _60]  .UpdateValue ( _59 [ _60] + Functions.Chr (  (int) ( _62[ (_67 + _68) ] ) )  ) ; 
                        __context__.SourceCodeLine = 29;
                        } 
                    
                    __context__.SourceCodeLine = 29;
                    _60 = (ushort) ( (_60 + 1) ) ; 
                    __context__.SourceCodeLine = 29;
                    _67 = (short) ( (_67 + _74) ) ; 
                    __context__.SourceCodeLine = 29;
                    } 
                
                } 
            
            __context__.SourceCodeLine = 29;
            return (ushort)( _60) ; 
            
            }
            
        private CrestronString _55 (  SplusExecutionContext __context__, CrestronString _57 ) 
            { 
            CrestronString _58;
            CrestronString _59;
            _58  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            _59  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            ushort _60 = 0;
            ushort _61 = 0;
            
            
            __context__.SourceCodeLine = 30;
            _59  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 30;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( _57 ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( _60  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_60  >= __FN_FORSTART_VAL__1) && (_60  <= __FN_FOREND_VAL__1) ) : ( (_60  <= __FN_FORSTART_VAL__1) && (_60  >= __FN_FOREND_VAL__1) ) ; _60  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 30;
                _60 = (ushort) ( Functions.Find( ":" , _57 ) ) ; 
                __context__.SourceCodeLine = 30;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _60 > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 30;
                    _58  .UpdateValue ( Functions.Remove ( ":" , _57 )  ) ; 
                    __context__.SourceCodeLine = 30;
                    _58  .UpdateValue ( Functions.Left ( _58 ,  (int) ( (Functions.Length( _58 ) - 1) ) )  ) ; 
                    __context__.SourceCodeLine = 30;
                    _59  .UpdateValue ( _59 + _58  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 30;
                    _59  .UpdateValue ( _59 + _57  ) ; 
                    __context__.SourceCodeLine = 30;
                    break ; 
                    } 
                
                __context__.SourceCodeLine = 30;
                } 
            
            __context__.SourceCodeLine = 30;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Left( _59 , (int)( 1 ) ) == " "))  ) ) 
                {
                __context__.SourceCodeLine = 30;
                _59  .UpdateValue ( Functions.Right ( _59 ,  (int) ( (Functions.Length( _59 ) - 1) ) )  ) ; 
                __context__.SourceCodeLine = 30;
                }
            
            __context__.SourceCodeLine = 30;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Right( _59 , (int)( 1 ) ) == " "))  ) ) 
                {
                __context__.SourceCodeLine = 30;
                _59  .UpdateValue ( Functions.Left ( _59 ,  (int) ( (Functions.Length( _59 ) - 1) ) )  ) ; 
                __context__.SourceCodeLine = 30;
                }
            
            __context__.SourceCodeLine = 30;
            return ( Functions.Lower ( _59 ) ) ; 
            
            }
            
        private ushort _56 (  SplusExecutionContext __context__, CrestronString _58 , CrestronString _59 , CrestronString _60 , ushort _61 ) 
            { 
            ushort _62 = 0;
            ushort _63 = 0;
            ushort _64 = 0;
            
            CrestronString _65;
            CrestronString _66;
            _65  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
            _66  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
            
            ushort [] _67;
            _67  = new ushort[ 17 ];
            
            CrestronString [] _68;
            _68  = new CrestronString[ 17 ];
            for( uint i = 0; i < 17; i++ )
                _68 [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
            
            CrestronString _69;
            _69  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
            
            CrestronString _70;
            _70  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            
            ushort _71 = 0;
            
            
            __context__.SourceCodeLine = 30;
            _71 = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 30;
            _62 = (ushort) ( 16 ) ; 
            __context__.SourceCodeLine = 31;
            while ( Functions.TestForTrue  ( ( Functions.Find( "License Activation is :" , _60 ))  ) ) 
                { 
                __context__.SourceCodeLine = 31;
                _69  .UpdateValue ( Functions.Remove ( "License Activation is :" , _60 )  ) ; 
                __context__.SourceCodeLine = 31;
                } 
            
            __context__.SourceCodeLine = 31;
            _60  .UpdateValue ( Functions.Remove ( "\u000D" , _60 )  ) ; 
            __context__.SourceCodeLine = 31;
            _70  .UpdateValue ( Functions.Left ( _60 ,  (int) ( (Functions.Length( _60 ) - 1) ) )  ) ; 
            __context__.SourceCodeLine = 31;
            _63 = (ushort) ( _54( __context__ , _58 , (ushort)( _62 ) , ref _67 , ref _68 ) ) ; 
            __context__.SourceCodeLine = 31;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)_63; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( _64  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_64  >= __FN_FORSTART_VAL__1) && (_64  <= __FN_FOREND_VAL__1) ) : ( (_64  <= __FN_FORSTART_VAL__1) && (_64  >= __FN_FOREND_VAL__1) ) ; _64  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 31;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_67[ _64 ] == _61))  ) ) 
                    { 
                    __context__.SourceCodeLine = 31;
                    _65  .UpdateValue ( _55 (  __context__ , _68[ _64 ])  ) ; 
                    __context__.SourceCodeLine = 31;
                    _66  .UpdateValue ( _55 (  __context__ , _70)  ) ; 
                    __context__.SourceCodeLine = 31;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_66 == _65))  ) ) 
                        { 
                        __context__.SourceCodeLine = 31;
                        _71 = (ushort) ( 1 ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 31;
                        _71 = (ushort) ( 0 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 31;
                    break ; 
                    } 
                
                __context__.SourceCodeLine = 31;
                } 
            
            __context__.SourceCodeLine = 31;
            return (ushort)( _71) ; 
            
            }
            
        private ushort _57 (  SplusExecutionContext __context__, ushort _59 ) 
            { 
            ushort _60 = 0;
            ushort _61 = 0;
            
            
            __context__.SourceCodeLine = 31;
            _60 = (ushort) ( ((Functions.GetHSeconds() - _59) / 100) ) ; 
            __context__.SourceCodeLine = 31;
            _61 = (ushort) ( (1800 - _60) ) ; 
            __context__.SourceCodeLine = 31;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _61 < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 31;
                _61 = (ushort) ( 0 ) ; 
                } 
            
            __context__.SourceCodeLine = 31;
            return (ushort)( _61) ; 
            
            }
            
        private CrestronString _58 (  SplusExecutionContext __context__, ushort _60 , ushort _61 ) 
            { 
            CrestronString _62;
            _62  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            
            
            __context__.SourceCodeLine = 31;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_60 == 0))  ) ) 
                { 
                __context__.SourceCodeLine = 31;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_61 == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 32;
                    _62  .UpdateValue ( "Invalid licence / Demo Expired"  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 32;
                    MakeString ( _62 , "Demo mode ({0:d} seconds remaining)", (short)_61) ; 
                    } 
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 32;
                _62  .UpdateValue ( "Licence OK"  ) ; 
                } 
            
            __context__.SourceCodeLine = 32;
            return ( _62 ) ; 
            
            }
            
        Crestron.Logos.SplusObjects.DigitalInput SYSTEM_OSC;
        Crestron.Logos.SplusObjects.DigitalInput REFRESH_STATUS;
        Crestron.Logos.SplusObjects.DigitalInput START_MODULE;
        Crestron.Logos.SplusObjects.DigitalInput SEND_SAVE;
        Crestron.Logos.SplusObjects.StringInput FAVOUTITE_CMD;
        Crestron.Logos.SplusObjects.StringInput POE_CMD;
        Crestron.Logos.SplusObjects.StringInput DEBUGENABLE;
        Crestron.Logos.SplusObjects.StringInput CRESTRON_MAC;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> OUTPUT;
        Crestron.Logos.SplusObjects.DigitalOutput REBOOTINGINPROGRESS;
        Crestron.Logos.SplusObjects.StringOutput OPERATIONFB__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput CRESTRONID__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput VERSION__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput ERRORMSG__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogOutput NUMOFOUTPUTS;
        Crestron.Logos.SplusObjects.AnalogOutput NUMOFINPUTS;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> OUTPUTFB;
        SplusTcpClient SWITCH_CLIENT;
        StringParameter P_SWITCHIP;
        StringParameter P_SWITCHUSERNAME;
        StringParameter P_SWITCHPASSWORD;
        StringParameter P_SWITCHLICENCEKEY;
        _59 _66;
        _67 _72;
        _73 [] _80;
        _73 _81;
        _41 _82;
        ushort _83 = 0;
        ushort _84 = 0;
        ushort _85 = 0;
        ushort _86 = 0;
        CrestronString _87;
        ushort [] _88;
        CrestronString _89;
        ushort _90 = 0;
        CrestronString _91;
        ushort _92 = 0;
        ushort _93 = 0;
        CrestronString _94;
        CrestronString [] _95;
        CrestronString _96;
        ushort _97 = 0;
        ushort _98 = 0;
        ushort _99 = 0;
        ushort _100 = 0;
        CrestronString [] _101;
        CrestronString [] _102;
        ushort _103 = 0;
        ushort _104 = 0;
        ushort _105 = 0;
        ushort _106 = 0;
        ushort _107 = 0;
        ushort _108 = 0;
        ushort _109 = 0;
        private void _110 (  SplusExecutionContext __context__, CrestronString _112 , CrestronString _113 ) 
            { 
            
            __context__.SourceCodeLine = 35;
            if ( Functions.TestForTrue  ( ( _86)  ) ) 
                { 
                __context__.SourceCodeLine = 35;
                Print( "\r\n {0}: {1} \r\n", _112 , _113 ) ; 
                } 
            
            
            }
            
        private CrestronString _111 (  SplusExecutionContext __context__, CrestronString _113 , ushort _3 , ushort _4 ) 
            { 
            ushort _114 = 0;
            
            CrestronString _115;
            _115  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 254, this );
            
            
            __context__.SourceCodeLine = 35;
            _114 = (ushort) ( (_4 - _3) ) ; 
            __context__.SourceCodeLine = 35;
            _115  .UpdateValue ( Functions.Mid ( _113 ,  (int) ( _3 ) ,  (int) ( _114 ) )  ) ; 
            __context__.SourceCodeLine = 35;
            return ( _115 ) ; 
            
            }
            
        private void _112 (  SplusExecutionContext __context__, ushort _64 ) 
            { 
            CrestronString _114;
            _114  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 128, this );
            
            ushort _115 = 0;
            
            
            __context__.SourceCodeLine = 35;
            _115 = (ushort) ( Functions.BoolToInt (_66._64 != _64) ) ; 
            __context__.SourceCodeLine = 35;
            _66 . _64 = (ushort) ( _64 ) ; 
            __context__.SourceCodeLine = 35;
            
                {
                int __SPLS_TMPVAR__SWTCH_6__ = ((int)_64);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 0) ) ) ) 
                        {
                        __context__.SourceCodeLine = 35;
                        _114  .UpdateValue ( "Not connected"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 1) ) ) ) 
                        {
                        __context__.SourceCodeLine = 35;
                        _114  .UpdateValue ( "Connecting"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 2) ) ) ) 
                        {
                        __context__.SourceCodeLine = 35;
                        _114  .UpdateValue ( "Logging in"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 3) ) ) ) 
                        {
                        __context__.SourceCodeLine = 36;
                        _114  .UpdateValue ( "Invalid username or password"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 4) ) ) ) 
                        {
                        __context__.SourceCodeLine = 36;
                        _114  .UpdateValue ( "Invalid privileged password"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 6) ) ) ) 
                        {
                        __context__.SourceCodeLine = 36;
                        _114  .UpdateValue ( "Configuring"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 5) ) ) ) 
                        {
                        __context__.SourceCodeLine = 36;
                        _114  .UpdateValue ( _66 . _65 + Functions.ItoA (  (int) ( _72._69 ) ) + " x " + Functions.ItoA (  (int) ( _72._70 ) )  ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 36;
                        _114  .UpdateValue ( "Unknown"  ) ; 
                        }
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 36;
            OPERATIONFB__DOLLAR__  .UpdateValue ( _114  ) ; 
            
            }
            
        private void _113 (  SplusExecutionContext __context__, _73 _115 ) 
            { 
            ushort _116 = 0;
            
            
            __context__.SourceCodeLine = 36;
            _116 = (ushort) ( _45( __context__ , ref _82 ) ) ; 
            __context__.SourceCodeLine = 36;
            _110 (  __context__ , "MsgQueue_enqueue iAddIndex = ", Functions.ItoA( (int)( _116 ) )) ; 
            __context__.SourceCodeLine = 36;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_116 != _82._44))  ) ) 
                { 
                __context__.SourceCodeLine = 36;
                _80 [ _116] . _74  .UpdateValue ( _115 . _74  ) ; 
                __context__.SourceCodeLine = 36;
                _80 [ _116] . _75  .UpdateValue ( _115 . _75  ) ; 
                __context__.SourceCodeLine = 36;
                _80 [ _116] . _76 = (ushort) ( _115._76 ) ; 
                __context__.SourceCodeLine = 36;
                _80 [ _116] . _77  .UpdateValue ( _115 . _77  ) ; 
                __context__.SourceCodeLine = 36;
                _80 [ _116] . _78 = (ushort) ( _115._78 ) ; 
                } 
            
            
            }
            
        private void _114 (  SplusExecutionContext __context__, ushort _116 , _73 _117 ) 
            { 
            
            __context__.SourceCodeLine = 36;
            _117 . _74  .UpdateValue ( _80 [ _116] . _74  ) ; 
            __context__.SourceCodeLine = 37;
            _117 . _75  .UpdateValue ( _80 [ _116] . _75  ) ; 
            __context__.SourceCodeLine = 37;
            _117 . _76 = (ushort) ( _80[ _116 ]._76 ) ; 
            __context__.SourceCodeLine = 37;
            _117 . _77  .UpdateValue ( _80 [ _116] . _77  ) ; 
            __context__.SourceCodeLine = 37;
            _117 . _78 = (ushort) ( _80[ _116 ]._78 ) ; 
            
            }
            
        private void _115 (  SplusExecutionContext __context__, ref _73 _117 ) 
            { 
            ushort _118 = 0;
            
            
            __context__.SourceCodeLine = 37;
            _118 = (ushort) ( _46( __context__ , _82 ) ) ; 
            __context__.SourceCodeLine = 37;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _118 < _82._44 ))  ) ) 
                { 
                __context__.SourceCodeLine = 37;
                _114 (  __context__ , (ushort)( _118 ), _117) ; 
                __context__.SourceCodeLine = 37;
                _80 [ _118] . _74  .UpdateValue ( "NULL STRING"  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 37;
                _110 (  __context__ , "Queue", "Queue:Error") ; 
                } 
            
            
            }
            
        private ushort _116 (  SplusExecutionContext __context__, CrestronString _118 ) 
            { 
            ushort _119 = 0;
            ushort _120 = 0;
            ushort _121 = 0;
            
            
            __context__.SourceCodeLine = 37;
            _121 = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 37;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( _118 ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( _119  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_119  >= __FN_FORSTART_VAL__1) && (_119  <= __FN_FOREND_VAL__1) ) : ( (_119  <= __FN_FORSTART_VAL__1) && (_119  >= __FN_FOREND_VAL__1) ) ; _119  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 37;
                _120 = (ushort) ( Byte( _118 , (int)( _119 ) ) ) ; 
                __context__.SourceCodeLine = 37;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _120 >= 48 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _120 <= 57 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (_120 == 46) )) ))  ) ) 
                    { 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 37;
                    _121 = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 37;
                    break ; 
                    } 
                
                __context__.SourceCodeLine = 37;
                } 
            
            __context__.SourceCodeLine = 37;
            return (ushort)( _121) ; 
            
            }
            
        private ushort _117 (  SplusExecutionContext __context__, CrestronString _119 ) 
            { 
            ushort [] _120;
            _120  = new ushort[ 4 ];
            
            ushort [] _121;
            _121  = new ushort[ 5 ];
            
            CrestronString _122;
            _122  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64, this );
            
            
            __context__.SourceCodeLine = 38;
            _120 [ 1] = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 38;
            _120 [ 2] = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 38;
            _120 [ 3] = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 38;
            _121 [ 1] = (ushort) ( 256 ) ; 
            __context__.SourceCodeLine = 38;
            _121 [ 2] = (ushort) ( 256 ) ; 
            __context__.SourceCodeLine = 38;
            _121 [ 3] = (ushort) ( 256 ) ; 
            __context__.SourceCodeLine = 38;
            _121 [ 4] = (ushort) ( 256 ) ; 
            __context__.SourceCodeLine = 38;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( _119 ) <= 15 ) ) && Functions.TestForTrue ( _116( __context__ , _119 ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 38;
                _120 [ 1] = (ushort) ( Functions.Find( "." , _119 , 1 ) ) ; 
                __context__.SourceCodeLine = 38;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _120[ 1 ] > 1 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 38;
                    _121 [ 1] = (ushort) ( Functions.Atoi( Functions.Left( _119 , (int)( (_120[ 1 ] - 1) ) ) ) ) ; 
                    __context__.SourceCodeLine = 38;
                    _120 [ 2] = (ushort) ( Functions.Find( "." , _119 , (_120[ 1 ] + 1) ) ) ; 
                    __context__.SourceCodeLine = 38;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (_120[ 2 ] + 1) > _120[ 1 ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 38;
                        _121 [ 2] = (ushort) ( Functions.Atoi( Functions.Mid( _119 , (int)( (_120[ 1 ] + 1) ) , (int)( ((_120[ 2 ] - _120[ 1 ]) - 1) ) ) ) ) ; 
                        __context__.SourceCodeLine = 38;
                        _120 [ 3] = (ushort) ( Functions.Find( "." , _119 , (_120[ 2 ] + 1) ) ) ; 
                        __context__.SourceCodeLine = 38;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (_120[ 3 ] + 1) > _120[ 2 ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 38;
                            _121 [ 3] = (ushort) ( Functions.Atoi( Functions.Mid( _119 , (int)( (_120[ 2 ] + 1) ) , (int)( ((_120[ 3 ] - _120[ 2 ]) - 1) ) ) ) ) ; 
                            __context__.SourceCodeLine = 38;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( _119 ) > _120[ 3 ] ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( _119 ) < (_120[ 3 ] + 4) ) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 38;
                                _121 [ 4] = (ushort) ( Functions.Atoi( Functions.Right( _119 , (int)( (Functions.Length( _119 ) - _120[ 3 ]) ) ) ) ) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    } 
                
                } 
            
            __context__.SourceCodeLine = 39;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _121[ 1 ] < 256 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _121[ 2 ] < 256 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _121[ 3 ] < 256 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _121[ 4 ] < 256 ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 39;
                return (ushort)( 65535) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 39;
                return (ushort)( 0) ; 
                } 
            
            
            return 0; // default return value (none specified in module)
            }
            
        private void _118 (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 39;
            _66 . _60  .UpdateValue ( ""  ) ; 
            
            }
            
        private void _119 (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 39;
            _110 (  __context__ , "MessageHandler g_MessageHandlerComms.connectState", Functions.ItoA( (int)( _66._64 ) )) ; 
            __context__.SourceCodeLine = 39;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _66._64 > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 39;
                _48 (  __context__ ,   ref  _82 ) ; 
                __context__.SourceCodeLine = 39;
                Functions.SocketDisconnectClient ( SWITCH_CLIENT ) ; 
                __context__.SourceCodeLine = 39;
                _112 (  __context__ , (ushort)( 0 )) ; 
                } 
            
            
            }
            
        private void _120 (  SplusExecutionContext __context__ ) 
            { 
            CrestronString _122;
            _122  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64, this );
            
            short _123 = 0;
            short _124 = 0;
            
            
            __context__.SourceCodeLine = 39;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_66._64 == 0))  ) ) 
                { 
                __context__.SourceCodeLine = 39;
                _81 . _74  .UpdateValue ( "NULL STRING"  ) ; 
                __context__.SourceCodeLine = 39;
                _81 . _75  .UpdateValue ( "NULL STRING"  ) ; 
                __context__.SourceCodeLine = 39;
                _81 . _76 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 39;
                _81 . _77  .UpdateValue ( "NULL STRING"  ) ; 
                __context__.SourceCodeLine = 39;
                _81 . _78 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 39;
                _112 (  __context__ , (ushort)( 1 )) ; 
                __context__.SourceCodeLine = 39;
                _66 . _60  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 39;
                if ( Functions.TestForTrue  ( ( _117( __context__ , P_SWITCHIP  ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 40;
                    _110 (  __context__ , "MessageHandler", "SocketConnectClient") ; 
                    __context__.SourceCodeLine = 40;
                    _123 = (short) ( Functions.SocketConnectClient( SWITCH_CLIENT , P_SWITCHIP  , (ushort)( 23 ) , (ushort)( 1 ) ) ) ; 
                    __context__.SourceCodeLine = 40;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_123 == -7) ) || Functions.TestForTrue ( Functions.BoolToInt (_123 == -8) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 40;
                        _110 (  __context__ , "MessageHandler", "Switch - Connection attempt in progress") ; 
                        __context__.SourceCodeLine = 40;
                        return ; 
                        } 
                    
                    __context__.SourceCodeLine = 40;
                    Functions.Delay (  (int) ( 1000 ) ) ; 
                    __context__.SourceCodeLine = 40;
                    _124 = (short) ( Functions.SocketGetAddressAsRequested( SWITCH_CLIENT , ref _122 ) ) ; 
                    __context__.SourceCodeLine = 40;
                    _89  .UpdateValue ( "Switch - Attempting to Connect to device at" + P_SWITCHIP + ":" + Functions.ItoA (  (int) ( 23 ) )  ) ; 
                    __context__.SourceCodeLine = 40;
                    _110 (  __context__ , "MessageHandler", _89) ; 
                    __context__.SourceCodeLine = 40;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_123 != 0) ) || Functions.TestForTrue ( Functions.BoolToInt (_124 != 0) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 41;
                        _89  .UpdateValue ( "Switch - IP Client Connect Error: " + Functions.ItoA (  (int) ( _123 ) ) + " , " + Functions.ItoA (  (int) ( _124 ) ) + " Address: " + _122  ) ; 
                        __context__.SourceCodeLine = 41;
                        _110 (  __context__ , "MessageHandler", _89) ; 
                        } 
                    
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 41;
                    _89  .UpdateValue ( "Switch - Stored IP Address is Invalid: " + P_SWITCHIP  ) ; 
                    __context__.SourceCodeLine = 41;
                    _110 (  __context__ , "MessageHandler", _89) ; 
                    } 
                
                } 
            
            
            }
            
        private void _121 (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 41;
            _119 (  __context__  ) ; 
            __context__.SourceCodeLine = 41;
            _120 (  __context__  ) ; 
            
            }
            
        private void _122 (  SplusExecutionContext __context__, CrestronString _124 ) 
            { 
            CrestronString _125;
            _125  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5000, this );
            
            ushort _126 = 0;
            
            
            __context__.SourceCodeLine = 41;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_124 != "NULL STRING"))  ) ) 
                { 
                __context__.SourceCodeLine = 41;
                _126 = (ushort) ( Functions.Find( "#NULL#" , _124 ) ) ; 
                __context__.SourceCodeLine = 41;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _126 > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 41;
                    _125  .UpdateValue ( Functions.Left ( _124 ,  (int) ( (_126 - 1) ) )  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 41;
                    _125  .UpdateValue ( _124 + "\r\n"  ) ; 
                    } 
                
                __context__.SourceCodeLine = 41;
                _110 (  __context__ , "fnMessageHandler_sendCiscoCommand command ", _125) ; 
                __context__.SourceCodeLine = 41;
                Functions.SocketSend ( SWITCH_CLIENT , _125 ) ; 
                } 
            
            
            }
            
        private void _123 (  SplusExecutionContext __context__, _73 _125 ) 
            { 
            
            __context__.SourceCodeLine = 42;
            _81 . _74  .UpdateValue ( _125 . _74  ) ; 
            __context__.SourceCodeLine = 42;
            _81 . _75  .UpdateValue ( _125 . _75  ) ; 
            __context__.SourceCodeLine = 42;
            _81 . _76 = (ushort) ( _125._76 ) ; 
            __context__.SourceCodeLine = 42;
            _81 . _77  .UpdateValue ( _125 . _77  ) ; 
            __context__.SourceCodeLine = 42;
            _81 . _78 = (ushort) ( _125._78 ) ; 
            __context__.SourceCodeLine = 42;
            _85 = (ushort) ( (_85 + 1) ) ; 
            __context__.SourceCodeLine = 42;
            _108 = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 42;
            CreateWait ( "WAIT_FOR_RX_SEND" , 10000 , WAIT_FOR_RX_SEND_Callback ) ;
            __context__.SourceCodeLine = 42;
            _122 (  __context__ , _81._74) ; 
            
            }
            
        public void WAIT_FOR_RX_SEND_CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            __context__.SourceCodeLine = 42;
            _121 (  __context__  ) ; 
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void _124 (  SplusExecutionContext __context__, CrestronString _126 ) 
        { 
        
        __context__.SourceCodeLine = 42;
        _112 (  __context__ , (ushort)( 3 )) ; 
        
        }
        
    private void _125 (  SplusExecutionContext __context__, CrestronString _127 ) 
        { 
        _73 _128;
        _128  = new _73( this, true );
        _128 .PopulateCustomAttributeList( false );
        
        
        __context__.SourceCodeLine = 42;
        _110 (  __context__ , "MessageHandler_sendPassword", "MessageHandler_sendPassword") ; 
        __context__.SourceCodeLine = 42;
        _128 . _74  .UpdateValue ( P_SWITCHPASSWORD  ) ; 
        __context__.SourceCodeLine = 42;
        _128 . _75  .UpdateValue ( "#"  ) ; 
        __context__.SourceCodeLine = 42;
        _128 . _76 = (ushort) ( 6 ) ; 
        __context__.SourceCodeLine = 42;
        _128 . _77  .UpdateValue ( "User Name:"  ) ; 
        __context__.SourceCodeLine = 42;
        _128 . _78 = (ushort) ( 4 ) ; 
        __context__.SourceCodeLine = 42;
        _123 (  __context__ , _128) ; 
        
        }
        
    private void _126 (  SplusExecutionContext __context__, CrestronString _128 ) 
        { 
        _73 _129;
        _129  = new _73( this, true );
        _129 .PopulateCustomAttributeList( false );
        
        
        __context__.SourceCodeLine = 43;
        _110 (  __context__ , "MessageHandler_sendUsername", "MessageHandler_sendUsername") ; 
        __context__.SourceCodeLine = 43;
        _129 . _74  .UpdateValue ( P_SWITCHUSERNAME  ) ; 
        __context__.SourceCodeLine = 43;
        _129 . _75  .UpdateValue ( "Password:"  ) ; 
        __context__.SourceCodeLine = 43;
        _129 . _76 = (ushort) ( 2 ) ; 
        __context__.SourceCodeLine = 43;
        _129 . _77  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 43;
        _129 . _78 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 43;
        _123 (  __context__ , _129) ; 
        
        }
        
    private void _127 (  SplusExecutionContext __context__ ) 
        { 
        _73 _129;
        _129  = new _73( this, true );
        _129 .PopulateCustomAttributeList( false );
        
        
        __context__.SourceCodeLine = 43;
        _110 (  __context__ , "MessageHandler_beginLoginSequence", "MessageHandler_beginLoginSequence") ; 
        __context__.SourceCodeLine = 43;
        _129 . _74  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 43;
        _129 . _75  .UpdateValue ( "User Name:"  ) ; 
        __context__.SourceCodeLine = 43;
        _129 . _76 = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 43;
        _129 . _77  .UpdateValue ( "#"  ) ; 
        __context__.SourceCodeLine = 43;
        _129 . _78 = (ushort) ( 6 ) ; 
        
        }
        
    private void _128 (  SplusExecutionContext __context__, ushort _130 ) 
        { 
        
        __context__.SourceCodeLine = 43;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _130 > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 43;
            _112 (  __context__ , (ushort)( 2 )) ; 
            __context__.SourceCodeLine = 43;
            _127 (  __context__  ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 43;
            _112 (  __context__ , (ushort)( 0 )) ; 
            __context__.SourceCodeLine = 43;
            _48 (  __context__ ,   ref  _82 ) ; 
            } 
        
        
        }
        
    private void _129 (  SplusExecutionContext __context__, CrestronString _131 , ushort _132 , CrestronString _133 , ushort _134 , CrestronString _135 ) 
        { 
        ushort _136 = 0;
        
        _73 _137;
        _137  = new _73( this, true );
        _137 .PopulateCustomAttributeList( false );
        
        
        __context__.SourceCodeLine = 44;
        _137 . _74  .UpdateValue ( _131  ) ; 
        __context__.SourceCodeLine = 44;
        _137 . _75  .UpdateValue ( _133  ) ; 
        __context__.SourceCodeLine = 44;
        _137 . _76 = (ushort) ( _132 ) ; 
        __context__.SourceCodeLine = 44;
        _137 . _77  .UpdateValue ( _135  ) ; 
        __context__.SourceCodeLine = 44;
        _137 . _78 = (ushort) ( _132 ) ; 
        __context__.SourceCodeLine = 44;
        _110 (  __context__ , "MessageHandler_sendMessage", _131) ; 
        __context__.SourceCodeLine = 44;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _66._64 >= 5 ) ) && Functions.TestForTrue ( Functions.BoolToInt (_81._74 == "NULL STRING") )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 44;
            _123 (  __context__ , _137) ; 
            __context__.SourceCodeLine = 44;
            _110 (  __context__ , "MessageHandler_sendMessage", "MessageHandler_sendNow") ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 44;
            _113 (  __context__ , _137) ; 
            __context__.SourceCodeLine = 44;
            _110 (  __context__ , "MessageHandler_sendMessage", "queue_enqueue") ; 
            __context__.SourceCodeLine = 44;
            _120 (  __context__  ) ; 
            } 
        
        
        }
        
    private void _130 (  SplusExecutionContext __context__, CrestronString _132 , ushort _133 , CrestronString _134 , ushort _135 , CrestronString _136 ) 
        { 
        ushort _137 = 0;
        
        _73 _138;
        _138  = new _73( this, true );
        _138 .PopulateCustomAttributeList( false );
        
        
        __context__.SourceCodeLine = 44;
        _138 . _74  .UpdateValue ( _132  ) ; 
        __context__.SourceCodeLine = 44;
        _138 . _75  .UpdateValue ( _134  ) ; 
        __context__.SourceCodeLine = 44;
        _138 . _76 = (ushort) ( _133 ) ; 
        __context__.SourceCodeLine = 44;
        _138 . _77  .UpdateValue ( _136  ) ; 
        __context__.SourceCodeLine = 44;
        _138 . _78 = (ushort) ( _133 ) ; 
        __context__.SourceCodeLine = 44;
        _113 (  __context__ , _138) ; 
        __context__.SourceCodeLine = 45;
        _110 (  __context__ , "MessageHandler_sendMessage", "queue_enqueue") ; 
        
        }
        
    private void _131 (  SplusExecutionContext __context__ ) 
        { 
        _73 _133;
        _133  = new _73( this, true );
        _133 .PopulateCustomAttributeList( false );
        
        
        __context__.SourceCodeLine = 45;
        _115 (  __context__ ,   ref  _133 ) ; 
        __context__.SourceCodeLine = 45;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_133._74 == "NULL STRING"))  ) ) 
            { 
            __context__.SourceCodeLine = 45;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_13__" , 10 , __SPLS_TMPVAR__WAITLABEL_13___Callback ) ;
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 45;
            _123 (  __context__ , _133) ; 
            } 
        
        
        }
        
    public void __SPLS_TMPVAR__WAITLABEL_13___CallbackFn( object stateInfo )
    {
    
        try
        {
            Wait __LocalWait__ = (Wait)stateInfo;
            SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
            __LocalWait__.RemoveFromList();
            
            
            __context__.SourceCodeLine = 45;
            _110 (  __context__ , "Wait 10 - NULL_MESSAGE", "") ; 
            __context__.SourceCodeLine = 45;
            _131 (  __context__  ) ; 
            
        
        
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        
    }
    
private void _132 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 45;
    _110 (  __context__ , "MessageHandler", "Closing TCP connection comms") ; 
    __context__.SourceCodeLine = 45;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_66._62 != ""))  ) ) 
        { 
        __context__.SourceCodeLine = 45;
        _66 . _62  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 45;
        _66 . _63 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 45;
        _48 (  __context__ ,   ref  _82 ) ; 
        __context__.SourceCodeLine = 45;
        _119 (  __context__  ) ; 
        __context__.SourceCodeLine = 45;
        _112 (  __context__ , (ushort)( 0 )) ; 
        } 
    
    
    }
    
private void _133 (  SplusExecutionContext __context__, CrestronString _62 , ushort _63 ) 
    { 
    
    __context__.SourceCodeLine = 45;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_66._64 != 0))  ) ) 
        { 
        __context__.SourceCodeLine = 45;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_66._62 != "") ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_66._62 != _62) ) || Functions.TestForTrue ( Functions.BoolToInt (_66._63 != _63) )) ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 45;
            _132 (  __context__  ) ; 
            } 
        
        } 
    
    __context__.SourceCodeLine = 45;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_66._64 == 0))  ) ) 
        { 
        __context__.SourceCodeLine = 45;
        _66 . _62  .UpdateValue ( _62  ) ; 
        __context__.SourceCodeLine = 45;
        _66 . _63 = (ushort) ( _63 ) ; 
        __context__.SourceCodeLine = 45;
        _120 (  __context__  ) ; 
        } 
    
    
    }
    
private void _134 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 46;
    _129 (  __context__ , "", (ushort)( 0 ), "#", (ushort)( 0 ), "") ; 
    __context__.SourceCodeLine = 46;
    _112 (  __context__ , (ushort)( _66._64 )) ; 
    
    }
    
private void _135 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 46;
    _66 . _60  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 46;
    _66 . _61 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 46;
    _66 . _62  .UpdateValue ( P_SWITCHIP  ) ; 
    __context__.SourceCodeLine = 46;
    _66 . _63 = (ushort) ( 23 ) ; 
    __context__.SourceCodeLine = 46;
    _66 . _64 = (ushort) ( Functions.ToInteger( -( 1 ) ) ) ; 
    __context__.SourceCodeLine = 46;
    _112 (  __context__ , (ushort)( 0 )) ; 
    
    }
    
private void _136 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 46;
    _133 (  __context__ , P_SWITCHIP , (ushort)( 23 )) ; 
    
    }
    
private CrestronString _137 (  SplusExecutionContext __context__, ushort _139 ) 
    { 
    CrestronString _140;
    _140  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    ushort _141 = 0;
    ushort _142 = 0;
    ushort _143 = 0;
    
    
    __context__.SourceCodeLine = 46;
    _141 = (ushort) ( ((_83 + 11) - 1) ) ; 
    __context__.SourceCodeLine = 46;
    _142 = (ushort) ( (_139 + 11) ) ; 
    __context__.SourceCodeLine = 46;
    _143 = (ushort) ( (10 - 1) ) ; 
    __context__.SourceCodeLine = 46;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_139 == (_83 - 1)))  ) ) 
        { 
        __context__.SourceCodeLine = 46;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_83 == 1))  ) ) 
            {
            __context__.SourceCodeLine = 46;
            MakeString ( _140 , "1-{0:d}", (short)_143) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 46;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_83 == 2))  ) ) 
                {
                __context__.SourceCodeLine = 46;
                MakeString ( _140 , "1-{0:d},{1:d}", (short)_143, (short)11) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 46;
                MakeString ( _140 , "1-{0:d},{1:d}-{2:d}", (short)_143, (short)11, (short)(_141 - 1)) ; 
                }
            
            }
        
        } 
    
    else 
        {
        __context__.SourceCodeLine = 46;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_139 == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 46;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_83 == 2))  ) ) 
                {
                __context__.SourceCodeLine = 46;
                MakeString ( _140 , "1-{0:d},{1:d}", (short)_143, (short)(11 + 1)) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 47;
                MakeString ( _140 , "1-{0:d},{1:d}-{2:d}", (short)_143, (short)(11 + 1), (short)_141) ; 
                }
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 47;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_139 == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 47;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_83 == 3))  ) ) 
                    {
                    __context__.SourceCodeLine = 47;
                    MakeString ( _140 , "1-{0:d},{1:d},{2:d}", (short)_143, (short)11, (short)(11 + 2)) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 47;
                    MakeString ( _140 , "1-{0:d},{1:d},{2:d}-{3:d}", (short)_143, (short)11, (short)(_142 + 1), (short)_141) ; 
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 47;
                MakeString ( _140 , "1-{0:d},{1:d}-{2:d},{3:d}-{4:d}", (short)_143, (short)11, (short)(_142 - 1), (short)(_142 + 1), (short)_141) ; 
                } 
            
            }
        
        }
    
    __context__.SourceCodeLine = 47;
    return ( _140 ) ; 
    
    }
    
private void _138 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 47;
    _110 (  __context__ , " fnCiscoSwitch_handleSaveStatus", "") ; 
    __context__.SourceCodeLine = 47;
    _129 (  __context__ , "copy running-config startup-config", (ushort)( 15 ), "Overwrite file [startup-config].... (Y/N)", (ushort)( 0 ), "") ; 
    
    }
    
private ushort _139 (  SplusExecutionContext __context__, CrestronString _141 , short _142 , short [,] _143 , ref CrestronString [] _144 ) 
    { 
    short _145 = 0;
    
    ushort _146 = 0;
    ushort _147 = 0;
    ushort _148 = 0;
    ushort _149 = 0;
    ushort _150 = 0;
    ushort _151 = 0;
    ushort _152 = 0;
    ushort _153 = 0;
    ushort _154 = 0;
    
    CrestronString _155;
    CrestronString _156;
    CrestronString _157;
    _155  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    _156  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 254, this );
    _157  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    
    _0 _158;
    _158  = new _0( this, true );
    _158 .PopulateCustomAttributeList( false );
    
    
    __context__.SourceCodeLine = 48;
    _145 = (short) ( 0 ) ; 
    __context__.SourceCodeLine = 48;
    _146 = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 48;
    _147 = (ushort) ( Functions.Length( _141 ) ) ; 
    __context__.SourceCodeLine = 48;
    do 
        { 
        __context__.SourceCodeLine = 48;
        _154 = (ushort) ( Functions.Find( "," , _141 , _146 ) ) ; 
        __context__.SourceCodeLine = 48;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_154 == 0))  ) ) 
            {
            __context__.SourceCodeLine = 48;
            _148 = (ushort) ( _147 ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 48;
            _148 = (ushort) ( _154 ) ; 
            }
        
        __context__.SourceCodeLine = 48;
        _156  .UpdateValue ( "gi(%d*)/?(%d*)/?(%d*)-?(%d*)"  ) ; 
        __context__.SourceCodeLine = 48;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _37( __context__ , _141 , (ushort)( _148 ) , (short)( _146 ) , _156 , (ushort)( Functions.Length( _156 ) ) , ref _158 ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 48;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _158._4[ 3 ] > _158._3[ 3 ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 48;
                _150 = (ushort) ( (Functions.Atoi( _111( __context__ , _141 , (ushort)( _158._3[ 1 ] ) , (ushort)( _158._4[ 1 ] ) ) ) - 1) ) ; 
                __context__.SourceCodeLine = 48;
                _151 = (ushort) ( 3 ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 48;
                _150 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 48;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _158._4[ 2 ] > _158._3[ 2 ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 48;
                    _151 = (ushort) ( 2 ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 48;
                    _151 = (ushort) ( 1 ) ; 
                    } 
                
                } 
            
            __context__.SourceCodeLine = 48;
            _152 = (ushort) ( Functions.Atoi( _111( __context__ , _141 , (ushort)( _158._3[ _151 ] ) , (ushort)( _158._4[ _151 ] ) ) ) ) ; 
            __context__.SourceCodeLine = 48;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _158._4[ 4 ] > _158._3[ 4 ] ))  ) ) 
                {
                __context__.SourceCodeLine = 49;
                _153 = (ushort) ( Functions.Atoi( _111( __context__ , _141 , (ushort)( _158._3[ 4 ] ) , (ushort)( _158._4[ 4 ] ) ) ) ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 49;
                _153 = (ushort) ( _152 ) ; 
                }
            
            __context__.SourceCodeLine = 49;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _152 < 1 ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _153 > 52 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _152 > _153 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _150 < 0 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _150 >= 8 ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 49;
                _110 (  __context__ , "_recordPortsInMap", "Invalid port name in VLAN port list") ; 
                __context__.SourceCodeLine = 49;
                _145 = (short) ( Functions.ToSignedInteger( -( 1000 ) ) ) ; 
                __context__.SourceCodeLine = 49;
                break ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 49;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Length( _144[ _150 ] ) == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 49;
                    _144 [ _150]  .UpdateValue ( _141  ) ; 
                    __context__.SourceCodeLine = 49;
                    _144 [ _150]  .UpdateValue ( Functions.Mid ( _144 [ _150] ,  (int) ( _146 ) ,  (int) ( (_158._3[ _151 ] - _146) ) )  ) ; 
                    } 
                
                __context__.SourceCodeLine = 49;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( _152 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)_153; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( _149  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_149  >= __FN_FORSTART_VAL__1) && (_149  <= __FN_FOREND_VAL__1) ) : ( (_149  <= __FN_FORSTART_VAL__1) && (_149  >= __FN_FOREND_VAL__1) ) ; _149  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 49;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_143[ _150 , _149 ] == 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 49;
                        _143 [ _150 , _149] = (short) ( _142 ) ; 
                        __context__.SourceCodeLine = 49;
                        _145 = (short) ( (_145 + 1) ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 49;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_143[ _150 , _149 ] != Functions.ToSignedLongInteger( -( _142 ) )))  ) ) 
                            { 
                            __context__.SourceCodeLine = 49;
                            _110 (  __context__ , "_recordPortsInMap", "Port appears on more than one TX VLAN") ; 
                            __context__.SourceCodeLine = 49;
                            _145 = (short) ( Functions.ToSignedInteger( -( 1000 ) ) ) ; 
                            } 
                        
                        }
                    
                    __context__.SourceCodeLine = 49;
                    } 
                
                __context__.SourceCodeLine = 49;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _153 > _143[ _150 , 0 ] ))  ) ) 
                    {
                    __context__.SourceCodeLine = 50;
                    _143 [ _150 , 0] = (short) ( _153 ) ; 
                    }
                
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 50;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_148 != _146))  ) ) 
                { 
                __context__.SourceCodeLine = 50;
                _110 (  __context__ , "_recordPortsInMap", "Unexpected port list format") ; 
                __context__.SourceCodeLine = 50;
                _145 = (short) ( Functions.ToSignedInteger( -( 1000 ) ) ) ; 
                __context__.SourceCodeLine = 50;
                break ; 
                } 
            
            }
        
        __context__.SourceCodeLine = 50;
        _146 = (ushort) ( (_148 + 1) ) ; 
        } 
    while (false == ( Functions.TestForTrue  ( Functions.BoolToInt (_148 == _147)) )); 
    __context__.SourceCodeLine = 50;
    return (ushort)( _145) ; 
    
    }
    
private void _140 (  SplusExecutionContext __context__, ushort _142 , CrestronString _143 ) 
    { 
    CrestronString _144;
    CrestronString _145;
    CrestronString _146;
    _144  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    _145  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    _146  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
    
    
    __context__.SourceCodeLine = 50;
    _144  .UpdateValue ( "10," + Functions.ItoA (  (int) ( (_142 + 10) ) )  ) ; 
    __context__.SourceCodeLine = 50;
    _145  .UpdateValue ( _137 (  __context__ , (ushort)( (_142 - 1) ))  ) ; 
    __context__.SourceCodeLine = 50;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_143 != ""))  ) ) 
        { 
        __context__.SourceCodeLine = 50;
        _130 (  __context__ , "configure", (ushort)( 0 ), "(config)#", (ushort)( 0 ), "") ; 
        __context__.SourceCodeLine = 50;
        MakeString ( _146 , "interface range {0}", _143 ) ; 
        __context__.SourceCodeLine = 50;
        _130 (  __context__ , _146, (ushort)( 0 ), "#", (ushort)( 15 ), "Do you wish to continue") ; 
        __context__.SourceCodeLine = 50;
        MakeString ( _146 , "switchport general allowed vlan add {0} untagged", _144 ) ; 
        __context__.SourceCodeLine = 51;
        _130 (  __context__ , _146, (ushort)( 0 ), "#", (ushort)( 0 ), "") ; 
        __context__.SourceCodeLine = 51;
        MakeString ( _146 , "switchport general allowed vlan remove {0}", _145 ) ; 
        __context__.SourceCodeLine = 51;
        _130 (  __context__ , _146, (ushort)( 0 ), "#", (ushort)( 0 ), "") ; 
        __context__.SourceCodeLine = 51;
        _130 (  __context__ , "exit", (ushort)( 0 ), "#", (ushort)( 0 ), "") ; 
        __context__.SourceCodeLine = 51;
        _130 (  __context__ , "exit", (ushort)( 0 ), "#", (ushort)( 0 ), "") ; 
        __context__.SourceCodeLine = 51;
        _131 (  __context__  ) ; 
        } 
    
    
    }
    
private ushort _141 (  SplusExecutionContext __context__, ushort _143 , CrestronString [] _144 ) 
    { 
    CrestronString _145;
    CrestronString _146;
    CrestronString _147;
    _145  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5000, this );
    _146  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    _147  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    
    ushort _148 = 0;
    ushort _149 = 0;
    ushort _150 = 0;
    
    
    __context__.SourceCodeLine = 51;
    _150 = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 51;
    _148 = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 51;
    while ( Functions.TestForTrue  ( ( Functions.BoolToInt (_144[ _150 ] != ""))  ) ) 
        { 
        __context__.SourceCodeLine = 51;
        MakeString ( _146 , "physicalRange[{0:d}]= {1}", (short)_150, _144 [ _150] ) ; 
        __context__.SourceCodeLine = 51;
        _110 (  __context__ , "CiscoComms", _146) ; 
        __context__.SourceCodeLine = 51;
        _150 = (ushort) ( (_150 + 1) ) ; 
        __context__.SourceCodeLine = 51;
        } 
    
    __context__.SourceCodeLine = 51;
    _150 = (ushort) ( (_150 - 1) ) ; 
    __context__.SourceCodeLine = 51;
    MakeString ( _146 , "switchPhysicalRange rangeLength = {0:d}", (short)_150) ; 
    __context__.SourceCodeLine = 51;
    _110 (  __context__ , "CiscoComms", _146) ; 
    __context__.SourceCodeLine = 52;
    _149 = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 52;
    ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
    ushort __FN_FOREND_VAL__1 = (ushort)_150; 
    int __FN_FORSTEP_VAL__1 = (int)1; 
    for ( _148  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_148  >= __FN_FORSTART_VAL__1) && (_148  <= __FN_FOREND_VAL__1) ) : ( (_148  <= __FN_FORSTART_VAL__1) && (_148  >= __FN_FOREND_VAL__1) ) ; _148  += (ushort)__FN_FORSTEP_VAL__1) 
        { 
        __context__.SourceCodeLine = 52;
        MakeString ( _146 , " switchPhysicalRange physicalRange[{0:d}] = {1}", (short)_148, _144 [ _148] ) ; 
        __context__.SourceCodeLine = 52;
        _110 (  __context__ , "CiscoComms", _146) ; 
        __context__.SourceCodeLine = 52;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_145 != "") ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( _145 ) < 80 ) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 52;
            _145  .UpdateValue ( _145 + ","  ) ; 
            }
        
        __context__.SourceCodeLine = 52;
        _145  .UpdateValue ( _145 + _144 [ _148]  ) ; 
        __context__.SourceCodeLine = 52;
        MakeString ( _146 , " switchPhysicalRange range = {0}", _144 [ _149] ) ; 
        __context__.SourceCodeLine = 52;
        _110 (  __context__ , "CiscoComms", _146) ; 
        __context__.SourceCodeLine = 52;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( _145 ) > 80 ))  ) ) 
            { 
            __context__.SourceCodeLine = 52;
            _140 (  __context__ , (ushort)( _143 ), _145) ; 
            __context__.SourceCodeLine = 52;
            _145  .UpdateValue ( ""  ) ; 
            } 
        
        __context__.SourceCodeLine = 52;
        } 
    
    __context__.SourceCodeLine = 52;
    _140 (  __context__ , (ushort)( _143 ), _145) ; 
    
    return 0; // default return value (none specified in module)
    }
    
private void _142 (  SplusExecutionContext __context__, CrestronString _144 , CrestronString _63 , CrestronString _145 ) 
    { 
    ushort _146 = 0;
    
    CrestronString _147;
    CrestronString _148;
    _147  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    _148  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 25, this );
    
    CrestronString _149;
    CrestronString _150;
    CrestronString _151;
    _149  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    _150  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    _151  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    
    
    __context__.SourceCodeLine = 52;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_96 == "SG300"))  ) ) 
        { 
        __context__.SourceCodeLine = 52;
        MakeString ( _149 , "gi{0}", _63 ) ; 
        } 
    
    else 
        {
        __context__.SourceCodeLine = 53;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_96 == "SG500-Standalone"))  ) ) 
            { 
            __context__.SourceCodeLine = 53;
            MakeString ( _149 , "gi{0}/{1}", _144 , _63 ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 53;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_96 == "SG500-Stack"))  ) ) 
                { 
                __context__.SourceCodeLine = 53;
                MakeString ( _149 , "gi{0}/1/{1}", _144 , _63 ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 53;
                _96  .UpdateValue ( "null"  ) ; 
                } 
            
            }
        
        }
    
    __context__.SourceCodeLine = 53;
    _110 (  __context__ , "fnCiscoSwitch_ChangePOE() g_StackForPOE = ", _96) ; 
    __context__.SourceCodeLine = 53;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_96 != "null"))  ) ) 
        { 
        __context__.SourceCodeLine = 53;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_145 == "ON"))  ) ) 
            { 
            __context__.SourceCodeLine = 53;
            _148  .UpdateValue ( "auto"  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 53;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_145 == "OFF"))  ) ) 
                { 
                __context__.SourceCodeLine = 53;
                _148  .UpdateValue ( "never"  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 53;
                _148  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 53;
                _110 (  __context__ , "CiscoSwitch_ChangePOE", "UNKNOWN ACTION") ; 
                } 
            
            }
        
        __context__.SourceCodeLine = 53;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_148 != ""))  ) ) 
            { 
            __context__.SourceCodeLine = 53;
            _130 (  __context__ , "configure", (ushort)( 0 ), "(config)#", (ushort)( 0 ), "") ; 
            __context__.SourceCodeLine = 53;
            MakeString ( _147 , "interface {0}\r\npower inline {1}\r\nexit", _149 , _148 ) ; 
            __context__.SourceCodeLine = 53;
            _130 (  __context__ , _147, (ushort)( 0 ), "(config)#", (ushort)( 0 ), "") ; 
            __context__.SourceCodeLine = 54;
            _130 (  __context__ , "exit", (ushort)( 0 ), "#", (ushort)( 0 ), "") ; 
            __context__.SourceCodeLine = 54;
            _131 (  __context__  ) ; 
            } 
        
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 54;
        _110 (  __context__ , "CiscoSwitch_ChangePOE", "SWITCH DOESN'T SUPPORT POE") ; 
        } 
    
    
    }
    
private void _143 (  SplusExecutionContext __context__, ushort _145 , CrestronString _146 ) 
    { 
    ushort [] _147;
    ushort _148 = 0;
    ushort _149 = 0;
    ushort _150 = 0;
    _147  = new ushort[ 405 ];
    
    CrestronString _151;
    _151  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 404, this );
    
    CrestronString _152;
    _152  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 404, this );
    
    CrestronString _153;
    CrestronString [] _154;
    _153  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
    _154  = new CrestronString[ 256 ];
    for( uint i = 0; i < 256; i++ )
        _154 [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    
    CrestronString _3;
    _3  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10, this );
    
    CrestronString _4;
    _4  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 3, this );
    
    ushort _155 = 0;
    
    ushort _156 = 0;
    
    CrestronString _157;
    _157  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10, this );
    
    ushort _158 = 0;
    
    CrestronString [] _159;
    _159  = new CrestronString[ 101 ];
    for( uint i = 0; i < 101; i++ )
        _159 [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10, this );
    
    CrestronString _160;
    _160  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    
    _0 _161;
    _161  = new _0( this, true );
    _161 .PopulateCustomAttributeList( false );
    
    
    __context__.SourceCodeLine = 54;
    _150 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 54;
    _151  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 54;
    _152  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 54;
    _153  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 54;
    _3  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 54;
    _4  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 54;
    _155 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 54;
    ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
    ushort __FN_FOREND_VAL__1 = (ushort)404; 
    int __FN_FORSTEP_VAL__1 = (int)1; 
    for ( _156  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_156  >= __FN_FORSTART_VAL__1) && (_156  <= __FN_FOREND_VAL__1) ) : ( (_156  <= __FN_FORSTART_VAL__1) && (_156  >= __FN_FOREND_VAL__1) ) ; _156  += (ushort)__FN_FORSTEP_VAL__1) 
        { 
        __context__.SourceCodeLine = 54;
        _147 [ _156] = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 54;
        } 
    
    __context__.SourceCodeLine = 54;
    MakeString ( _160 , " Input: {0:d} Output: {1}", (short)_145, _146 ) ; 
    __context__.SourceCodeLine = 55;
    _110 (  __context__ , "createRangeInterface()", _160) ; 
    __context__.SourceCodeLine = 55;
    do 
        { 
        __context__.SourceCodeLine = 55;
        _148 = (ushort) ( Functions.Find( "," , _146 , 1 ) ) ; 
        __context__.SourceCodeLine = 55;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _148 > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 55;
            _157  .UpdateValue ( Functions.Remove ( "," , _146 , 1)  ) ; 
            __context__.SourceCodeLine = 55;
            _157  .UpdateValue ( Functions.Left ( _157 ,  (int) ( (Functions.Length( _157 ) - 1) ) )  ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 55;
            _157  .UpdateValue ( _146  ) ; 
            } 
        
        __context__.SourceCodeLine = 55;
        _156 = (ushort) ( Functions.Atoi( _157 ) ) ; 
        __context__.SourceCodeLine = 55;
        _147 [ _156] = (ushort) ( 1 ) ; 
        } 
    while (false == ( Functions.TestForTrue  ( Functions.BoolToInt ( _148 < 1 )) )); 
    __context__.SourceCodeLine = 55;
    MakeString ( _160 , " g_CiscoSwitch.readyForCommands = {0:d}", (short)_72._71) ; 
    __context__.SourceCodeLine = 55;
    _110 (  __context__ , "createRangeInterface()", _160) ; 
    __context__.SourceCodeLine = 55;
    ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
    ushort __FN_FOREND_VAL__2 = (ushort)(_84 + 1); 
    int __FN_FORSTEP_VAL__2 = (int)1; 
    for ( _156  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (_156  >= __FN_FORSTART_VAL__2) && (_156  <= __FN_FOREND_VAL__2) ) : ( (_156  <= __FN_FORSTART_VAL__2) && (_156  >= __FN_FOREND_VAL__2) ) ; _156  += (ushort)__FN_FORSTEP_VAL__2) 
        { 
        __context__.SourceCodeLine = 55;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( _147[ _156 ] ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _156 <= _84 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _156 > 0 ) )) ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _145 <= _83 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _145 > 0 ) )) ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 55;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_72._68 == 0) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _145 > 3 ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _156 > 3 ) )) ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 55;
                MakeString ( _160 , " DEMO Invalid port Number Input = {0:d} Output: {1:d}", (short)_145, (short)_156) ; 
                __context__.SourceCodeLine = 55;
                _110 (  __context__ , "createRangeInterface", _160) ; 
                __context__.SourceCodeLine = 56;
                _147 [ _156] = (ushort) ( 0 ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 56;
                OUTPUTFB [ _156]  .Value = (ushort) ( _145 ) ; 
                __context__.SourceCodeLine = 56;
                MakeString ( _160 , " g_RXPositionOnStack[{0:d}] = : {1}", (short)_156, _95 [ _156 ] ) ; 
                __context__.SourceCodeLine = 56;
                _110 (  __context__ , "createRangeInterface  ", _160) ; 
                __context__.SourceCodeLine = 56;
                _157  .UpdateValue ( _95 [ _156 ]  ) ; 
                __context__.SourceCodeLine = 56;
                _158 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 56;
                do 
                    { 
                    __context__.SourceCodeLine = 56;
                    _148 = (ushort) ( Functions.Find( "/" , _157 , 1 ) ) ; 
                    __context__.SourceCodeLine = 56;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _148 > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 56;
                        _158 = (ushort) ( (_158 + 1) ) ; 
                        __context__.SourceCodeLine = 56;
                        _159 [ _158 ]  .UpdateValue ( Functions.Remove ( "/" , _157 , 1)  ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 56;
                        _158 = (ushort) ( (_158 + 1) ) ; 
                        __context__.SourceCodeLine = 56;
                        _159 [ _158 ]  .UpdateValue ( _157  ) ; 
                        } 
                    
                    } 
                while (false == ( Functions.TestForTrue  ( Functions.BoolToInt ( _148 < 1 )) )); 
                __context__.SourceCodeLine = 56;
                MakeString ( _160 , " createRangeInterface()  - split2length {0:d}", (short)_158) ; 
                __context__.SourceCodeLine = 56;
                _110 (  __context__ , "createRangeInterface", _160) ; 
                __context__.SourceCodeLine = 56;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _158 > 1 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 56;
                    _153  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 56;
                    _151  .UpdateValue ( _159 [ _158 ]  ) ; 
                    __context__.SourceCodeLine = 56;
                    ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__3 = (ushort)(_158 - 1); 
                    int __FN_FORSTEP_VAL__3 = (int)1; 
                    for ( _149  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (_149  >= __FN_FORSTART_VAL__3) && (_149  <= __FN_FOREND_VAL__3) ) : ( (_149  <= __FN_FORSTART_VAL__3) && (_149  >= __FN_FOREND_VAL__3) ) ; _149  += (ushort)__FN_FORSTEP_VAL__3) 
                        {
                        __context__.SourceCodeLine = 56;
                        _153  .UpdateValue ( _153 + _159 [ _149 ]  ) ; 
                        __context__.SourceCodeLine = 56;
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 57;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _38( __context__ , _95[ _156 ] , (short)( 1 ) , "gi(%d+)" , ref _161 ) > Functions.ToSignedLongInteger( -( 1 ) ) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 57;
                        _151  .UpdateValue ( _111 (  __context__ , _95[ _156 ], (ushort)( _161._3[ 1 ] ), (ushort)( _161._4[ 1 ] ))  ) ; 
                        __context__.SourceCodeLine = 57;
                        _153  .UpdateValue ( "gi"  ) ; 
                        } 
                    
                    }
                
                __context__.SourceCodeLine = 57;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_3 != ""))  ) ) 
                    { 
                    __context__.SourceCodeLine = 57;
                    _155 = (ushort) ( _38( __context__ , _3 , (short)( 1 ) , _153 , ref _161 ) ) ; 
                    } 
                
                __context__.SourceCodeLine = 57;
                MakeString ( _160 , "switchLogicalRange()  - previousposOnSwitch: {0} posOnSwitch: {1} checkPrefix: {2:d} Prefix: {3} start:{4} ", _152 , _151 , (short)_155, _153 , _3 ) ; 
                __context__.SourceCodeLine = 57;
                _110 (  __context__ , "createRangeInterface", _160) ; 
                __context__.SourceCodeLine = 57;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ((Functions.Atoi( _152 ) + 1) == Functions.Atoi( _151 )) ) && Functions.TestForTrue ( Functions.BoolToInt (_155 != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (_3 == "") )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 57;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_3 == ""))  ) ) 
                        { 
                        __context__.SourceCodeLine = 57;
                        _3  .UpdateValue ( _95 [ _156 ]  ) ; 
                        __context__.SourceCodeLine = 57;
                        _4  .UpdateValue ( ""  ) ; 
                        __context__.SourceCodeLine = 57;
                        _152  .UpdateValue ( _151  ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 57;
                        _4  .UpdateValue ( _151  ) ; 
                        __context__.SourceCodeLine = 57;
                        _152  .UpdateValue ( _151  ) ; 
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 57;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_3 != ""))  ) ) 
                        { 
                        __context__.SourceCodeLine = 57;
                        _150 = (ushort) ( (_150 + 1) ) ; 
                        __context__.SourceCodeLine = 57;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_4 == ""))  ) ) 
                            {
                            __context__.SourceCodeLine = 57;
                            _154 [ _150 ]  .UpdateValue ( _3  ) ; 
                            }
                        
                        else 
                            {
                            __context__.SourceCodeLine = 57;
                            _154 [ _150 ]  .UpdateValue ( _3 + "-" + _4  ) ; 
                            }
                        
                        __context__.SourceCodeLine = 58;
                        _3  .UpdateValue ( _95 [ _156 ]  ) ; 
                        __context__.SourceCodeLine = 58;
                        _4  .UpdateValue ( ""  ) ; 
                        __context__.SourceCodeLine = 58;
                        _152  .UpdateValue ( _151  ) ; 
                        } 
                    
                    }
                
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 58;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_3 != ""))  ) ) 
                { 
                __context__.SourceCodeLine = 58;
                _150 = (ushort) ( (_150 + 1) ) ; 
                __context__.SourceCodeLine = 58;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_4 == ""))  ) ) 
                    {
                    __context__.SourceCodeLine = 58;
                    _154 [ _150 ]  .UpdateValue ( _3  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 58;
                    _154 [ _150 ]  .UpdateValue ( _3 + "-" + _4  ) ; 
                    }
                
                __context__.SourceCodeLine = 58;
                _3  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 58;
                _4  .UpdateValue ( ""  ) ; 
                } 
            
            }
        
        __context__.SourceCodeLine = 55;
        } 
    
    __context__.SourceCodeLine = 58;
    if ( Functions.TestForTrue  ( ( _141( __context__ , (ushort)( _145 ) , _154 ))  ) ) 
        { 
        } 
    
    
    }
    
private void _144 (  SplusExecutionContext __context__ ) 
    { 
    _73 _146;
    _146  = new _73( this, true );
    _146 .PopulateCustomAttributeList( false );
    
    
    __context__.SourceCodeLine = 58;
    _110 (  __context__ , "fnCiscoSwitch_Send_Y()", "") ; 
    __context__.SourceCodeLine = 58;
    _146 . _74  .UpdateValue ( "y#NULL#"  ) ; 
    __context__.SourceCodeLine = 58;
    _146 . _75  .UpdateValue ( "#"  ) ; 
    __context__.SourceCodeLine = 58;
    _146 . _76 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 58;
    _146 . _77  .UpdateValue ( "Do you want to continue ? (Y/N)"  ) ; 
    __context__.SourceCodeLine = 58;
    _146 . _78 = (ushort) ( 17 ) ; 
    __context__.SourceCodeLine = 58;
    _123 (  __context__ , _146) ; 
    
    }
    
private void _145 (  SplusExecutionContext __context__ ) 
    { 
    _73 _147;
    _147  = new _73( this, true );
    _147 .PopulateCustomAttributeList( false );
    
    
    __context__.SourceCodeLine = 58;
    _110 (  __context__ , "fnCiscoSwitch_Send_Y()", "") ; 
    __context__.SourceCodeLine = 58;
    _147 . _74  .UpdateValue ( "y#NULL#"  ) ; 
    __context__.SourceCodeLine = 58;
    _147 . _75  .UpdateValue ( "Shutting down ..."  ) ; 
    __context__.SourceCodeLine = 58;
    _147 . _76 = (ushort) ( 18 ) ; 
    __context__.SourceCodeLine = 58;
    _147 . _77  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 58;
    _147 . _78 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 58;
    _123 (  __context__ , _147) ; 
    __context__.SourceCodeLine = 58;
    _92 = (ushort) ( 1 ) ; 
    
    }
    
private void _146 (  SplusExecutionContext __context__ ) 
    { 
    CrestronString _148;
    _148  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
    
    
    __context__.SourceCodeLine = 59;
    _148  .UpdateValue ( "The module has detected an unusable configuration on the switch.\r\n\r\n but cant restore the switch to default configuration because another device is using it\r\n\r\nPlease Makesure that the crestron is the only device on the switch then restart the crestron"  ) ; 
    __context__.SourceCodeLine = 59;
    CreateWait ( "__SPLS_TMPVAR__WAITLABEL_14__" , 200 , __SPLS_TMPVAR__WAITLABEL_14___Callback ) ;
    
    }
    
public void __SPLS_TMPVAR__WAITLABEL_14___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 59;
            REBOOTINGINPROGRESS  .Value = (ushort) ( 0 ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

private void _147 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 59;
    _132 (  __context__  ) ; 
    __context__.SourceCodeLine = 59;
    _72 . _71 = (ushort) ( 0 ) ; 
    
    }
    
private void _148 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 59;
    _110 (  __context__ , "startUp", "startUp") ; 
    __context__.SourceCodeLine = 59;
    _135 (  __context__  ) ; 
    __context__.SourceCodeLine = 59;
    _72 . _71 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 59;
    _133 (  __context__ , P_SWITCHIP , (ushort)( 23 )) ; 
    
    }
    
private void _149 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 59;
    _72 . _68 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 59;
    _72 . _69 = (ushort) ( 3 ) ; 
    __context__.SourceCodeLine = 59;
    _72 . _70 = (ushort) ( 3 ) ; 
    
    }
    
private ushort _150 (  SplusExecutionContext __context__, ushort _152 ) 
    { 
    ushort _153 = 0;
    ushort _154 = 0;
    ushort _155 = 0;
    ushort _156 = 0;
    ushort _157 = 0;
    
    CrestronString _158;
    CrestronString _159;
    _158  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
    _159  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5, this );
    
    
    __context__.SourceCodeLine = 60;
    _110 (  __context__ , "fnCiscoSwitch_CreateCmdfromTables startFrom", Functions.ItoA( (int)( _152 ) )) ; 
    __context__.SourceCodeLine = 60;
    _110 (  __context__ , "fnCiscoSwitch_CreateCmdfromTables g_MessageHandlerComms.connectState", Functions.ItoA( (int)( _66._64 ) )) ; 
    __context__.SourceCodeLine = 60;
    _103 = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 60;
    _156 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 60;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _66._64 >= 5 ))  ) ) 
        { 
        __context__.SourceCodeLine = 60;
        _153 = (ushort) ( _152 ) ; 
        __context__.SourceCodeLine = 60;
        while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _153 <= 1500 ))  ) ) 
            { 
            __context__.SourceCodeLine = 60;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_102[ _153 ] != _101[ _153 ]))  ) ) 
                { 
                __context__.SourceCodeLine = 60;
                _156 = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 60;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _153 < 404 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 60;
                    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__1 = (ushort)404; 
                    int __FN_FORSTEP_VAL__1 = (int)1; 
                    for ( _154  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_154  >= __FN_FORSTART_VAL__1) && (_154  <= __FN_FOREND_VAL__1) ) : ( (_154  <= __FN_FORSTART_VAL__1) && (_154  >= __FN_FOREND_VAL__1) ) ; _154  += (ushort)__FN_FORSTEP_VAL__1) 
                        {
                        __context__.SourceCodeLine = 60;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_102[ _154 ] != _101[ _154 ]) ) && Functions.TestForTrue ( Functions.BoolToInt (_101[ _153 ] == _101[ _154 ]) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 60;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_158 != ""))  ) ) 
                                {
                                __context__.SourceCodeLine = 60;
                                _158  .UpdateValue ( _158 + "," + Functions.ItoA (  (int) ( _154 ) )  ) ; 
                                }
                            
                            else 
                                {
                                __context__.SourceCodeLine = 60;
                                _158  .UpdateValue ( Functions.ItoA (  (int) ( _154 ) )  ) ; 
                                }
                            
                            __context__.SourceCodeLine = 60;
                            _102 [ _154 ]  .UpdateValue ( _101 [ _154 ]  ) ; 
                            } 
                        
                        __context__.SourceCodeLine = 60;
                        }
                    
                    __context__.SourceCodeLine = 60;
                    _143 (  __context__ , (ushort)( Functions.Atoi( _102[ _153 ] ) ), _158) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 61;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _153 > (404 + 1) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _153 < 1380 ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 61;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _153 >= 500 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _153 < 610 ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 61;
                            _157 = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 61;
                            _155 = (ushort) ( (_153 - 500) ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 61;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _153 >= 610 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _153 < 720 ) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 61;
                                _157 = (ushort) ( 2 ) ; 
                                __context__.SourceCodeLine = 61;
                                _155 = (ushort) ( (_153 - 610) ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 61;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _153 >= 720 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _153 < 830 ) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 61;
                                    _157 = (ushort) ( 3 ) ; 
                                    __context__.SourceCodeLine = 61;
                                    _155 = (ushort) ( (_153 - 720) ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 61;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _153 >= 830 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _153 < 940 ) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 61;
                                        _157 = (ushort) ( 4 ) ; 
                                        __context__.SourceCodeLine = 61;
                                        _155 = (ushort) ( (_153 - 830) ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 61;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _153 >= 940 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _153 < 1050 ) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 61;
                                            _157 = (ushort) ( 5 ) ; 
                                            __context__.SourceCodeLine = 61;
                                            _155 = (ushort) ( (_153 - 940) ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 61;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _153 >= 1050 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _153 < 1160 ) )) ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 61;
                                                _157 = (ushort) ( 6 ) ; 
                                                __context__.SourceCodeLine = 61;
                                                _155 = (ushort) ( (_153 - 1050) ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 61;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _153 >= 1160 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _153 < 1270 ) )) ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 61;
                                                    _157 = (ushort) ( 7 ) ; 
                                                    __context__.SourceCodeLine = 61;
                                                    _155 = (ushort) ( (_153 - 1160) ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 61;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _153 >= 1270 ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 61;
                                                        _157 = (ushort) ( 8 ) ; 
                                                        __context__.SourceCodeLine = 61;
                                                        _155 = (ushort) ( (_153 - 1270) ) ; 
                                                        } 
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        __context__.SourceCodeLine = 61;
                        _110 (  __context__ , "fnCiscoSwitch_CreateCmdfromTables index", Functions.ItoA( (int)( _155 ) )) ; 
                        __context__.SourceCodeLine = 61;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _155 > 52 ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 62;
                            _155 = (ushort) ( (_155 - 52) ) ; 
                            __context__.SourceCodeLine = 62;
                            _159  .UpdateValue ( "OFF"  ) ; 
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 62;
                            _159  .UpdateValue ( "ON"  ) ; 
                            } 
                        
                        __context__.SourceCodeLine = 62;
                        _101 [ _153 ]  .UpdateValue ( "0"  ) ; 
                        __context__.SourceCodeLine = 62;
                        _142 (  __context__ , Functions.ItoA( (int)( _157 ) ), Functions.ItoA( (int)( _155 ) ), _159) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 62;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_153 == 1381))  ) ) 
                            { 
                            __context__.SourceCodeLine = 62;
                            _98 = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 62;
                            _129 (  __context__ , "show vlan", (ushort)( 10 ), "#", (ushort)( 0 ), "") ; 
                            __context__.SourceCodeLine = 62;
                            _101 [ _153 ]  .UpdateValue ( "0"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 62;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_153 == 1382))  ) ) 
                                { 
                                __context__.SourceCodeLine = 62;
                                _129 (  __context__ , "show vlan", (ushort)( 10 ), "#", (ushort)( 0 ), "") ; 
                                __context__.SourceCodeLine = 62;
                                _101 [ _153 ]  .UpdateValue ( "0"  ) ; 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 62;
                break ; 
                } 
            
            __context__.SourceCodeLine = 62;
            _153 = (ushort) ( (_153 + 1) ) ; 
            __context__.SourceCodeLine = 62;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _153 > 1500 ))  ) ) 
                { 
                __context__.SourceCodeLine = 62;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_106 == 1))  ) ) 
                    { 
                    __context__.SourceCodeLine = 62;
                    _106 = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 62;
                    _153 = (ushort) ( 1 ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 62;
                    _92 = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 62;
                    _103 = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 62;
                    _153 = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 62;
                    break ; 
                    } 
                
                } 
            
            __context__.SourceCodeLine = 60;
            } 
        
        } 
    
    __context__.SourceCodeLine = 62;
    _110 (  __context__ , "fnCiscoSwitch_CreateCmdfromTables return i", Functions.ItoA( (int)( _153 ) )) ; 
    __context__.SourceCodeLine = 62;
    return (ushort)( _153) ; 
    
    }
    
private void _151 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 62;
    _110 (  __context__ , "fnTriggerChangedStatus g_processingCmdTable", Functions.ItoA( (int)( _103 ) )) ; 
    __context__.SourceCodeLine = 62;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_103 == 1))  ) ) 
        { 
        __context__.SourceCodeLine = 62;
        _106 = (ushort) ( 1 ) ; 
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 62;
        _92 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 63;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_72._71 == 65535))  ) ) 
            {
            __context__.SourceCodeLine = 63;
            _104 = (ushort) ( _150( __context__ , (ushort)( 1 ) ) ) ; 
            }
        
        } 
    
    
    }
    
private void _152 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 63;
    _110 (  __context__ , "fnCiscoSwitch_beginOperations ", "fnCiscoSwitch_beginOperations") ; 
    __context__.SourceCodeLine = 63;
    if ( Functions.TestForTrue  ( ( _72._68)  ) ) 
        {
        __context__.SourceCodeLine = 63;
        _66 . _65  .UpdateValue ( ""  ) ; 
        }
    
    else 
        {
        __context__.SourceCodeLine = 63;
        _66 . _65  .UpdateValue ( "DEMO "  ) ; 
        }
    
    __context__.SourceCodeLine = 63;
    _72 . _71 = (ushort) ( 65535 ) ; 
    __context__.SourceCodeLine = 63;
    _92 = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 63;
    _112 (  __context__ , (ushort)( 5 )) ; 
    __context__.SourceCodeLine = 63;
    _103 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 63;
    _104 = (ushort) ( _150( __context__ , (ushort)( 1 ) ) ) ; 
    
    }
    
private void _153 (  SplusExecutionContext __context__, CrestronString _155 ) 
    { 
    ushort _156 = 0;
    ushort _157 = 0;
    ushort _158 = 0;
    ushort _159 = 0;
    ushort _160 = 0;
    ushort _161 = 0;
    ushort _162 = 0;
    ushort _2 = 0;
    ushort _163 = 0;
    ushort _164 = 0;
    ushort _165 = 0;
    ushort _166 = 0;
    ushort _167 = 0;
    
    short _168 = 0;
    short _169 = 0;
    short _170 = 0;
    short _171 = 0;
    short _172 = 0;
    short _173 = 0;
    short [,] _174;
    _174  = new short[ 9,53 ];
    
    CrestronString _175;
    CrestronString [] _176;
    CrestronString _177;
    CrestronString _178;
    CrestronString _179;
    CrestronString [] _180;
    _175  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64000, this );
    _177  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
    _178  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    _179  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
    _176  = new CrestronString[ 101 ];
    for( uint i = 0; i < 101; i++ )
        _176 [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    _180  = new CrestronString[ 501 ];
    for( uint i = 0; i < 501; i++ )
        _180 [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
    
    _0 _181;
    _181  = new _0( this, true );
    _181 .PopulateCustomAttributeList( false );
    
    
    __context__.SourceCodeLine = 63;
    _172 = (short) ( 0 ) ; 
    __context__.SourceCodeLine = 63;
    _173 = (short) ( 0 ) ; 
    __context__.SourceCodeLine = 63;
    short __FN_FORSTART_VAL__1 = (short) ( 0 ) ;
    short __FN_FOREND_VAL__1 = (short)8; 
    int __FN_FORSTEP_VAL__1 = (int)1; 
    for ( _170  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_170  >= __FN_FORSTART_VAL__1) && (_170  <= __FN_FOREND_VAL__1) ) : ( (_170  <= __FN_FORSTART_VAL__1) && (_170  >= __FN_FOREND_VAL__1) ) ; _170  += (short)__FN_FORSTEP_VAL__1) 
        { 
        __context__.SourceCodeLine = 63;
        ushort __FN_FORSTART_VAL__2 = (ushort) ( 0 ) ;
        ushort __FN_FOREND_VAL__2 = (ushort)52; 
        int __FN_FORSTEP_VAL__2 = (int)1; 
        for ( _167  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (_167  >= __FN_FORSTART_VAL__2) && (_167  <= __FN_FOREND_VAL__2) ) : ( (_167  <= __FN_FORSTART_VAL__2) && (_167  >= __FN_FOREND_VAL__2) ) ; _167  += (ushort)__FN_FORSTEP_VAL__2) 
            { 
            __context__.SourceCodeLine = 64;
            _174 [ _170 , _167] = (short) ( 0 ) ; 
            __context__.SourceCodeLine = 63;
            } 
        
        __context__.SourceCodeLine = 63;
        } 
    
    __context__.SourceCodeLine = 64;
    _170 = (short) ( 1 ) ; 
    __context__.SourceCodeLine = 64;
    _165 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 64;
    _159 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 64;
    while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "\r\n" , _155 , 1 ) > 0 ))  ) ) 
        { 
        __context__.SourceCodeLine = 64;
        _177  .UpdateValue ( Functions.Remove ( "\r\n" , _155 , 1)  ) ; 
        __context__.SourceCodeLine = 64;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_177 != ""))  ) ) 
            { 
            __context__.SourceCodeLine = 64;
            _168 = (short) ( Functions.ToSignedInteger( -( 1 ) ) ) ; 
            __context__.SourceCodeLine = 64;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _168 < 2 ))  ) ) 
                { 
                __context__.SourceCodeLine = 64;
                while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( _177 ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 64;
                    _157 = (ushort) ( Byte( _177 , (int)( 1 ) ) ) ; 
                    __context__.SourceCodeLine = 64;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_157 == 32))  ) ) 
                        {
                        __context__.SourceCodeLine = 64;
                        _179  .UpdateValue ( Functions.Remove ( " " , _177 , 1)  ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 64;
                        break ; 
                        }
                    
                    __context__.SourceCodeLine = 64;
                    } 
                
                __context__.SourceCodeLine = 64;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Length( _177 ) == 0))  ) ) 
                    {
                    __context__.SourceCodeLine = 64;
                    break ; 
                    }
                
                __context__.SourceCodeLine = 64;
                _168 = (short) ( (_168 + 1) ) ; 
                __context__.SourceCodeLine = 64;
                _158 = (ushort) ( Functions.Find( " " , _177 , 1 ) ) ; 
                __context__.SourceCodeLine = 64;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_158 == 0))  ) ) 
                    {
                    __context__.SourceCodeLine = 64;
                    _158 = (ushort) ( Functions.Length( _177 ) ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 64;
                    _158 = (ushort) ( (_158 - 1) ) ; 
                    }
                
                __context__.SourceCodeLine = 64;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_158 == 0))  ) ) 
                    {
                    __context__.SourceCodeLine = 64;
                    _176 [ _168 ]  .UpdateValue ( ""  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 64;
                    _176 [ _168 ]  .UpdateValue ( Functions.Remove ( _158, _177 )  ) ; 
                    }
                
                __context__.SourceCodeLine = 64;
                } 
            
            __context__.SourceCodeLine = 64;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_168 == 2))  ) ) 
                { 
                __context__.SourceCodeLine = 64;
                _171 = (short) ( Functions.Atoi( _176[ 0 ] ) ) ; 
                __context__.SourceCodeLine = 64;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_171 == 2) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Functions.Left( _176[ 1 ] , (int)( 1 ) ) == "2") ) || Functions.TestForTrue ( Functions.BoolToInt (_176[ 1 ] == "CONTROL") )) ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 64;
                    _159 = (ushort) ( (_159 | 1) ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 65;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_171 == 10) ) && Functions.TestForTrue ( Functions.BoolToInt (Functions.Left( _176[ 1 ] , (int)( 4 ) ) == "JAP_") )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 65;
                        _2 = (ushort) ( _38( __context__ , _176[ 1 ] , (short)( 1 ) , "JAP_(%d+)x(%d+)" , ref _181 ) ) ; 
                        __context__.SourceCodeLine = 65;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _2 <= 1 ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 65;
                            _110 (  __context__ , "CiscoSwitch_processShowVLan", "Failed to find JAP inter-VLAN routing VLAN") ; 
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 65;
                            _163 = (ushort) ( Functions.Atoi( _111( __context__ , _176[ 1 ] , (ushort)( _181._3[ 1 ] ) , (ushort)( _181._4[ 1 ] ) ) ) ) ; 
                            __context__.SourceCodeLine = 65;
                            _164 = (ushort) ( Functions.Atoi( _111( __context__ , _176[ 1 ] , (ushort)( _181._3[ 2 ] ) , (ushort)( _181._4[ 2 ] ) ) ) ) ; 
                            __context__.SourceCodeLine = 65;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _163 > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _164 > 0 ) )) ))  ) ) 
                                {
                                __context__.SourceCodeLine = 65;
                                _159 = (ushort) ( (_159 | 2) ) ; 
                                }
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 65;
                                _110 (  __context__ , "CiscoSwitch_processShowVLan", "Invalid input/output count in JAP routing VLAN") ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 65;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _171 >= 11 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _171 <= 404 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Functions.Left( _176[ 1 ] , (int)( 3 ) ) == "TX_") ) || Functions.TestForTrue ( Functions.BoolToInt (Functions.Left( _176[ 1 ] , (int)( 12 ) ) == "TRANSMITTER_") )) ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 66;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Left( _176[ 1 ] , (int)( 12 ) ) == "TRANSMITTER_"))  ) ) 
                                { 
                                __context__.SourceCodeLine = 66;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _38( __context__ , _176[ 2 ] , (short)( 1 ) , "gi(%d*)/?(%d*)" , ref _181 ) <= 1 ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 66;
                                    _110 (  __context__ , "fnCiscoSwitch_processVlanPortStatus", "problem with TRANSMITTER_ col2") ; 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 66;
                                    _160 = (ushort) ( Functions.Atoi( _111( __context__ , _176[ 2 ] , (ushort)( _181._3[ 1 ] ) , (ushort)( _181._4[ 1 ] ) ) ) ) ; 
                                    __context__.SourceCodeLine = 66;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _160 <= 0 ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 66;
                                        _110 (  __context__ , "CiscoSwitch_processShowVLan", "Invalid port name in TX VLAN name") ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 66;
                                        _174 [ 0 , _160] = (short) ( Functions.ToSignedInteger( -( _171 ) ) ) ; 
                                        __context__.SourceCodeLine = 66;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _160 > _174[ 0 , 0 ] ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 66;
                                            _174 [ 0 , 0] = (short) ( _160 ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 66;
                                        _172 = (short) ( (_172 + 1) ) ; 
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 66;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _38( __context__ , _176[ 1 ] , (short)( 1 ) , "TX_%d+_gi(%d*)/?(%d*)/?(%d*)" , ref _181 ) <= 1 ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 66;
                                    _110 (  __context__ , "CiscoSwitch_processShowVLan", "TX VLAN name with unexpected format") ; 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 67;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _181._4[ 3 ] > _181._3[ 3 ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 67;
                                        _162 = (ushort) ( (Functions.Atoi( _111( __context__ , _176[ 1 ] , (ushort)( _181._3[ 1 ] ) , (ushort)( _181._4[ 1 ] ) ) ) - 1) ) ; 
                                        __context__.SourceCodeLine = 67;
                                        _160 = (ushort) ( Functions.Atoi( _111( __context__ , _176[ 1 ] , (ushort)( _181._3[ 3 ] ) , (ushort)( _181._4[ 3 ] ) ) ) ) ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 67;
                                        _162 = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 67;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _181._4[ 2 ] > _181._3[ 2 ] ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 67;
                                            _160 = (ushort) ( Functions.Atoi( _111( __context__ , _176[ 1 ] , (ushort)( _181._3[ 2 ] ) , (ushort)( _181._4[ 2 ] ) ) ) ) ; 
                                            }
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 67;
                                            _160 = (ushort) ( Functions.Atoi( _111( __context__ , _176[ 1 ] , (ushort)( _181._3[ 1 ] ) , (ushort)( _181._4[ 1 ] ) ) ) ) ; 
                                            }
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 67;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _160 < 1 ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _160 > 52 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _162 < 0 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _162 >= 8 ) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 67;
                                        _110 (  __context__ , "CiscoSwitch_processShowVLan", "TX VLAN name with invalid port name") ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 67;
                                        _174 [ _162 , _160] = (short) ( Functions.ToSignedInteger( -( _171 ) ) ) ; 
                                        __context__.SourceCodeLine = 67;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _160 > _174[ _162 , 0 ] ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 67;
                                            _174 [ _162 , 0] = (short) ( _160 ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 67;
                                        _172 = (short) ( (_172 + 1) ) ; 
                                        } 
                                    
                                    } 
                                
                                }
                            
                            __context__.SourceCodeLine = 67;
                            _173 = (short) ( (_173 + _139( __context__ , _176[ 2 ] , (short)( _171 ) , (short[,])( _174 ) , ref _180 )) ) ; 
                            __context__.SourceCodeLine = 68;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Right( _176[ 2 ] , (int)( 1 ) ) == ","))  ) ) 
                                {
                                __context__.SourceCodeLine = 68;
                                _165 = (ushort) ( _171 ) ; 
                                }
                            
                            else 
                                {
                                __context__.SourceCodeLine = 68;
                                _165 = (ushort) ( 0 ) ; 
                                }
                            
                            __context__.SourceCodeLine = 68;
                            _159 = (ushort) ( (_159 | 4) ) ; 
                            } 
                        
                        }
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 68;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _168 > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _165 > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (Functions.Left( _176[ 0 ] , (int)( 2 ) ) == "gi") )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 68;
                    _173 = (short) ( (_173 + _139( __context__ , _176[ 0 ] , (short)( _165 ) , (short[,])( _174 ) , ref _180 )) ) ; 
                    __context__.SourceCodeLine = 68;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Right( _176[ 0 ] , (int)( 1 ) ) != ","))  ) ) 
                        {
                        __context__.SourceCodeLine = 68;
                        _165 = (ushort) ( 0 ) ; 
                        }
                    
                    } 
                
                }
            
            } 
        
        __context__.SourceCodeLine = 68;
        _110 (  __context__ , "CiscoSwitch_processShowVLan iNumberOfInputs = ", Functions.ItoA( (int)( _172 ) )) ; 
        __context__.SourceCodeLine = 68;
        _110 (  __context__ , "CiscoSwitch_processShowVLan iNumberOfOutputs = ", Functions.ItoA( (int)( _173 ) )) ; 
        __context__.SourceCodeLine = 68;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_159 != 7) ) || Functions.TestForTrue ( Functions.BoolToInt (_172 != _163) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (_173 != _164) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 68;
            _110 (  __context__ , "CiscoSwitch_processShowVLan", "Invalid configuration (missing VLANs or unexpected number or inputs or outputs)") ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 68;
            _161 = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 68;
            _83 = (ushort) ( _172 ) ; 
            __context__.SourceCodeLine = 68;
            _84 = (ushort) ( _173 ) ; 
            __context__.SourceCodeLine = 69;
            ushort __FN_FORSTART_VAL__3 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__3 = (ushort)8; 
            int __FN_FORSTEP_VAL__3 = (int)1; 
            for ( _166  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (_166  >= __FN_FORSTART_VAL__3) && (_166  <= __FN_FOREND_VAL__3) ) : ( (_166  <= __FN_FORSTART_VAL__3) && (_166  >= __FN_FOREND_VAL__3) ) ; _166  += (ushort)__FN_FORSTEP_VAL__3) 
                { 
                __context__.SourceCodeLine = 69;
                ushort __FN_FORSTART_VAL__4 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__4 = (ushort)_174[ _166 , 0 ]; 
                int __FN_FORSTEP_VAL__4 = (int)1; 
                for ( _167  = __FN_FORSTART_VAL__4; (__FN_FORSTEP_VAL__4 > 0)  ? ( (_167  >= __FN_FORSTART_VAL__4) && (_167  <= __FN_FOREND_VAL__4) ) : ( (_167  <= __FN_FORSTART_VAL__4) && (_167  >= __FN_FOREND_VAL__4) ) ; _167  += (ushort)__FN_FORSTEP_VAL__4) 
                    { 
                    __context__.SourceCodeLine = 69;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _174[ _166 , _167 ] > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 69;
                        _161 = (ushort) ( (_161 + 1) ) ; 
                        __context__.SourceCodeLine = 69;
                        OUTPUTFB [ _161]  .Value = (ushort) ( (_174[ _166 , _167 ] - 10) ) ; 
                        __context__.SourceCodeLine = 69;
                        _102 [ _161 ]  .UpdateValue ( Functions.ItoA (  (int) ( (_174[ _166 , _167 ] - 10) ) )  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 69;
                    } 
                
                __context__.SourceCodeLine = 69;
                } 
            
            __context__.SourceCodeLine = 69;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_98 == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 69;
                _98 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 69;
                _138 (  __context__  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 69;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_107 == 1))  ) ) 
                    { 
                    __context__.SourceCodeLine = 69;
                    _107 = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 69;
                    _152 (  __context__  ) ; 
                    } 
                
                }
            
            } 
        
        __context__.SourceCodeLine = 64;
        } 
    
    __context__.SourceCodeLine = 69;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_159 != 7) ) || Functions.TestForTrue ( Functions.BoolToInt (_172 != _163) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (_173 != _164) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (_107 == 1) )) ))  ) ) 
        { 
        __context__.SourceCodeLine = 69;
        _97 = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 69;
        _105 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 69;
        REBOOTINGINPROGRESS  .Value = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 69;
        _178  .UpdateValue ( "The module has detected an unusable configuration on the switch.\r\n\r\nRestoring switch to default configuration.\r\n\r\nThis may take several minutes."  ) ; 
        __context__.SourceCodeLine = 69;
        ERRORMSG__DOLLAR__  .UpdateValue ( _178  ) ; 
        __context__.SourceCodeLine = 70;
        _129 (  __context__ , "reload", (ushort)( 15 ), "Are you sure you want to continue ? (Y/N)", (ushort)( 19 ), "Try Again Later") ; 
        } 
    
    
    }
    
private void _154 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 70;
    _112 (  __context__ , (ushort)( 6 )) ; 
    __context__.SourceCodeLine = 70;
    _110 (  __context__ , "fnCiscoSwitch_loggedOn", "fnCiscoSwitch_loggedOn") ; 
    __context__.SourceCodeLine = 70;
    _129 (  __context__ , "terminal datadump", (ushort)( 0 ), "#", (ushort)( 0 ), "") ; 
    __context__.SourceCodeLine = 70;
    _129 (  __context__ , "terminal width 512", (ushort)( 0 ), "#", (ushort)( 0 ), "") ; 
    __context__.SourceCodeLine = 70;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_105 == 0))  ) ) 
        {
        __context__.SourceCodeLine = 70;
        _129 (  __context__ , "show vlan", (ushort)( 12 ), "#", (ushort)( 0 ), "") ; 
        }
    
    else 
        { 
        __context__.SourceCodeLine = 70;
        _107 = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 70;
        _129 (  __context__ , "show vlan", (ushort)( 10 ), "#", (ushort)( 0 ), "") ; 
        } 
    
    
    }
    
private void _155 (  SplusExecutionContext __context__ ) 
    { 
    ushort _157 = 0;
    ushort _158 = 0;
    
    CrestronString _159;
    CrestronString _160;
    CrestronString _161;
    CrestronString _162;
    _159  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    _160  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    _161  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5000, this );
    _162  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5000, this );
    
    CrestronString _163;
    CrestronString _164;
    _163  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
    _164  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    
    ushort _165 = 0;
    
    ushort _166 = 0;
    
    ushort [] _167;
    _167  = new ushort[ 17 ];
    
    CrestronString [] _168;
    _168  = new CrestronString[ 17 ];
    for( uint i = 0; i < 17; i++ )
        _168 [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    
    
    __context__.SourceCodeLine = 70;
    _161  .UpdateValue ( _94  ) ; 
    __context__.SourceCodeLine = 71;
    _110 (  __context__ , "processShowVersionMessage rxDataMac ", _161) ; 
    __context__.SourceCodeLine = 71;
    while ( Functions.TestForTrue  ( ( Functions.Find( "License Activation is :" , _161 ))  ) ) 
        { 
        __context__.SourceCodeLine = 71;
        _162  .UpdateValue ( Functions.Remove ( "License Activation is :" , _161 )  ) ; 
        __context__.SourceCodeLine = 71;
        } 
    
    __context__.SourceCodeLine = 71;
    _110 (  __context__ , "processShowVersionMessage rxDataMac1 ", _161) ; 
    __context__.SourceCodeLine = 71;
    _161  .UpdateValue ( Functions.Remove ( "\u000D" , _161 )  ) ; 
    __context__.SourceCodeLine = 71;
    _110 (  __context__ , "processShowVersionMessage rxDataMac2 ", _161) ; 
    __context__.SourceCodeLine = 71;
    _163  .UpdateValue ( Functions.Left ( _161 ,  (int) ( (Functions.Length( _161 ) - 1) ) )  ) ; 
    __context__.SourceCodeLine = 71;
    CRESTRONID__DOLLAR__  .UpdateValue ( _161  ) ; 
    __context__.SourceCodeLine = 71;
    _166 = (ushort) ( 16 ) ; 
    __context__.SourceCodeLine = 71;
    _165 = (ushort) ( _54( __context__ , P_SWITCHLICENCEKEY  , (ushort)( _166 ) , ref _167 , ref _168 ) ) ; 
    __context__.SourceCodeLine = 71;
    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
    ushort __FN_FOREND_VAL__1 = (ushort)_165; 
    int __FN_FORSTEP_VAL__1 = (int)1; 
    for ( _158  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_158  >= __FN_FORSTART_VAL__1) && (_158  <= __FN_FOREND_VAL__1) ) : ( (_158  <= __FN_FORSTART_VAL__1) && (_158  >= __FN_FOREND_VAL__1) ) ; _158  += (ushort)__FN_FORSTEP_VAL__1) 
        { 
        __context__.SourceCodeLine = 71;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_167[ _158 ] == 6))  ) ) 
            { 
            __context__.SourceCodeLine = 71;
            _159  .UpdateValue ( _55 (  __context__ , _168[ _158 ])  ) ; 
            __context__.SourceCodeLine = 71;
            _160  .UpdateValue ( _55 (  __context__ , _163)  ) ; 
            __context__.SourceCodeLine = 71;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_160 == _159))  ) ) 
                {
                __context__.SourceCodeLine = 71;
                _72 . _68 = (ushort) ( 65535 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 71;
                _72 . _68 = (ushort) ( 0 ) ; 
                }
            
            __context__.SourceCodeLine = 71;
            break ; 
            } 
        
        __context__.SourceCodeLine = 71;
        } 
    
    __context__.SourceCodeLine = 71;
    if ( Functions.TestForTrue  ( ( _72._68)  ) ) 
        { 
        __context__.SourceCodeLine = 71;
        _72 . _68 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 72;
        ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__2 = (ushort)_165; 
        int __FN_FORSTEP_VAL__2 = (int)1; 
        for ( _158  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (_158  >= __FN_FORSTART_VAL__2) && (_158  <= __FN_FOREND_VAL__2) ) : ( (_158  <= __FN_FORSTART_VAL__2) && (_158  >= __FN_FOREND_VAL__2) ) ; _158  += (ushort)__FN_FORSTEP_VAL__2) 
            { 
            __context__.SourceCodeLine = 72;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_167[ _158 ] == 4))  ) ) 
                { 
                __context__.SourceCodeLine = 72;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_168[ _158 ] == "JADDriver"))  ) ) 
                    {
                    __context__.SourceCodeLine = 72;
                    _72 . _68 = (ushort) ( 65535 ) ; 
                    }
                
                __context__.SourceCodeLine = 72;
                break ; 
                } 
            
            __context__.SourceCodeLine = 72;
            } 
        
        } 
    
    __context__.SourceCodeLine = 72;
    if ( Functions.TestForTrue  ( ( _72._68)  ) ) 
        { 
        __context__.SourceCodeLine = 72;
        _91  .UpdateValue ( ""  ) ; 
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 72;
        _91  .UpdateValue ( "Demo"  ) ; 
        } 
    
    __context__.SourceCodeLine = 72;
    MakeString ( _164 , "numberOfRX={0:d} g_numberOfTX= {1:d}", (short)_84, (short)_83) ; 
    __context__.SourceCodeLine = 72;
    _110 (  __context__ , "processShowVersionMessage", _164) ; 
    __context__.SourceCodeLine = 72;
    _72 . _69 = (ushort) ( _83 ) ; 
    __context__.SourceCodeLine = 72;
    _72 . _70 = (ushort) ( _84 ) ; 
    __context__.SourceCodeLine = 72;
    _90 = (ushort) ( (_83 + _84) ) ; 
    __context__.SourceCodeLine = 72;
    NUMOFINPUTS  .Value = (ushort) ( _72._69 ) ; 
    __context__.SourceCodeLine = 72;
    NUMOFOUTPUTS  .Value = (ushort) ( _72._70 ) ; 
    __context__.SourceCodeLine = 72;
    _152 (  __context__  ) ; 
    
    }
    
private void _156 (  SplusExecutionContext __context__, CrestronString _158 ) 
    { 
    ushort _159 = 0;
    ushort _160 = 0;
    ushort _161 = 0;
    ushort _162 = 0;
    ushort _163 = 0;
    ushort _164 = 0;
    ushort _165 = 0;
    ushort [] _166;
    ushort _167 = 0;
    ushort _168 = 0;
    ushort _169 = 0;
    ushort _170 = 0;
    ushort _2 = 0;
    ushort _171 = 0;
    ushort _172 = 0;
    ushort IPROCESSINGVLANID = 0;
    ushort I = 0;
    ushort J = 0;
    _166  = new ushort[ 405 ];
    
    short _173 = 0;
    short _174 = 0;
    short _175 = 0;
    short _176 = 0;
    short _177 = 0;
    short [,] _178;
    short _179 = 0;
    _178  = new short[ 9,53 ];
    
    CrestronString _180;
    CrestronString [] _181;
    CrestronString _182;
    CrestronString _183;
    CrestronString _184;
    CrestronString _185;
    CrestronString _186;
    CrestronString [] _187;
    _180  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64000, this );
    _182  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
    _183  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    _184  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    _185  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
    _186  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
    _181  = new CrestronString[ 101 ];
    for( uint i = 0; i < 101; i++ )
        _181 [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    _187  = new CrestronString[ 501 ];
    for( uint i = 0; i < 501; i++ )
        _187 [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
    
    _0 _188;
    _188  = new _0( this, true );
    _188 .PopulateCustomAttributeList( false );
    
    
    __context__.SourceCodeLine = 73;
    _176 = (short) ( 0 ) ; 
    __context__.SourceCodeLine = 73;
    _177 = (short) ( 0 ) ; 
    __context__.SourceCodeLine = 73;
    short __FN_FORSTART_VAL__1 = (short) ( 0 ) ;
    short __FN_FOREND_VAL__1 = (short)8; 
    int __FN_FORSTEP_VAL__1 = (int)1; 
    for ( _175  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_175  >= __FN_FORSTART_VAL__1) && (_175  <= __FN_FOREND_VAL__1) ) : ( (_175  <= __FN_FORSTART_VAL__1) && (_175  >= __FN_FOREND_VAL__1) ) ; _175  += (short)__FN_FORSTEP_VAL__1) 
        { 
        __context__.SourceCodeLine = 73;
        ushort __FN_FORSTART_VAL__2 = (ushort) ( 0 ) ;
        ushort __FN_FOREND_VAL__2 = (ushort)52; 
        int __FN_FORSTEP_VAL__2 = (int)1; 
        for ( J  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (J  >= __FN_FORSTART_VAL__2) && (J  <= __FN_FOREND_VAL__2) ) : ( (J  <= __FN_FORSTART_VAL__2) && (J  >= __FN_FOREND_VAL__2) ) ; J  += (ushort)__FN_FORSTEP_VAL__2) 
            { 
            __context__.SourceCodeLine = 73;
            _178 [ _175 , J] = (short) ( 0 ) ; 
            __context__.SourceCodeLine = 73;
            } 
        
        __context__.SourceCodeLine = 73;
        } 
    
    __context__.SourceCodeLine = 73;
    _175 = (short) ( 1 ) ; 
    __context__.SourceCodeLine = 73;
    IPROCESSINGVLANID = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 73;
    _164 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 73;
    _110 (  __context__ , "CiscoSwitch_processShowVLan - vlanMessage", _158) ; 
    __context__.SourceCodeLine = 73;
    while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "\r\n" , _158 , 1 ) > 0 ))  ) ) 
        { 
        __context__.SourceCodeLine = 73;
        _182  .UpdateValue ( Functions.Remove ( "\r\n" , _158 , 1)  ) ; 
        __context__.SourceCodeLine = 73;
        _159 = (ushort) ( 500 ) ; 
        __context__.SourceCodeLine = 73;
        _110 (  __context__ , "CiscoSwitch_processShowVLan row ", _182) ; 
        __context__.SourceCodeLine = 73;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_182 != ""))  ) ) 
            { 
            __context__.SourceCodeLine = 73;
            _173 = (short) ( Functions.ToSignedInteger( -( 1 ) ) ) ; 
            __context__.SourceCodeLine = 73;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _173 < 2 ))  ) ) 
                { 
                __context__.SourceCodeLine = 73;
                while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( _182 ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 73;
                    _162 = (ushort) ( Byte( _182 , (int)( 1 ) ) ) ; 
                    __context__.SourceCodeLine = 73;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_162 == 32))  ) ) 
                        {
                        __context__.SourceCodeLine = 73;
                        _186  .UpdateValue ( Functions.Remove ( " " , _182 , 1)  ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 73;
                        break ; 
                        }
                    
                    __context__.SourceCodeLine = 73;
                    } 
                
                __context__.SourceCodeLine = 73;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Length( _182 ) == 0))  ) ) 
                    {
                    __context__.SourceCodeLine = 73;
                    break ; 
                    }
                
                __context__.SourceCodeLine = 74;
                _173 = (short) ( (_173 + 1) ) ; 
                __context__.SourceCodeLine = 74;
                _163 = (ushort) ( Functions.Find( " " , _182 , 1 ) ) ; 
                __context__.SourceCodeLine = 74;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_163 == 0))  ) ) 
                    {
                    __context__.SourceCodeLine = 74;
                    _163 = (ushort) ( Functions.Length( _182 ) ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 74;
                    _163 = (ushort) ( (_163 - 1) ) ; 
                    }
                
                __context__.SourceCodeLine = 74;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_163 == 0))  ) ) 
                    {
                    __context__.SourceCodeLine = 74;
                    _181 [ _173 ]  .UpdateValue ( ""  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 74;
                    _181 [ _173 ]  .UpdateValue ( Functions.Remove ( _163, _182 )  ) ; 
                    }
                
                __context__.SourceCodeLine = 73;
                } 
            
            __context__.SourceCodeLine = 74;
            _110 (  __context__ , "CiscoSwitch_processShowVLan icol ", Functions.ItoA( (int)( _173 ) )) ; 
            __context__.SourceCodeLine = 74;
            _110 (  __context__ , "CiscoSwitch_processShowVLan col[0] ", _181[ 0 ]) ; 
            __context__.SourceCodeLine = 74;
            _110 (  __context__ , "CiscoSwitch_processShowVLan col[1] ", _181[ 1 ]) ; 
            __context__.SourceCodeLine = 74;
            _110 (  __context__ , "CiscoSwitch_processShowVLan col[2] ", _181[ 2 ]) ; 
            __context__.SourceCodeLine = 74;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_173 == 2))  ) ) 
                { 
                __context__.SourceCodeLine = 74;
                _179 = (short) ( Functions.Atoi( _181[ 0 ] ) ) ; 
                __context__.SourceCodeLine = 74;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_179 == 2) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Functions.Left( _181[ 1 ] , (int)( 1 ) ) == "2") ) || Functions.TestForTrue ( Functions.BoolToInt (_181[ 1 ] == "CONTROL") )) ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 74;
                    _164 = (ushort) ( (_164 | 1) ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 74;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_179 == 10) ) && Functions.TestForTrue ( Functions.BoolToInt (Functions.Left( _181[ 1 ] , (int)( 4 ) ) == "JAP_") )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 75;
                        _2 = (ushort) ( _38( __context__ , _181[ 1 ] , (short)( 1 ) , "JAP_(%d+)x(%d+)" , ref _188 ) ) ; 
                        __context__.SourceCodeLine = 75;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _2 <= 1 ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 75;
                            _110 (  __context__ , "CiscoSwitch_processShowVLan", "Failed to find JAP inter-VLAN routing VLAN") ; 
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 75;
                            _171 = (ushort) ( Functions.Atoi( _111( __context__ , _181[ 1 ] , (ushort)( _188._3[ 1 ] ) , (ushort)( _188._4[ 1 ] ) ) ) ) ; 
                            __context__.SourceCodeLine = 75;
                            _172 = (ushort) ( Functions.Atoi( _111( __context__ , _181[ 1 ] , (ushort)( _188._3[ 2 ] ) , (ushort)( _188._4[ 2 ] ) ) ) ) ; 
                            __context__.SourceCodeLine = 75;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _171 > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _172 > 0 ) )) ))  ) ) 
                                {
                                __context__.SourceCodeLine = 75;
                                _164 = (ushort) ( (_164 | 2) ) ; 
                                }
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 75;
                                _110 (  __context__ , "CiscoSwitch_processShowVLan", "Invalid input/output count in JAP routing VLAN") ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 75;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _179 >= 11 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _179 <= 404 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Functions.Left( _181[ 1 ] , (int)( 3 ) ) == "TX_") ) || Functions.TestForTrue ( Functions.BoolToInt (Functions.Left( _181[ 1 ] , (int)( 12 ) ) == "TRANSMITTER_") )) ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 75;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Left( _181[ 1 ] , (int)( 12 ) ) == "TRANSMITTER_"))  ) ) 
                                { 
                                __context__.SourceCodeLine = 76;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _38( __context__ , _181[ 2 ] , (short)( 1 ) , "gi(%d*)/?(%d*)" , ref _188 ) <= 2 ))  ) ) 
                                    { 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 76;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _188._4[ 2 ] > _188._3[ 2 ] ))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 76;
                                        _165 = (ushort) ( Functions.Atoi( _111( __context__ , _181[ 2 ] , (ushort)( _188._3[ 2 ] ) , (ushort)( _188._4[ 2 ] ) ) ) ) ; 
                                        }
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 76;
                                        _165 = (ushort) ( Functions.Atoi( _111( __context__ , _181[ 2 ] , (ushort)( _188._3[ 1 ] ) , (ushort)( _188._4[ 1 ] ) ) ) ) ; 
                                        }
                                    
                                    __context__.SourceCodeLine = 76;
                                    _110 (  __context__ , "CiscoSwitch_processShowVLan col[2]", _181[ 2 ]) ; 
                                    __context__.SourceCodeLine = 76;
                                    _110 (  __context__ , "CiscoSwitch_processShowVLan results.start[1]", Functions.ItoA( (int)( _188._3[ 1 ] ) )) ; 
                                    __context__.SourceCodeLine = 76;
                                    _110 (  __context__ , "CiscoSwitch_processShowVLan results.end[1]", Functions.ItoA( (int)( _188._4[ 1 ] ) )) ; 
                                    __context__.SourceCodeLine = 76;
                                    _110 (  __context__ , "CiscoSwitch_processShowVLan results.start[2]", Functions.ItoA( (int)( _188._3[ 2 ] ) )) ; 
                                    __context__.SourceCodeLine = 76;
                                    _110 (  __context__ , "CiscoSwitch_processShowVLan results.end[2]", Functions.ItoA( (int)( _188._4[ 2 ] ) )) ; 
                                    __context__.SourceCodeLine = 77;
                                    _110 (  __context__ , "CiscoSwitch_processShowVLan iPort", Functions.ItoA( (int)( _165 ) )) ; 
                                    __context__.SourceCodeLine = 77;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _165 <= 0 ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 77;
                                        _110 (  __context__ , "CiscoSwitch_processShowVLan", "Invalid port name in TX VLAN name") ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 77;
                                        _178 [ 0 , _165] = (short) ( Functions.ToSignedInteger( -( _179 ) ) ) ; 
                                        __context__.SourceCodeLine = 77;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _165 > _178[ 0 , 0 ] ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 77;
                                            _178 [ 0 , 0] = (short) ( _165 ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 77;
                                        _176 = (short) ( (_176 + 1) ) ; 
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 77;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _38( __context__ , _181[ 1 ] , (short)( 1 ) , "TX_%d+_gi(%d*)/?(%d*)/?(%d*)" , ref _188 ) <= 1 ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 77;
                                    _110 (  __context__ , "CiscoSwitch_processShowVLan", "TX VLAN name with unexpected format") ; 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 77;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _188._4[ 3 ] > _188._3[ 3 ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 77;
                                        _170 = (ushort) ( (Functions.Atoi( _111( __context__ , _181[ 1 ] , (ushort)( _188._3[ 1 ] ) , (ushort)( _188._4[ 1 ] ) ) ) - 1) ) ; 
                                        __context__.SourceCodeLine = 77;
                                        _165 = (ushort) ( Functions.Atoi( _111( __context__ , _181[ 1 ] , (ushort)( _188._3[ 3 ] ) , (ushort)( _188._4[ 3 ] ) ) ) ) ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 77;
                                        _170 = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 78;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _188._4[ 2 ] > _188._3[ 2 ] ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 78;
                                            _165 = (ushort) ( Functions.Atoi( _111( __context__ , _181[ 1 ] , (ushort)( _188._3[ 2 ] ) , (ushort)( _188._4[ 2 ] ) ) ) ) ; 
                                            }
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 78;
                                            _165 = (ushort) ( Functions.Atoi( _111( __context__ , _181[ 1 ] , (ushort)( _188._3[ 1 ] ) , (ushort)( _188._4[ 1 ] ) ) ) ) ; 
                                            }
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 78;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _165 < 1 ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _165 > 52 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _170 < 0 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _170 >= 8 ) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 78;
                                        _110 (  __context__ , "CiscoSwitch_processShowVLan", "TX VLAN name with invalid port name") ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 78;
                                        _178 [ _170 , _165] = (short) ( Functions.ToSignedInteger( -( _179 ) ) ) ; 
                                        __context__.SourceCodeLine = 78;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _165 > _178[ _170 , 0 ] ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 78;
                                            _178 [ _170 , 0] = (short) ( _165 ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 78;
                                        _176 = (short) ( (_176 + 1) ) ; 
                                        } 
                                    
                                    } 
                                
                                }
                            
                            __context__.SourceCodeLine = 78;
                            _177 = (short) ( (_177 + _139( __context__ , _181[ 2 ] , (short)( _179 ) , (short[,])( _178 ) , ref _187 )) ) ; 
                            __context__.SourceCodeLine = 78;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Right( _181[ 2 ] , (int)( 1 ) ) == ","))  ) ) 
                                {
                                __context__.SourceCodeLine = 78;
                                IPROCESSINGVLANID = (ushort) ( _179 ) ; 
                                }
                            
                            else 
                                {
                                __context__.SourceCodeLine = 78;
                                IPROCESSINGVLANID = (ushort) ( 0 ) ; 
                                }
                            
                            __context__.SourceCodeLine = 78;
                            _164 = (ushort) ( (_164 | 4) ) ; 
                            } 
                        
                        }
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 78;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _173 > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( IPROCESSINGVLANID > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (Functions.Left( _181[ 0 ] , (int)( 2 ) ) == "gi") )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 79;
                    _177 = (short) ( (_177 + _139( __context__ , _181[ 0 ] , (short)( IPROCESSINGVLANID ) , (short[,])( _178 ) , ref _187 )) ) ; 
                    __context__.SourceCodeLine = 79;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Right( _181[ 0 ] , (int)( 1 ) ) != ","))  ) ) 
                        {
                        __context__.SourceCodeLine = 79;
                        IPROCESSINGVLANID = (ushort) ( 0 ) ; 
                        }
                    
                    } 
                
                }
            
            } 
        
        __context__.SourceCodeLine = 79;
        _110 (  __context__ , "CiscoSwitch_processShowVLan iNumberOfInputs = ", Functions.ItoA( (int)( _176 ) )) ; 
        __context__.SourceCodeLine = 79;
        _110 (  __context__ , "CiscoSwitch_processShowVLan iNumberOfOutputs = ", Functions.ItoA( (int)( _177 ) )) ; 
        __context__.SourceCodeLine = 79;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_164 != 7) ) || Functions.TestForTrue ( Functions.BoolToInt (_176 != _171) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (_177 != _172) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 79;
            _110 (  __context__ , "CiscoSwitch_processShowVLan", "Invalid configuration (missing VLANs or unexpected number or inputs or outputs)") ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 79;
            _97 = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 79;
            _167 = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 79;
            _183  .UpdateValue ( Functions.ItoA (  (int) ( _177 ) )  ) ; 
            __context__.SourceCodeLine = 79;
            _83 = (ushort) ( _176 ) ; 
            __context__.SourceCodeLine = 79;
            _84 = (ushort) ( _177 ) ; 
            __context__.SourceCodeLine = 79;
            ushort __FN_FORSTART_VAL__3 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__3 = (ushort)8; 
            int __FN_FORSTEP_VAL__3 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (I  >= __FN_FORSTART_VAL__3) && (I  <= __FN_FOREND_VAL__3) ) : ( (I  <= __FN_FORSTART_VAL__3) && (I  >= __FN_FOREND_VAL__3) ) ; I  += (ushort)__FN_FORSTEP_VAL__3) 
                { 
                __context__.SourceCodeLine = 79;
                ushort __FN_FORSTART_VAL__4 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__4 = (ushort)_178[ I , 0 ]; 
                int __FN_FORSTEP_VAL__4 = (int)1; 
                for ( J  = __FN_FORSTART_VAL__4; (__FN_FORSTEP_VAL__4 > 0)  ? ( (J  >= __FN_FORSTART_VAL__4) && (J  <= __FN_FOREND_VAL__4) ) : ( (J  <= __FN_FORSTART_VAL__4) && (J  >= __FN_FOREND_VAL__4) ) ; J  += (ushort)__FN_FORSTEP_VAL__4) 
                    { 
                    __context__.SourceCodeLine = 79;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _178[ I , J ] > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 80;
                        MakeString ( _185 , "{0}{1:d}", _187 [ I ] , (short)J) ; 
                        __context__.SourceCodeLine = 80;
                        _95 [ _167 ]  .UpdateValue ( _185  ) ; 
                        __context__.SourceCodeLine = 80;
                        MakeString ( _185 , "g_RXPositionOnStack[{0:d}] = {1}", (short)_167, _95 [ _167 ] ) ; 
                        __context__.SourceCodeLine = 80;
                        _110 (  __context__ , "CiscoSwitch_processShowVLan  ", _185) ; 
                        __context__.SourceCodeLine = 80;
                        OUTPUTFB [ _167]  .Value = (ushort) ( (_178[ I , J ] - 10) ) ; 
                        __context__.SourceCodeLine = 80;
                        _102 [ _167 ]  .UpdateValue ( Functions.ItoA (  (int) ( (_178[ I , J ] - 10) ) )  ) ; 
                        __context__.SourceCodeLine = 80;
                        _101 [ _167 ]  .UpdateValue ( Functions.ItoA (  (int) ( (_178[ I , J ] - 10) ) )  ) ; 
                        __context__.SourceCodeLine = 80;
                        _167 = (ushort) ( (_167 + 1) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 79;
                    } 
                
                __context__.SourceCodeLine = 79;
                } 
            
            __context__.SourceCodeLine = 80;
            _185  .UpdateValue ( _95 [ 1 ]  ) ; 
            __context__.SourceCodeLine = 80;
            _161 = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 80;
            do 
                { 
                __context__.SourceCodeLine = 80;
                _163 = (ushort) ( Functions.Find( "/" , _185 , 1 ) ) ; 
                __context__.SourceCodeLine = 80;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _163 > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 80;
                    _161 = (ushort) ( (_161 + 1) ) ; 
                    __context__.SourceCodeLine = 80;
                    _186  .UpdateValue ( Functions.Remove ( "/" , _185 , 1)  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 80;
                    _161 = (ushort) ( (_161 + 1) ) ; 
                    __context__.SourceCodeLine = 80;
                    _186  .UpdateValue ( _185  ) ; 
                    } 
                
                } 
            while (false == ( Functions.TestForTrue  ( Functions.BoolToInt ( _163 < 1 )) )); 
            __context__.SourceCodeLine = 80;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_161 == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 80;
                _96  .UpdateValue ( "SG300"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 80;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_161 == 2))  ) ) 
                    { 
                    __context__.SourceCodeLine = 80;
                    _96  .UpdateValue ( "SG500-Standalone"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 80;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_161 == 3))  ) ) 
                        { 
                        __context__.SourceCodeLine = 80;
                        _96  .UpdateValue ( "SG500-Stack"  ) ; 
                        } 
                    
                    }
                
                }
            
            } 
        
        __context__.SourceCodeLine = 73;
        } 
    
    __context__.SourceCodeLine = 81;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_164 != 7) ) || Functions.TestForTrue ( Functions.BoolToInt (_176 != _171) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (_177 != _172) )) ))  ) ) 
        { 
        __context__.SourceCodeLine = 81;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_97 == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 81;
            _97 = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 81;
            REBOOTINGINPROGRESS  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 81;
            _185  .UpdateValue ( "The module has detected an unusable configuration on the switch.\r\n\r\nRestoring switch to default configuration.\r\n\r\nThis may take several minutes."  ) ; 
            __context__.SourceCodeLine = 81;
            ERRORMSG__DOLLAR__  .UpdateValue ( _185  ) ; 
            __context__.SourceCodeLine = 81;
            _129 (  __context__ , "reload", (ushort)( 15 ), "Are you sure you want to continue ? (Y/N)", (ushort)( 19 ), "Try Again Later") ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 81;
            REBOOTINGINPROGRESS  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 81;
            _185  .UpdateValue ( "Bad Switch configuration\r\n\r\n Please run JAD CONFIG Again"  ) ; 
            __context__.SourceCodeLine = 81;
            ERRORMSG__DOLLAR__  .UpdateValue ( _185  ) ; 
            __context__.SourceCodeLine = 81;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_15__" , 200 , __SPLS_TMPVAR__WAITLABEL_15___Callback ) ;
            } 
        
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 81;
        REBOOTINGINPROGRESS  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 81;
        ERRORMSG__DOLLAR__  .UpdateValue ( "NONE"  ) ; 
        __context__.SourceCodeLine = 82;
        _155 (  __context__  ) ; 
        __context__.SourceCodeLine = 82;
        _105 = (ushort) ( 1 ) ; 
        } 
    
    
    }
    
public void __SPLS_TMPVAR__WAITLABEL_15___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 81;
            REBOOTINGINPROGRESS  .Value = (ushort) ( 0 ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

private void _157 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 82;
    _154 (  __context__  ) ; 
    
    }
    
private void _158 (  SplusExecutionContext __context__, ushort _160 , CrestronString _161 ) 
    { 
    
    __context__.SourceCodeLine = 82;
    _110 (  __context__ , "MsgActionSwitch - ", Functions.ItoA( (int)( _160 ) )) ; 
    __context__.SourceCodeLine = 82;
    
        {
        int __SPLS_TMPVAR__SWTCH_7__ = ((int)_160);
        
            { 
            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 1) ) ) ) 
                { 
                __context__.SourceCodeLine = 82;
                _126 (  __context__ , _161) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 2) ) ) ) 
                { 
                __context__.SourceCodeLine = 82;
                _125 (  __context__ , _161) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 6) ) ) ) 
                { 
                __context__.SourceCodeLine = 82;
                _157 (  __context__  ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 4) ) ) ) 
                { 
                __context__.SourceCodeLine = 82;
                _124 (  __context__ , _161) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 10) ) ) ) 
                { 
                __context__.SourceCodeLine = 82;
                _153 (  __context__ , _161) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 12) ) ) ) 
                { 
                __context__.SourceCodeLine = 82;
                _156 (  __context__ , _161) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 14) ) ) ) 
                { 
                __context__.SourceCodeLine = 82;
                _152 (  __context__  ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 15) ) ) ) 
                { 
                __context__.SourceCodeLine = 82;
                _144 (  __context__  ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 17) ) ) ) 
                { 
                __context__.SourceCodeLine = 82;
                _145 (  __context__  ) ; 
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 18) ) ) ) 
                { 
                __context__.SourceCodeLine = 82;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_16__" , 2000 , __SPLS_TMPVAR__WAITLABEL_16___Callback ) ;
                } 
            
            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 19) ) ) ) 
                { 
                __context__.SourceCodeLine = 82;
                _146 (  __context__  ) ; 
                } 
            
            } 
            
        }
        
    
    
    }
    
public void __SPLS_TMPVAR__WAITLABEL_16___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 82;
            _119 (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

private void _159 (  SplusExecutionContext __context__ ) 
    { 
    ushort _161 = 0;
    ushort _162 = 0;
    
    CrestronString _163;
    _163  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    
    
    __context__.SourceCodeLine = 82;
    _161 = (ushort) ( Functions.Find( "\u0027" , SWITCH_CLIENT.SocketRxBuf , 1 ) ) ; 
    __context__.SourceCodeLine = 82;
    if ( Functions.TestForTrue  ( ( _161)  ) ) 
        { 
        __context__.SourceCodeLine = 82;
        _163  .UpdateValue ( Functions.Mid ( SWITCH_CLIENT .  SocketRxBuf ,  (int) ( 1 ) ,  (int) ( _161 ) )  ) ; 
        __context__.SourceCodeLine = 83;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "#" , SWITCH_CLIENT.SocketRxBuf , 1 ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 83;
            SWITCH_CLIENT .  SocketRxBuf  .UpdateValue ( Functions.Right ( SWITCH_CLIENT .  SocketRxBuf ,  (int) ( (Functions.Length( SWITCH_CLIENT.SocketRxBuf ) - _161) ) )  ) ; 
            } 
        
        } 
    
    
    }
    
private void _160 (  SplusExecutionContext __context__ ) 
    { 
    CrestronString _162;
    _162  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
    
    _73 _163;
    _163  = new _73( this, true );
    _163 .PopulateCustomAttributeList( false );
    
    
    __context__.SourceCodeLine = 83;
    if ( Functions.TestForTrue  ( ( _49( __context__ , ref _82 ))  ) ) 
        { 
        __context__.SourceCodeLine = 83;
        _81 . _74  .UpdateValue ( "NULL STRING"  ) ; 
        __context__.SourceCodeLine = 83;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_72._71 == 65535))  ) ) 
            {
            __context__.SourceCodeLine = 83;
            _104 = (ushort) ( _150( __context__ , (ushort)( _104 ) ) ) ; 
            }
        
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 83;
        _115 (  __context__ ,   ref  _163 ) ; 
        __context__.SourceCodeLine = 83;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_163._74 == "NULL STRING"))  ) ) 
            { 
            __context__.SourceCodeLine = 83;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_17__" , 10 , __SPLS_TMPVAR__WAITLABEL_17___Callback ) ;
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 83;
            _123 (  __context__ , _163) ; 
            } 
        
        } 
    
    
    }
    
public void __SPLS_TMPVAR__WAITLABEL_17___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 83;
            _160 (  __context__  ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

private ushort _161 (  SplusExecutionContext __context__ ) 
    { 
    ushort _163 = 0;
    ushort _164 = 0;
    ushort _165 = 0;
    ushort _166 = 0;
    
    CrestronString _5;
    _5  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    ushort _167 = 0;
    
    CrestronString _168;
    CrestronString _169;
    _168  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10000, this );
    _169  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10000, this );
    
    ushort _170 = 0;
    
    ushort _171 = 0;
    
    
    __context__.SourceCodeLine = 84;
    _110 (  __context__ , "fnMessageHandler_onCommRx g_DebugRXCommsCnt ", Functions.ItoA( (int)( _109 ) )) ; 
    __context__.SourceCodeLine = 84;
    _109 = (ushort) ( (_109 + 1) ) ; 
    __context__.SourceCodeLine = 84;
    _170 = (ushort) ( _85 ) ; 
    __context__.SourceCodeLine = 84;
    _159 (  __context__  ) ; 
    __context__.SourceCodeLine = 84;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "**** IDLE TIMEOUT ***\r\n\r\n\r\n" , SWITCH_CLIENT.SocketRxBuf , 1 ) > 0 ))  ) ) 
        { 
        __context__.SourceCodeLine = 84;
        SWITCH_CLIENT .  SocketRxBuf  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 84;
        _163 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 84;
        _119 (  __context__  ) ; 
        __context__.SourceCodeLine = 84;
        _120 (  __context__  ) ; 
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 84;
        _5  .UpdateValue ( _81 . _75  ) ; 
        __context__.SourceCodeLine = 84;
        _110 (  __context__ , "fnMessageHandler_onCommRx ok pattern = ", _5) ; 
        __context__.SourceCodeLine = 84;
        _110 (  __context__ , "fnMessageHandler_onCommRx Switch_client.SocketRxBuf ", SWITCH_CLIENT.SocketRxBuf) ; 
        __context__.SourceCodeLine = 84;
        _163 = (ushort) ( Functions.Find( _5 , SWITCH_CLIENT.SocketRxBuf ) ) ; 
        __context__.SourceCodeLine = 84;
        _110 (  __context__ , "fnMessageHandler_onCommRx pos ", Functions.ItoA( (int)( _163 ) )) ; 
        __context__.SourceCodeLine = 84;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _163 > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 84;
            _167 = (ushort) ( _81._76 ) ; 
            __context__.SourceCodeLine = 84;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_18__" , 10 , __SPLS_TMPVAR__WAITLABEL_18___Callback ) ;
            } 
        
        else 
            {
            __context__.SourceCodeLine = 85;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( _81._77 ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 85;
                _5  .UpdateValue ( _81 . _77  ) ; 
                __context__.SourceCodeLine = 85;
                _163 = (ushort) ( Functions.Find( _5 , SWITCH_CLIENT.SocketRxBuf ) ) ; 
                __context__.SourceCodeLine = 85;
                _110 (  __context__ , "fnMessageHandler_onCommRx fail pos ", Functions.ItoA( (int)( _163 ) )) ; 
                __context__.SourceCodeLine = 85;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _163 > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 85;
                    _167 = (ushort) ( _81._78 ) ; 
                    __context__.SourceCodeLine = 85;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_108 == 65535))  ) ) 
                        { 
                        __context__.SourceCodeLine = 85;
                        _108 = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 85;
                        CancelAllWait ( ) ; 
                        } 
                    
                    } 
                
                } 
            
            }
        
        __context__.SourceCodeLine = 85;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _163 > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 85;
            _165 = (ushort) ( Functions.Find( _81._74 , SWITCH_CLIENT.SocketRxBuf ) ) ; 
            __context__.SourceCodeLine = 85;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _165 > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 85;
                _168  .UpdateValue ( Functions.Mid ( SWITCH_CLIENT .  SocketRxBuf ,  (int) ( (_165 + Functions.Length( _81._74 )) ) ,  (int) ( (Functions.Length( SWITCH_CLIENT.SocketRxBuf ) - ((_165 + Functions.Length( _81._74 )) - 1)) ) )  ) ; 
                } 
            
            __context__.SourceCodeLine = 85;
            _163 = (ushort) ( ((_163 + Functions.Length( _5 )) - 1) ) ; 
            __context__.SourceCodeLine = 85;
            _169  .UpdateValue ( Functions.Remove ( _163, SWITCH_CLIENT .  SocketRxBuf )  ) ; 
            __context__.SourceCodeLine = 85;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _167 > 0 ))  ) ) 
                { 
                } 
            
            __context__.SourceCodeLine = 85;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _167 > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 85;
                _158 (  __context__ , (ushort)( _167 ), _168) ; 
                } 
            
            __context__.SourceCodeLine = 86;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_170 == _85))  ) ) 
                { 
                __context__.SourceCodeLine = 86;
                _160 (  __context__  ) ; 
                } 
            
            } 
        
        } 
    
    __context__.SourceCodeLine = 86;
    return (ushort)( _163) ; 
    
    }
    
public void __SPLS_TMPVAR__WAITLABEL_18___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 84;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_108 == 65535))  ) ) 
                { 
                __context__.SourceCodeLine = 84;
                _108 = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 85;
                CancelAllWait ( ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

private ushort _162 (  SplusExecutionContext __context__ ) 
    { 
    ushort _164 = 0;
    
    
    __context__.SourceCodeLine = 86;
    _164 = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 86;
    while ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.GetC( _87 ) != 65535))  ) ) 
        { 
        __context__.SourceCodeLine = 86;
        _164 = (ushort) ( 65535 ) ; 
        __context__.SourceCodeLine = 86;
        } 
    
    __context__.SourceCodeLine = 86;
    return (ushort)( _164) ; 
    
    }
    
private void _163 (  SplusExecutionContext __context__, CrestronString _74 ) 
    { 
    ushort _165 = 0;
    ushort _166 = 0;
    ushort _167 = 0;
    
    CrestronString _168;
    CrestronString _169;
    CrestronString _170;
    CrestronString _171;
    CrestronString _172;
    CrestronString _173;
    CrestronString _63;
    CrestronString _174;
    _168  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
    _169  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4, this );
    _170  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4, this );
    _171  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    _172  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    _173  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4, this );
    _63  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 3, this );
    _174  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 3, this );
    
    
    __context__.SourceCodeLine = 86;
    _110 (  __context__ , "ExtractPOECommand cmd", _74) ; 
    __context__.SourceCodeLine = 86;
    _168  .UpdateValue ( Functions.Remove ( " " , _74 , 1)  ) ; 
    __context__.SourceCodeLine = 86;
    _168  .UpdateValue ( Functions.Left ( _168 ,  (int) ( (Functions.Length( _168 ) - 1) ) )  ) ; 
    __context__.SourceCodeLine = 86;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_168 == "POE"))  ) ) 
        { 
        __context__.SourceCodeLine = 86;
        _165 = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 86;
        while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( ";" , _74 , 1 ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 86;
            _171  .UpdateValue ( Functions.Remove ( ";" , _74 , 1)  ) ; 
            __context__.SourceCodeLine = 86;
            _171  .UpdateValue ( Functions.Left ( _171 ,  (int) ( (Functions.Length( _171 ) - 1) ) )  ) ; 
            __context__.SourceCodeLine = 86;
            _172  .UpdateValue ( Functions.Remove ( "=" , _171 , 1)  ) ; 
            __context__.SourceCodeLine = 86;
            _172  .UpdateValue ( Functions.Left ( _172 ,  (int) ( (Functions.Length( _172 ) - 1) ) )  ) ; 
            __context__.SourceCodeLine = 86;
            _173  .UpdateValue ( _171  ) ; 
            __context__.SourceCodeLine = 87;
            _174  .UpdateValue ( Functions.Remove ( "," , _172 , 1)  ) ; 
            __context__.SourceCodeLine = 87;
            _174  .UpdateValue ( Functions.Left ( _174 ,  (int) ( (Functions.Length( _174 ) - 1) ) )  ) ; 
            __context__.SourceCodeLine = 87;
            _63  .UpdateValue ( _172  ) ; 
            __context__.SourceCodeLine = 87;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_174 != "") ) && Functions.TestForTrue ( Functions.BoolToInt (_63 != "") )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 87;
                
                    {
                    int __SPLS_TMPVAR__SWTCH_8__ = ((int)Functions.Atoi( _174 ));
                    
                        { 
                        if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 1) ) ) ) 
                            {
                            __context__.SourceCodeLine = 87;
                            _167 = (ushort) ( (500 + Functions.Atoi( _63 )) ) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 2) ) ) ) 
                            {
                            __context__.SourceCodeLine = 87;
                            _167 = (ushort) ( (610 + Functions.Atoi( _63 )) ) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 3) ) ) ) 
                            {
                            __context__.SourceCodeLine = 87;
                            _167 = (ushort) ( (720 + Functions.Atoi( _63 )) ) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 4) ) ) ) 
                            {
                            __context__.SourceCodeLine = 87;
                            _167 = (ushort) ( (830 + Functions.Atoi( _63 )) ) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 5) ) ) ) 
                            {
                            __context__.SourceCodeLine = 87;
                            _167 = (ushort) ( (940 + Functions.Atoi( _63 )) ) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 6) ) ) ) 
                            {
                            __context__.SourceCodeLine = 87;
                            _167 = (ushort) ( (1050 + Functions.Atoi( _63 )) ) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 7) ) ) ) 
                            {
                            __context__.SourceCodeLine = 87;
                            _167 = (ushort) ( (1160 + Functions.Atoi( _63 )) ) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 8) ) ) ) 
                            {
                            __context__.SourceCodeLine = 87;
                            _167 = (ushort) ( (1270 + Functions.Atoi( _63 )) ) ; 
                            }
                        
                        } 
                        
                    }
                    
                
                __context__.SourceCodeLine = 87;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_173 == "ON"))  ) ) 
                    { 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 87;
                    _167 = (ushort) ( (_167 + 52) ) ; 
                    } 
                
                __context__.SourceCodeLine = 87;
                _110 (  __context__ , "ExtractPOECommand StackSwitchNumber", _174) ; 
                __context__.SourceCodeLine = 87;
                _110 (  __context__ , "ExtractPOECommand index", Functions.ItoA( (int)( _167 ) )) ; 
                __context__.SourceCodeLine = 87;
                _110 (  __context__ , "ExtractPOECommand Action", _173) ; 
                __context__.SourceCodeLine = 87;
                _101 [ _167 ]  .UpdateValue ( "1"  ) ; 
                __context__.SourceCodeLine = 87;
                _151 (  __context__  ) ; 
                } 
            
            __context__.SourceCodeLine = 86;
            } 
        
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 88;
        _110 (  __context__ , "ExtractPOECommand", "UNKNOWN COMMAND") ; 
        } 
    
    
    }
    
private void _164 (  SplusExecutionContext __context__, CrestronString _74 ) 
    { 
    ushort _166 = 0;
    ushort _167 = 0;
    ushort _168 = 0;
    ushort _169 = 0;
    ushort _170 = 0;
    
    CrestronString _171;
    CrestronString _172;
    CrestronString _173;
    CrestronString _174;
    CrestronString _175;
    _171  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    _172  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    _173  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 3, this );
    _174  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 3, this );
    _175  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    
    
    __context__.SourceCodeLine = 88;
    _166 = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 88;
    while ( Functions.TestForTrue  ( ( Functions.BoolToInt (_166 != 0))  ) ) 
        { 
        __context__.SourceCodeLine = 88;
        _166 = (ushort) ( Functions.Find( ";" , _74 , 1 ) ) ; 
        __context__.SourceCodeLine = 88;
        _171  .UpdateValue ( Functions.Remove ( ";" , _74 , 1)  ) ; 
        __context__.SourceCodeLine = 88;
        _171  .UpdateValue ( Functions.Left ( _171 ,  (int) ( (Functions.Length( _171 ) - 1) ) )  ) ; 
        __context__.SourceCodeLine = 88;
        _172  .UpdateValue ( Functions.Remove ( "SW " , _171 , 1)  ) ; 
        __context__.SourceCodeLine = 88;
        _172  .UpdateValue ( Functions.Remove ( "=" , _171 , 1)  ) ; 
        __context__.SourceCodeLine = 88;
        _172  .UpdateValue ( Functions.Left ( _172 ,  (int) ( (Functions.Length( _172 ) - 1) ) )  ) ; 
        __context__.SourceCodeLine = 88;
        _173  .UpdateValue ( _171  ) ; 
        __context__.SourceCodeLine = 88;
        _170 = (ushort) ( Functions.Atoi( _173 ) ) ; 
        __context__.SourceCodeLine = 88;
        _167 = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 88;
        do 
            { 
            __context__.SourceCodeLine = 88;
            _168 = (ushort) ( Functions.Find( "," , _172 , 1 ) ) ; 
            __context__.SourceCodeLine = 88;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _168 > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 88;
                _175  .UpdateValue ( Functions.Remove ( "," , _172 , 1)  ) ; 
                __context__.SourceCodeLine = 88;
                _175  .UpdateValue ( Functions.Left ( _175 ,  (int) ( (Functions.Length( _175 ) - 1) ) )  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 88;
                _175  .UpdateValue ( _172  ) ; 
                } 
            
            __context__.SourceCodeLine = 88;
            _169 = (ushort) ( Functions.Atoi( _175 ) ) ; 
            __context__.SourceCodeLine = 89;
            _110 (  __context__ , "ExtractInputAndOutputForSwitch iOutputPort;", Functions.ItoA( (int)( _169 ) )) ; 
            __context__.SourceCodeLine = 89;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _169 <= _84 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _169 > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _170 <= _83 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _170 > 0 ) )) ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 89;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_72._68 == 0) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _170 > 3 ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _169 > 3 ) )) ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 89;
                    _110 (  __context__ , "ExtractInputAndOutputForSwitch", "Cant Process this in demo mode") ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 89;
                    _102 [ _169 ]  .UpdateValue ( "0"  ) ; 
                    __context__.SourceCodeLine = 89;
                    _101 [ _169 ]  .UpdateValue ( _173  ) ; 
                    } 
                
                } 
            
            } 
        while (false == ( Functions.TestForTrue  ( Functions.BoolToInt ( _168 < 1 )) )); 
        __context__.SourceCodeLine = 89;
        _151 (  __context__  ) ; 
        __context__.SourceCodeLine = 88;
        } 
    
    
    }
    
private void _165 (  SplusExecutionContext __context__, ushort _167 , ushort _168 ) 
    { 
    
    __context__.SourceCodeLine = 89;
    _110 (  __context__ , "fnCiscoSwitch_handleSetInput input", Functions.ItoA( (int)( _167 ) )) ; 
    __context__.SourceCodeLine = 89;
    _110 (  __context__ , "fnCiscoSwitch_handleSetInput ioutput", Functions.ItoA( (int)( _168 ) )) ; 
    __context__.SourceCodeLine = 89;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _168 <= _84 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _168 > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _167 <= _83 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( _167 > 0 ) )) ) )) ))  ) ) 
        { 
        __context__.SourceCodeLine = 90;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (_72._68 == 0) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( _167 > 3 ) ) || Functions.TestForTrue ( Functions.BoolToInt ( _168 > 3 ) )) ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 90;
            _110 (  __context__ , "ExtractInputAndOutputForSwitch", "Cant Process this in demo mode") ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 90;
            _102 [ _168 ]  .UpdateValue ( "0"  ) ; 
            __context__.SourceCodeLine = 90;
            _101 [ _168 ]  .UpdateValue ( Functions.ItoA (  (int) ( _167 ) )  ) ; 
            __context__.SourceCodeLine = 90;
            _151 (  __context__  ) ; 
            } 
        
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 90;
        _110 (  __context__ , "fnCiscoSwitch_handleSetInput ", "Cant Process this in demo mode") ; 
        } 
    
    
    }
    
private void _166 (  SplusExecutionContext __context__, CrestronString _168 ) 
    { 
    ushort _169 = 0;
    
    
    __context__.SourceCodeLine = 90;
    _169 = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 90;
    while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _169 < Functions.Length( _168 ) ))  ) ) 
        { 
        __context__.SourceCodeLine = 90;
        Print( "{0}", Functions.Mid ( _168 ,  (int) ( _169 ) ,  (int) ( 100 ) ) ) ; 
        __context__.SourceCodeLine = 90;
        _169 = (ushort) ( (_169 + 100) ) ; 
        __context__.SourceCodeLine = 90;
        } 
    
    
    }
    
object START_MODULE_OnPush_0 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 90;
        _149 (  __context__  ) ; 
        __context__.SourceCodeLine = 90;
        _148 (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SYSTEM_OSC_OnPush_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString STMP;
        STMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        ushort IERROR = 0;
        ushort IC = 0;
        
        
        __context__.SourceCodeLine = 90;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_92 == 1))  ) ) 
            { 
            __context__.SourceCodeLine = 90;
            _120 (  __context__  ) ; 
            __context__.SourceCodeLine = 90;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _66._64 >= 5 ))  ) ) 
                { 
                __context__.SourceCodeLine = 90;
                _66 . _61 = (ushort) ( (_66._61 + 1) ) ; 
                __context__.SourceCodeLine = 91;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Mod( _66._61 , 30 ) == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 91;
                    _129 (  __context__ , " ", (ushort)( 0 ), "#", (ushort)( 0 ), "") ; 
                    } 
                
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object REFRESH_STATUS_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 91;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_72._71 == 65535))  ) ) 
            { 
            __context__.SourceCodeLine = 91;
            _101 [ 1382 ]  .UpdateValue ( "1"  ) ; 
            __context__.SourceCodeLine = 91;
            _151 (  __context__  ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 91;
            _110 (  __context__ , "refresh_status", "g_CiscoSwitch.readyForCommands  =  false") ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SEND_SAVE_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 91;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_72._71 == 65535))  ) ) 
            { 
            __context__.SourceCodeLine = 91;
            _101 [ 1381 ]  .UpdateValue ( "1"  ) ; 
            __context__.SourceCodeLine = 91;
            _151 (  __context__  ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 91;
            _110 (  __context__ , "send_save", "g_CiscoSwitch.readyForCommands  =  false") ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POE_CMD_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 91;
        _163 (  __context__ , POE_CMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FAVOUTITE_CMD_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 91;
        _164 (  __context__ , FAVOUTITE_CMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DEBUGENABLE_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 91;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DEBUGENABLE == "enable debug"))  ) ) 
            { 
            __context__.SourceCodeLine = 91;
            _86 = (ushort) ( 65535 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 91;
            _86 = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CRESTRON_MAC_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 91;
        _94  .UpdateValue ( _94 + CRESTRON_MAC  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object OUTPUT_OnChange_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort OUTPUTPORT = 0;
        ushort INPUTPORT = 0;
        
        ushort I = 0;
        
        
        __context__.SourceCodeLine = 92;
        OUTPUTPORT = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 92;
        INPUTPORT = (ushort) ( OUTPUT[ OUTPUTPORT ] .UshortValue ) ; 
        __context__.SourceCodeLine = 92;
        _110 (  __context__ , "Output Switching", "") ; 
        __context__.SourceCodeLine = 92;
        _165 (  __context__ , (ushort)( INPUTPORT ), (ushort)( OUTPUTPORT )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SWITCH_CLIENT_OnSocketConnect_9 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 92;
        _81 . _75  .UpdateValue ( "User Name:"  ) ; 
        __context__.SourceCodeLine = 92;
        _81 . _76 = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 92;
        _81 . _77  .UpdateValue ( "#"  ) ; 
        __context__.SourceCodeLine = 92;
        _81 . _78 = (ushort) ( 6 ) ; 
        __context__.SourceCodeLine = 92;
        _89  .UpdateValue ( "\r\nClient Connect Event. Connected to: " + P_SWITCHIP + "\r\n"  ) ; 
        __context__.SourceCodeLine = 92;
        _110 (  __context__ , "socket", _89) ; 
        __context__.SourceCodeLine = 92;
        _128 (  __context__ , (ushort)( 1 )) ; 
        __context__.SourceCodeLine = 92;
        _48 (  __context__ ,   ref  _82 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object SWITCH_CLIENT_OnSocketDisconnect_10 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 92;
        _110 (  __context__ , "socket", "\r\nClient DisConnect Event\r\n") ; 
        __context__.SourceCodeLine = 92;
        _128 (  __context__ , (ushort)( 0 )) ; 
        __context__.SourceCodeLine = 92;
        _99 = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object SWITCH_CLIENT_OnSocketStatus_11 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        short STATUS = 0;
        
        
        __context__.SourceCodeLine = 93;
        STATUS = (short) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 93;
        _110 (  __context__ , "The SocketGetStatus returns:", Functions.ItoA( (int)( STATUS ) )) ; 
        __context__.SourceCodeLine = 93;
        _110 (  __context__ , "The MyClient.SocketStatus returns:", Functions.ItoA( (int)( SWITCH_CLIENT.SocketStatus ) )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

private void _167 (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 93;
    if ( Functions.TestForTrue  ( ( _93)  ) ) 
        { 
        __context__.SourceCodeLine = 93;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_19__" , 50 , __SPLS_TMPVAR__WAITLABEL_19___Callback ) ;
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 93;
        _93 = (ushort) ( 65535 ) ; 
        __context__.SourceCodeLine = 93;
        while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( _161( __context__ ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 93;
            } 
        
        __context__.SourceCodeLine = 93;
        _93 = (ushort) ( 0 ) ; 
        } 
    
    
    }
    
public void __SPLS_TMPVAR__WAITLABEL_19___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 93;
            _167 (  __context__  ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object SWITCH_CLIENT_OnSocketReceive_12 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 93;
        _167 (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort _169 = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 93;
        _94  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 93;
        _92 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 93;
        _47 (  __context__ ,   ref  _82 , (ushort)( 1000 )) ; 
        __context__.SourceCodeLine = 93;
        VERSION__DOLLAR__  .UpdateValue ( "V2_03"  ) ; 
        __context__.SourceCodeLine = 93;
        ERRORMSG__DOLLAR__  .UpdateValue ( "NONE"  ) ; 
        __context__.SourceCodeLine = 93;
        _85 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 93;
        _98 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 93;
        _86 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 93;
        _93 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 93;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)1500; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( _169  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_169  >= __FN_FORSTART_VAL__1) && (_169  <= __FN_FOREND_VAL__1) ) : ( (_169  <= __FN_FORSTART_VAL__1) && (_169  >= __FN_FOREND_VAL__1) ) ; _169  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 93;
            _101 [ _169 ]  .UpdateValue ( "0"  ) ; 
            __context__.SourceCodeLine = 93;
            } 
        
        __context__.SourceCodeLine = 93;
        ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__2 = (ushort)1500; 
        int __FN_FORSTEP_VAL__2 = (int)1; 
        for ( _169  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (_169  >= __FN_FORSTART_VAL__2) && (_169  <= __FN_FOREND_VAL__2) ) : ( (_169  <= __FN_FORSTART_VAL__2) && (_169  >= __FN_FOREND_VAL__2) ) ; _169  += (ushort)__FN_FORSTEP_VAL__2) 
            { 
            __context__.SourceCodeLine = 93;
            _102 [ _169 ]  .UpdateValue ( "0"  ) ; 
            __context__.SourceCodeLine = 93;
            } 
        
        __context__.SourceCodeLine = 93;
        _106 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 93;
        _99 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 93;
        _103 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 93;
        _104 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 93;
        _105 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 93;
        _109 = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    _88  = new ushort[ 81 ];
    _87  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
    _89  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
    _91  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 32, this );
    _94  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
    _96  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
    _95  = new CrestronString[ 405 ];
    for( uint i = 0; i < 405; i++ )
        _95 [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10, this );
    _101  = new CrestronString[ 1501 ];
    for( uint i = 0; i < 1501; i++ )
        _101 [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, this );
    _102  = new CrestronString[ 1501 ];
    for( uint i = 0; i < 1501; i++ )
        _102 [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, this );
    SWITCH_CLIENT  = new SplusTcpClient ( 64000, this );
    _66  = new _59( this, true );
    _66 .PopulateCustomAttributeList( false );
    _72  = new _67( this, true );
    _72 .PopulateCustomAttributeList( false );
    _81  = new _73( this, true );
    _81 .PopulateCustomAttributeList( false );
    _82  = new _41( this, true );
    _82 .PopulateCustomAttributeList( false );
    _80  = new _73[ 1001 ];
    for( uint i = 0; i < 1001; i++ )
    {
        _80 [i] = new _73( this, true );
        _80 [i].PopulateCustomAttributeList( false );
        
    }
    
    SYSTEM_OSC = new Crestron.Logos.SplusObjects.DigitalInput( SYSTEM_OSC__DigitalInput__, this );
    m_DigitalInputList.Add( SYSTEM_OSC__DigitalInput__, SYSTEM_OSC );
    
    REFRESH_STATUS = new Crestron.Logos.SplusObjects.DigitalInput( REFRESH_STATUS__DigitalInput__, this );
    m_DigitalInputList.Add( REFRESH_STATUS__DigitalInput__, REFRESH_STATUS );
    
    START_MODULE = new Crestron.Logos.SplusObjects.DigitalInput( START_MODULE__DigitalInput__, this );
    m_DigitalInputList.Add( START_MODULE__DigitalInput__, START_MODULE );
    
    SEND_SAVE = new Crestron.Logos.SplusObjects.DigitalInput( SEND_SAVE__DigitalInput__, this );
    m_DigitalInputList.Add( SEND_SAVE__DigitalInput__, SEND_SAVE );
    
    REBOOTINGINPROGRESS = new Crestron.Logos.SplusObjects.DigitalOutput( REBOOTINGINPROGRESS__DigitalOutput__, this );
    m_DigitalOutputList.Add( REBOOTINGINPROGRESS__DigitalOutput__, REBOOTINGINPROGRESS );
    
    OUTPUT = new InOutArray<AnalogInput>( 404, this );
    for( uint i = 0; i < 404; i++ )
    {
        OUTPUT[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( OUTPUT__AnalogSerialInput__ + i, OUTPUT__AnalogSerialInput__, this );
        m_AnalogInputList.Add( OUTPUT__AnalogSerialInput__ + i, OUTPUT[i+1] );
    }
    
    NUMOFOUTPUTS = new Crestron.Logos.SplusObjects.AnalogOutput( NUMOFOUTPUTS__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( NUMOFOUTPUTS__AnalogSerialOutput__, NUMOFOUTPUTS );
    
    NUMOFINPUTS = new Crestron.Logos.SplusObjects.AnalogOutput( NUMOFINPUTS__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( NUMOFINPUTS__AnalogSerialOutput__, NUMOFINPUTS );
    
    OUTPUTFB = new InOutArray<AnalogOutput>( 404, this );
    for( uint i = 0; i < 404; i++ )
    {
        OUTPUTFB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( OUTPUTFB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( OUTPUTFB__AnalogSerialOutput__ + i, OUTPUTFB[i+1] );
    }
    
    FAVOUTITE_CMD = new Crestron.Logos.SplusObjects.StringInput( FAVOUTITE_CMD__AnalogSerialInput__, 1000, this );
    m_StringInputList.Add( FAVOUTITE_CMD__AnalogSerialInput__, FAVOUTITE_CMD );
    
    POE_CMD = new Crestron.Logos.SplusObjects.StringInput( POE_CMD__AnalogSerialInput__, 1000, this );
    m_StringInputList.Add( POE_CMD__AnalogSerialInput__, POE_CMD );
    
    DEBUGENABLE = new Crestron.Logos.SplusObjects.StringInput( DEBUGENABLE__AnalogSerialInput__, 50, this );
    m_StringInputList.Add( DEBUGENABLE__AnalogSerialInput__, DEBUGENABLE );
    
    CRESTRON_MAC = new Crestron.Logos.SplusObjects.StringInput( CRESTRON_MAC__AnalogSerialInput__, 200, this );
    m_StringInputList.Add( CRESTRON_MAC__AnalogSerialInput__, CRESTRON_MAC );
    
    OPERATIONFB__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( OPERATIONFB__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( OPERATIONFB__DOLLAR____AnalogSerialOutput__, OPERATIONFB__DOLLAR__ );
    
    CRESTRONID__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( CRESTRONID__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( CRESTRONID__DOLLAR____AnalogSerialOutput__, CRESTRONID__DOLLAR__ );
    
    VERSION__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( VERSION__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( VERSION__DOLLAR____AnalogSerialOutput__, VERSION__DOLLAR__ );
    
    ERRORMSG__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( ERRORMSG__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( ERRORMSG__DOLLAR____AnalogSerialOutput__, ERRORMSG__DOLLAR__ );
    
    P_SWITCHIP = new StringParameter( P_SWITCHIP__Parameter__, this );
    m_ParameterList.Add( P_SWITCHIP__Parameter__, P_SWITCHIP );
    
    P_SWITCHUSERNAME = new StringParameter( P_SWITCHUSERNAME__Parameter__, this );
    m_ParameterList.Add( P_SWITCHUSERNAME__Parameter__, P_SWITCHUSERNAME );
    
    P_SWITCHPASSWORD = new StringParameter( P_SWITCHPASSWORD__Parameter__, this );
    m_ParameterList.Add( P_SWITCHPASSWORD__Parameter__, P_SWITCHPASSWORD );
    
    P_SWITCHLICENCEKEY = new StringParameter( P_SWITCHLICENCEKEY__Parameter__, this );
    m_ParameterList.Add( P_SWITCHLICENCEKEY__Parameter__, P_SWITCHLICENCEKEY );
    
    WAIT_FOR_RX_SEND_Callback = new WaitFunction( WAIT_FOR_RX_SEND_CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_13___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_13___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_14___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_14___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_15___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_15___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_16___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_16___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_17___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_17___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_18___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_18___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_19___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_19___CallbackFn );
    
    START_MODULE.OnDigitalPush.Add( new InputChangeHandlerWrapper( START_MODULE_OnPush_0, false ) );
    SYSTEM_OSC.OnDigitalPush.Add( new InputChangeHandlerWrapper( SYSTEM_OSC_OnPush_1, false ) );
    REFRESH_STATUS.OnDigitalPush.Add( new InputChangeHandlerWrapper( REFRESH_STATUS_OnPush_2, false ) );
    SEND_SAVE.OnDigitalPush.Add( new InputChangeHandlerWrapper( SEND_SAVE_OnPush_3, false ) );
    POE_CMD.OnSerialChange.Add( new InputChangeHandlerWrapper( POE_CMD_OnChange_4, false ) );
    FAVOUTITE_CMD.OnSerialChange.Add( new InputChangeHandlerWrapper( FAVOUTITE_CMD_OnChange_5, false ) );
    DEBUGENABLE.OnSerialChange.Add( new InputChangeHandlerWrapper( DEBUGENABLE_OnChange_6, false ) );
    CRESTRON_MAC.OnSerialChange.Add( new InputChangeHandlerWrapper( CRESTRON_MAC_OnChange_7, false ) );
    for( uint i = 0; i < 404; i++ )
        OUTPUT[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( OUTPUT_OnChange_8, false ) );
        
    SWITCH_CLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( SWITCH_CLIENT_OnSocketConnect_9, false ) );
    SWITCH_CLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( SWITCH_CLIENT_OnSocketDisconnect_10, false ) );
    SWITCH_CLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( SWITCH_CLIENT_OnSocketStatus_11, false ) );
    SWITCH_CLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( SWITCH_CLIENT_OnSocketReceive_12, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_JAP_AVSWITCH_CISCO_V2_03 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction WAIT_FOR_RX_SEND_Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_13___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_14___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_15___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_16___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_17___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_18___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_19___Callback;


const uint SYSTEM_OSC__DigitalInput__ = 0;
const uint REFRESH_STATUS__DigitalInput__ = 1;
const uint START_MODULE__DigitalInput__ = 2;
const uint SEND_SAVE__DigitalInput__ = 3;
const uint FAVOUTITE_CMD__AnalogSerialInput__ = 0;
const uint POE_CMD__AnalogSerialInput__ = 1;
const uint DEBUGENABLE__AnalogSerialInput__ = 2;
const uint CRESTRON_MAC__AnalogSerialInput__ = 3;
const uint OUTPUT__AnalogSerialInput__ = 4;
const uint REBOOTINGINPROGRESS__DigitalOutput__ = 0;
const uint OPERATIONFB__DOLLAR____AnalogSerialOutput__ = 0;
const uint CRESTRONID__DOLLAR____AnalogSerialOutput__ = 1;
const uint VERSION__DOLLAR____AnalogSerialOutput__ = 2;
const uint ERRORMSG__DOLLAR____AnalogSerialOutput__ = 3;
const uint NUMOFOUTPUTS__AnalogSerialOutput__ = 4;
const uint NUMOFINPUTS__AnalogSerialOutput__ = 5;
const uint OUTPUTFB__AnalogSerialOutput__ = 6;
const uint P_SWITCHIP__Parameter__ = 10;
const uint P_SWITCHUSERNAME__Parameter__ = 11;
const uint P_SWITCHPASSWORD__Parameter__ = 12;
const uint P_SWITCHLICENCEKEY__Parameter__ = 13;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class _0 : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public CrestronString  _1;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  _2 = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  [] _3;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  [] _4;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  _5;
    
    [SplusStructAttribute(5, false, false)]
    public ushort  _6 = 0;
    
    [SplusStructAttribute(6, false, false)]
    public ushort  _7 = 0;
    
    [SplusStructAttribute(7, false, false)]
    public ushort  _8 = 0;
    
    [SplusStructAttribute(8, false, false)]
    public ushort  _9 = 0;
    
    [SplusStructAttribute(9, false, false)]
    public ushort  [] _10;
    
    [SplusStructAttribute(10, false, false)]
    public short  [] _11;
    
    
    public _0( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        _3  = new ushort[ 11 ];
        _4  = new ushort[ 11 ];
        _10  = new ushort[ 11 ];
        _11  = new short[ 11 ];
        _1  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 80, Owner );
        _5  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, Owner );
        
        
    }
    
}
[SplusStructAttribute(-1, true, false)]
public class _41 : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  _42 = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  _43 = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  _44 = 0;
    
    
    public _41( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        
        
    }
    
}
[SplusStructAttribute(-1, true, false)]
public class _59 : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public CrestronString  _60;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  _61 = 0;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  _62;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  _63 = 0;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  _64 = 0;
    
    [SplusStructAttribute(5, false, false)]
    public CrestronString  _65;
    
    
    public _59( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        _60  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64000, Owner );
        _62  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        _65  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        
        
    }
    
}
[SplusStructAttribute(-1, true, false)]
public class _67 : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  _68 = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  _69 = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  _70 = 0;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  _71 = 0;
    
    
    public _67( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        
        
    }
    
}
[SplusStructAttribute(-1, true, false)]
public class _73 : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public CrestronString  _74;
    
    [SplusStructAttribute(1, false, false)]
    public CrestronString  _75;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  _76 = 0;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  _77;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  _78 = 0;
    
    [SplusStructAttribute(5, false, false)]
    public CrestronString  _79;
    
    
    public _73( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        _74  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5000, Owner );
        _75  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, Owner );
        _77  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, Owner );
        _79  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, Owner );
        
        
    }
    
}

}
